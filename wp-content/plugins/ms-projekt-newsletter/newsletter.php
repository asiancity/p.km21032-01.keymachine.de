<?php
/*
Plugin Name: ATH Newsletter
Plugin URI:
Description: Bitte folgende Plugins installieren: HELP-TABS von ATH.
Version: 1.201702.24
Author: Anh-Tuan Hoang
Author URI:
*/

$upload_dir = wp_upload_dir();

define( 'ATH', plugin_dir_path( __FILE__ ) );
define( 'ATH_NEWLETTER', '/newsletters' );
define( 'ATH_VERTEILER', '/verteiler' );
define( 'ATH_NEWLETTER_PATH', $upload_dir["path"] . ATH_NEWLETTER );
define( 'ATH_NEWLETTER_URL', $upload_dir["url"] . ATH_NEWLETTER );
define( 'ATH_VERTEILER_PATH', $upload_dir["path"] . ATH_VERTEILER );
define( 'ATH_VERTEILER_URL', $upload_dir["url"] . ATH_VERTEILER );
define( 'ATH_PLUGIN_PATH', plugin_dir_path(__FILE__));
define( 'ATH_PLUGIN_URL', plugin_dir_url(__FILE__));

if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
if(!class_exists('MSDashboard')){
    wp_die( sprintf( __("No plugins found for &#8220;%s&#8221;."), "MSDashboard - ms_main" ) .'<br><br>'. plugin_dir_path( __FILE__ ) );
}


require('classes/newsletter_events_list.php');
require('classes/newsletter_edit_template.php');
require('classes/newsletter_mailing.php');
require('classes/newsletter_sms.php');
require('classes/newsletter_events_data_events.php');
require('classes/newsletter_events_data_import.php');
require('classes/newsletter_events_data_import_ajax.php');
require('classes/newsletter_events_data_import_stats.php');
require('classes/newsletter_unsubscribe.php');

if( is_admin() )
{
	new ath_newsletter();
}


class ath_newsletter{

	public $dashicon = 'dashicons-email-alt';

	/*
		fn
	*/
	public function __construct(){

		add_action( 'admin_menu', array($this, 'newsletter_admin_menu' ));
		add_filter( 'set-screen-option', array($this, 'set_screen_option'), 10, 3);

		add_action( 'admin_init', 'newsletter_edit_template::newsletter_add_editor_styles' );
		add_action( 'admin_enqueue_scripts', array($this, 'enqueue') );

		/*Newsletter*/
		add_action( 'wp_ajax_newsletter_add_to_mailinglist', array('newsletter_mailing', 'add_to_mailinglist') );
		add_action( 'wp_ajax_nopriv_newsletter_add_to_mailinglist', array('newsletter_mailing' ,'add_to_mailinglist') );

		add_action( 'wp_ajax_newsletter_delete_to_mailinglist', array('newsletter_mailing', 'delete_to_mailinglist') );
		add_action( 'wp_ajax_nopriv_newsletter_delete_to_mailinglist', array('newsletter_mailing' ,'delete_to_mailinglist') );

    add_action( 'wp_ajax_newsletter_mailing_send', array('newsletter_mailing', 'ajax_mailinglist_send') );
		add_action( 'wp_ajax_nopriv_newsletter_mailing_send', array('newsletter_mailing' ,'ajax_mailinglist_send') );

		add_action( 'wp_ajax_newsletter_sql_supermailer', array('newsletter_mailing', 'ajax_sql_supermailer') );
		add_action( 'wp_ajax_nopriv_newsletter_sql_supermailer', array('newsletter_mailing' ,'ajax_sql_supermailer') );

		/*SMS*/
		add_action( 'wp_ajax_newsletter_add_to_smslist', array('newsletter_sms', 'add_to_smslist') );
		add_action( 'wp_ajax_nopriv_newsletter_add_to_smslist', array('newsletter_sms' ,'add_to_smslist') );

		add_action( 'wp_ajax_newsletter_delete_to_smslist', array('newsletter_sms', 'delete_to_smslist') );
		add_action( 'wp_ajax_nopriv_newsletter_delete_to_smslist', array('newsletter_sms' ,'delete_to_smslist') );

    add_action( 'wp_ajax_newsletter_smslist_send', array('newsletter_sms', 'ajax_smslist_send') );
		add_action( 'wp_ajax_nopriv_newsletter_smslist_send', array('newsletter_sms' ,'ajax_smslist_send') );

		add_action( 'wp_ajax_newsletter_smslist_export', array('newsletter_sms', 'ajax_sms_export') );
		add_action( 'wp_ajax_nopriv_newsletter_smslist_export', array('newsletter_sms' ,'ajax_sms_export') );

		add_action( 'wp_ajax_newsletter_smslist_export_show', array('newsletter_sms', 'ajax_sms_export_show') );
		add_action( 'wp_ajax_nopriv_newsletter_smslist_export_show', array('newsletter_sms' ,'ajax_sms_export_show') );

    add_action( 'wp_ajax_newsletter_events_data_import_ajax', array('MSNewsletterEventsDataImportAjax', 'insert') );
		add_action( 'wp_ajax_nopriv_newsletter_events_data_import_ajax', array('MSNewsletterEventsDataImportAjax' ,'insert') );

    add_action( 'wp_ajax_newsletter_events_data_import_delete_ajax', array('MSNewsletterEventsDataImportAjax', 'delete') );
		add_action( 'wp_ajax_nopriv_newsletter_events_data_import_delete_ajax', array('MSNewsletterEventsDataImportAjax' ,'delete') );

    add_action( 'wp_ajax_newsletter_events_data_events_importAnzUsersToEvent_ajax', array('MSNewsletterEventsDataEvents', 'importAnzUsersToEvent') );
		add_action( 'wp_ajax_nopriv_newsletter_events_data_events_importAnzUsersToEvent_ajax', array('MSNewsletterEventsDataEvents' ,'importAnzUsersToEvent') );

    add_action( 'wp_ajax_newsletterUnsubscribe', array('newsletterUnsubscribe', 'ajax_newsletterUnsubscribe') );
    add_action( 'wp_ajax_nopriv_newsletterUnsubscribe', array('newsletterUnsubscribe' ,'ajax_newsletterUnsubscribe') );
  }
  	/*
		fn
	*/
	public function enqueue() {
		wp_enqueue_style( 'plugin-newsletter', ATH_PLUGIN_URL .'css/plugin-newsletter.css' );
   	wp_enqueue_script( 'ath-newsletter-script', ATH_PLUGIN_URL. 'js/scripts.js' );
	}
	/*
		fn
	*/
	public function newsletter_admin_menu(){
		global $newsletter_page_settings;

		$newsletter_page_settings = add_menu_page('Newsletter', 'Newsletter', 'edit_pages', 'ath-newsletter', array($this, 'newsletter_page_actions'), $this->dashicon, 12);
    $newsletter_page_settings_02 = add_submenu_page( 'ath-newsletter', 'Neu importierte Users', 'Stats neuer Users', 'edit_pages', 'ath-newsletter-events-data-stats', array($this, 'newsletterEventsDataImportStats'));
    $newsletter_page_settings_01 = add_submenu_page( 'ath-newsletter', 'Anmeldungen importieren', 'Anmeldungen importieren', 'edit_pages', 'ath-newsletter-events-data-events', array($this, 'MSNewsletterEventsDataEvents'));
    $newsletter_page_settings_03 = add_submenu_page( 'ath-newsletter', 'Abmeldung', 'Abmeldung', 'edit_pages', 'ath-newsletter-unsubscribe', array($this, 'MSNewsletterUnsubscribe'));
		add_action( "load-{$newsletter_page_settings}", array($this, 'add_options') );
    add_action( "load-{$newsletter_page_settings_01}", array($this, 'add_options') );
		add_action( 'admin_head-' . $newsletter_page_settings, array($this, 'help_tab') );
	}

	/*
		fn
	*/
	public function newsletter_page_actions(){
		?>
		<div class="wrap">
			<h2 class="page-title"><span class="dashicons <?php echo $this->dashicon ?>"></span> <?php echo $GLOBALS['title'] ?></h2>
			<?php
            switch( @$_REQUEST["action"] ) {

                 case 'edit':
                    $newsletter_edit_template = new newsletter_edit_template;
                    return $newsletter_edit_template->display();
                case 'mailinglist':
                    $newsletter_mailing = new newsletter_mailing;
                    return $newsletter_mailing->display();

                case 'smslist':
                    $newsletter_sms = new newsletter_sms;
                    return $newsletter_sms->display();

                 default:
                    return $this->newletter_events_list();
            }
            ?>
         </div>
         <?php
	}
  /*
		fn
	*/
  public function MSNewsletterEventsDataEvents()
  {
    $_action  = isset( $_REQUEST["action"] )  ? $_REQUEST["action"] : NULL;
  ?>
      <div class="wrap">
          <h2 class="page-title"><span class="dashicons <?php echo $this->dashicon ?>"></span> <?php echo $GLOBALS['title'] ?></h2>

          <?php
          switch( $_action ) {
            case 'import':
                $MSNewsletterEventsDataImport = new MSNewsletterEventsDataImport;
                $MSNewsletterEventsDataImport->prepare_items();
                ?>
                <div class="subtitle">Externes Event - Booking-Anzahl: <?php echo $MSNewsletterEventsDataImport->record_count() ?></div>
                <div class="subtitle">Intern - wiml_users_events - Users-Anzahl: <span id="MSNewsletterEventsDataDeleteValue"><?php echo $MSNewsletterEventsDataImport->user_events_record_count() ?></span> <a href="#" id="MSNewsletterEventsDataDelete" data-href="<?php echo admin_url("admin-ajax.php"); ?>?action=newsletter_events_data_import_delete_ajax&event_id=<?php echo $_REQUEST["event_id"] ?>&event_datum=<?php echo $_REQUEST["event_datum"] ?>"><span class="dashicons dashicons-trash"></span> Löschen, um neu zu importieren.</a></div>
                <?php /*<div class="alignleft">
                  <a href="?page=ath-users&orderby=updatetime&order=desc">Wiedergekommende Users</a>
                </div> */ ?>
                <div class="alignright">
                  <?php $MSNewsletterEventsDataImport->clubs(); ?>
                  <a href="#" id="MSNewsletterEventsDataImport" class="button button-primary" data-href="<?php echo admin_url("admin-ajax.php"); ?>?action=newsletter_events_data_import_ajax&event_id=<?php echo $_REQUEST["event_id"] ?>"><?php _e("Import") ?></a>
                </div>
                <form method="get" class="table-small">
                <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                <?php $MSNewsletterEventsDataImport->display() ?>
                </form>
                <?php
                break;
            default:
                $MSNewsletterEventsDataEvents = new MSNewsletterEventsDataEvents;
                $MSNewsletterEventsDataEvents->prepare_items();
                ?>
                <div class="subtitle">Externe Events</div>
                <form method="get">
                <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                <?php $MSNewsletterEventsDataEvents->display() ?>
                </form>
                <?php
                break;
          } ?>
      </div>
  <?php
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
  public function newsletterEventsDataImportStats(){
  ?>
    <div class="wrap">
			<h2 class="page-title"><span class="dashicons <?php echo $this->dashicon ?>"></span> <?php echo $GLOBALS['title'] ?></h2>
      <?php
      $newsletterEventsDataImportStats = new newsletterEventsDataImportStats;
      return $newsletterEventsDataImportStats->display();
      ?>
    </div>
  <?php
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
	 public function MSNewsletterUnsubscribe(){
   ?>
      <div class="wrap">
   		<h2 class="page-title"><span class="dashicons <?php echo $this->dashicon ?>"></span> <?php echo $GLOBALS['title'] ?></h2>
         <?php
         $newsletterUnsubscribe = new newsletterUnsubscribe;
         return $newsletterUnsubscribe->display();
         ?>
      </div>
   <?php
   }
	/*
		fn
	*/
	public function newletter_events_list(){

		$newletter_events_list = new newsletter_events_list;
		$newletter_events_list->prepare_items(); ?>
		<a href="?page=mainstream-guestlist&view=load-external-events" class="button alignright">External Events loaden</a>
		<form method="get">
		<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
		<?php $newletter_events_list->display() ?>
		</form>
    <?php
	}
	/*
	   fn
	*/
	public function add_options() {
		global $newsletter_page_settings, $newsletter_page_settings_01;

		$screen = get_current_screen();

		if(!is_object($screen))
			return;

		$option = 'per_page';
		$args = array(
			'label'    => 'Events',
			'default'  => 30,
			'option'   => 'newsletter_per_page'
		);

		add_screen_option( $option, $args );
    $newsletter_events_list = new newsletter_events_list;
    $MSNewsletterEventsDataEvents = new MSNewsletterEventsDataEvents;
	}

	public function set_screen_option($status, $option, $value) {
		if ( 'newsletter_per_page' == $option ) return $value;
	}
	/*
		fn
	*/
	public function help_tab() {

		if (!class_exists('help_tabs')) {
			$class = 'notice notice-error';
			$message = 'No Class "help_tabs". ' . sprintf(__("Install %s now"), 'Plugin (help_tabs)');
			printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message );
			return '';
		}

		$screen		= get_current_screen();

		$dir = ATH_PLUGIN_PATH . '/helps';
		$help_tabs = new help_tabs( $screen, $dir );
	}
}
?>
