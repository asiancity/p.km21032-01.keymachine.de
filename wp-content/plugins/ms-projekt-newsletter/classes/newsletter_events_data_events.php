<?php
/**
 *
 */
class MSNewsletterEventsDataEvents extends newsletter_events_list
{
  public $hiddenColumns;
  public $page_slug = 'ath-newsletter';
  public $mydb;
  /*
		fn
	*/
  public function __construct(){
    global $datenbank;
      //Set parent defaults
      parent::__construct( array(
        'singular'  => 'newsletterEvent',     //singular name of the listed records
        'plural'    => 'newsletterEvents',    //plural name of the listed records
        'ajax'      => false        //does this table support ajax?
      ) );
      $this->hiddenColumns = 'manage'.$this->page_slug.'_page_'.$_REQUEST["page"].'columnshidden';
      $db = $datenbank[0];
  		$mydb = new DB_MySQL($db['localhost'], $db['db'], $db['username'], $db['password']);
  		$this->mydb = $mydb;
  }
  /*
		fn
	*/
  public function table_data( $per_page = 10, $page_number = 1 ){
    global $wpdb, $ms_prefix;
		$type = isset($_GET['type']) ?  $_GET['type'] : NULL;
		$mydb = $this->mydb;
		$url  = $this->url;
    $data = array();


		$sql = "SELECT e.*, (SELECT count(booking_id) FROM ".$ms_prefix."em_bookings b WHERE b.event_id=e.event_id) anz ";
    $sql .= " FROM ".$ms_prefix."em_events e";
    $sql .= " WHERE e.recurrence=0 AND e.event_start_date <= '".current_time("Y-m-d")."'";

    //var_dump( $sql );
		if ( ! empty( $_REQUEST['orderby'] ) ) {

			$sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
			$sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';

		} else {

			$sql .= ' ORDER BY event_start_date desc ';
		}
		$sql .= " LIMIT ".$per_page;
		$sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;
		$events = $mydb->get_results($sql);
    if ( count($events) < 1 ){
      return '';
    }

    /* Compare with Newsletter DB */
    $nsql     = 'SELECT * FROM wiml_maillist_newsletter WHERE event_datum <= "'.current_time('Y-m-d').'"';
    $nEvents  = $wpdb->get_results( $nsql );
    foreach( $nEvents as $nEvent ){
      $ndata[ $nEvent->event_id ] = $nEvent->id;
    }

    foreach($events as $event ) {
      $data[] = array(
        "name"          => $event->event_name,
        "event_datum"   => $event->event_start_date,
        "event_id"      => $event->event_id,
        "anz"           => $event->anz,
        "nid"           => @$ndata[ $event->event_id ]
      );
    }
		return $data;
	}
  /*
		fn
	*/
  public function record_count() {
    global $wpdb, $ms_prefix;
    $mydb   = $this->mydb;
    $sql    = 'SELECT * FROM '.$ms_prefix.'em_events WHERE recurrence=0 AND event_start_date <= "'.current_time("Y-m-d").'"';
    $query  = $mydb->query($sql);
    return count($query);
  }
  /*
		fn
	*/
  public function get_columns() {
	   $columns = array(
       'buttonAction'  => 'Anmeldungen',
        'name'      	  => 'Title',
        'event_datum'   => 'Datum',
        'anz'           => 'Anmeldungen-Anzahl',
        'event_id'      => 'event_id',
        'nid'           => 'Newsletter id',
        'importUserAnz' => 'Import'
    );
	  return $columns;
	}
  /*
		fn
	*/
  public function column_default( $item, $column_name ){
    $_page  = isset( $_REQUEST["page"] )  ? $_REQUEST["page"] : NULL;
    switch( $column_name ) {
        case 'importUserAnz':
          return '<span data-href="'.admin_url("admin-ajax.php").'?action=newsletter_events_data_events_importAnzUsersToEvent_ajax&event_id='.$item["event_id"].'"><img src="'.esc_url( admin_url( 'images/loading.gif' ) ).'" /></span>';
        //newsletter_events_data_import_importAnzUsersToEvent_ajax
				case 'buttonAction':
				  return '<a href="'.admin_url("admin.php").'?page='.$_page.'&action=import&event_id='.$item["event_id"].'&event_datum='.$item["event_datum"].'"><span class="dashicons dashicons-upload"></span></a>';

				default:
					return @$item[ $column_name ];
		}
  }
  /*
		fn
	*/
  public function column_name($item){
    return $item["name"];
  }

  public function extra_tablenav($which) {
    return '';
  }
  /**
  *  fn
  */
  public static function importAnzUsersToEvent(){
    $_event_id  = isset( $_REQUEST["event_id"] )  ? $_REQUEST["event_id"] : NULL;
    if($_event_id < 1){
      wp_die( "KEINE EVENT-ID" );
    }

    global $wpdb;
    $sql  = "SELECT COUNT(*) FROM wiml_users_events WHERE event_id='". $_event_id ."'";
    // var_dump( $sql );
    wp_send_json(array( 'anz' => $wpdb->get_var( $sql ) ) );
    wp_die();
  }
}
?>
