<?php
class newsletter_events_list extends WP_List_Table {

	public $items = '';

  public function __construct(){

      //Set parent defaults
      parent::__construct( array(
        'singular'  => 'newsletter',     //singular name of the listed records
        'plural'    => 'newsletters',    //plural name of the listed records
        'ajax'      => false        //does this table support ajax?
      ) );

  }
	public function table_data( $per_page = 10, $page_number = 1 ){
		global $wpdb;
		$sql = "select * from wiml_maillist_newsletter";
		if ( ! empty( $_REQUEST['eventtime'] ) && $_REQUEST['eventtime'] == 'last') {
			$sql .= ' WHERE event_datum <= "'.current_time('Y-m-d').'"';
		} else {
			$sql .= ' WHERE event_datum >= "'.current_time('Y-m-d').'"';
		}
		if ( ! empty( $_REQUEST['orderby'] ) ) {

			$sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
			$sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';

		} else {
			if ( ! empty( $_REQUEST['eventtime'] ) && $_REQUEST['eventtime'] == 'last') {
			$sql .= ' ORDER BY event_datum desc ';
			} else {
			$sql .= ' ORDER BY event_datum asc ';
			}
		}

		$sql .= " LIMIT ".$per_page;
		$sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;
		$data = $wpdb->get_results($sql, ARRAY_A);

		return $data;
	}
	public function record_count() {
		global $wpdb;
    $sql = '';
		if ( ! empty( $_REQUEST['eventtime'] ) && $_REQUEST['eventtime'] == 'last') {
		    $sql = 'SELECT COUNT(*) FROM wiml_maillist_newsletter WHERE event_datum <= "'.current_time('Y-m-d').'"';
		} else {
		    $sql = 'SELECT COUNT(*) FROM wiml_maillist_newsletter WHERE event_datum >= "'.current_time('Y-m-d').'"';
		}
    return 0;
		return $wpdb->get_var( $sql );
	}

	public function column_name($item){
		$event = (object) array();
		$event->event_id = $item["event_id"];
		$event->event_datum = $item["event_datum"];
		$actions = array(
			'edit'      => sprintf('<a href="?page=%s&action=%s&id=%s">Edit</a>',$_REQUEST['page'],'edit',$item['id']),
			'show'      => sprintf('<a target="_blank" href="'.ATH_NEWLETTER_URL.'/'. $item['id'] . '.html">Newsletter</a>'),
			'verteiler' => sprintf('<a target="_blank" href="'.ATH_VERTEILER_URL.'/'. $item['verteiler'] . '">Verteiler</a>'),
		);
		if( !$item['verteiler'] )
			unset( $actions["verteiler"] );

		if( !file_exists( ATH_NEWLETTER_PATH.'/'. $item['id'] . '.html' ) )
			unset( $actions["show"] );

		return sprintf('%1$s %2$s',$item['name'],$this->row_actions($actions));
	}
	/*
		fn
	*/
	public function column_anzahlEmails($item){
		$actions = array(
			'add'      => sprintf('<a href="?page=%s&action=%s&id=%s">'.__('Liste edit').'</a>',$_REQUEST['page'],'mailinglist',$item['id']),
		);
		return sprintf('%1$s %2$s',(($item['anzahlEmails'] ) ? $item['anzahlEmails'] : 0),$this->row_actions($actions));
	}
	/*
		fn
	*/
	public function column_sms($item){
		$actions = array(
			'add'      => sprintf('<a href="?page=%s&action=%s&id=%s">'.__('Liste edit').'</a>',$_REQUEST['page'],'smslist',$item['id']),
		);
		return sprintf('%1$s %2$s',(($item['sms'] ) ? $item['sms'] : 0),$this->row_actions($actions));
	}
	/*
		fn
	*/
	public function column_default( $item, $column_name )
    {
        switch( $column_name ) {

						case 'name':
						  	return $item[ $column_name ];
						case 'event_datum':
								return date_i18n( "l, d.m.Y", strtotime($item[ $column_name ]) );
						/*
						case 'anzahlEmails':
						case 'sms':
						    return ($item[ $column_name ]) ? $item[ $column_name ] : 0;
						*/
						default:
						    //return '';
								return @$item[ $column_name ];
				}
    }

	function get_columns() {
	   $columns = array(
          'name'      	  => 'Title',
          'event_datum' 	=> 'Datum',
          'anzahlEmails'	=> 'E-Mail',
					'sms'        	  => 'SMS',
        );
	  return $columns;
	}

	public function get_sortable_columns() {
	  $sortable_columns = array(
		'name' => array( 'name', true ),
		'event_datum' => array( 'event_datum', true )
	  );

	  return $sortable_columns;
	}
  public function get_hidden_columns()
  {
      return array();
  }

	public function prepare_items() {
		/** Process bulk action */
		$this->process_bulk_action();

		$user			= get_current_user_id();
		$screen			= get_current_screen();
		$screen_option	= $screen->get_option('per_page', 'option');
		$per_page		= get_user_meta($user, $screen_option, true);
		$current_page	= $this->get_pagenum();
		$total_items 	= $this->record_count();

		if ( empty ( $per_page ) || $per_page < 1 ) {
			$per_page	= $screen->get_option( 'per_page', 'default' );
		}

		$this->set_pagination_args( array(
		  'total_items' => $total_items, //WE have to calculate the total number of items
		  'per_page'    => $per_page //WE have to determine how many items to show on a page
		) );

    $columns  = $this->get_columns();		// columns
    $hidden   = $this->get_hidden_columns();			// hidden
    $sortable = $this->get_sortable_columns();	// sortable

    $this->_column_headers = array($columns, $hidden, $sortable);
		$this->items = $this->table_data( $per_page, $current_page );
	}
	function extra_tablenav($which) {
		if($which == 'top'){
		$actions  = array(  'next' => 'kommende Events', 'last' => 'Vergangen Events', );
		$selected = !empty( $_REQUEST['eventtime'] ) ? $_REQUEST['eventtime'] : '';
		?>
		<div class="alignleft actions">
			<select name="eventtime">
				<?php foreach ( $actions as $k => $v ) : ?>
					<option value="<?php echo esc_attr( $k ); ?>" <?php echo ($k == $selected) ? 'selected="selected"' : '' ?>><?php echo esc_html( $v ); ?></option>
				<?php endforeach; ?>
			</select>

			<?php submit_button( __( 'Filter'), 'secondary', false, false, array( 'id' => 'post-query-submit' ) ); ?>

		</div>

		<?php
		}
	}
}
?>
