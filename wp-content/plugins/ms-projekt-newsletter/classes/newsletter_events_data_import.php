<?php
/**
 *
 */
class MSNewsletterEventsDataImport extends MSNewsletterEventsDataEvents
{

  public $mydb;
  /*
		fn
	*/
  public function __construct(){
    global $datenbank;
      //Set parent defaults
      parent::__construct( array(
        'singular'  => 'newsletterEventBooking',     //singular name of the listed records
        'plural'    => 'newsletterEventBookings',    //plural name of the listed records
        'ajax'      => false        //does this table support ajax?
      ) );
      $db = $datenbank[0];
  		$mydb = new DB_MySQL($db['localhost'], $db['db'], $db['username'], $db['password']);
  		$this->mydb = $mydb;
  }
  /*
		fn
	*/
  public function table_data( $per_page = 10, $page_number = 1 ){
    global $wpdb, $ms_prefix;
		$mydb = $this->mydb;
		$url  = $this->url;
    $data = array();


		$sql = "SELECT b.*, e.* ";
    $sql .= " FROM ".$ms_prefix."em_bookings b";
    $sql .= " LEFT JOIN ".$ms_prefix."em_events e ON b.event_id=e.event_id";
    $sql .= " WHERE b.event_id='".$_REQUEST["event_id"]."'";

    //var_dump( $sql );
		if ( ! empty( $_REQUEST['orderby'] ) ) {

			$sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
			$sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';

		} else {

			$sql .= ' ORDER BY booking_date asc ';
		}
		/*$sql .= " LIMIT ".$per_page;
		$sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;*/
		$bookings = $mydb->get_results($sql);

    foreach(  $bookings as $booking ) {
      $booking_meta = maybe_unserialize( $booking->booking_meta );
      $data[] = array(
        "event_id"          => $booking->event_id,
        "event_name"        => $booking->event_name,
        "event_datum"       => $booking->event_start_date,
        "booking_id"        => $booking->booking_id,
        "booking_date"      => $booking->booking_date,
        "firstname"         => $booking_meta["registration"]["first_name"],
        "lastname"          => $booking_meta["registration"]["last_name"],
        "email_address"     => $booking_meta["registration"]["user_email"],
        "mobile"            => $booking_meta["registration"]["dbem_phone"],
        "booking_web"       => $booking->booking_web,
        "booking_confirm"   => $booking->booking_confirm
      );
    }
		return $data;
	}
  /*
		fn
	*/
  public function record_count() {
    global $wpdb, $ms_prefix;
    $_event_id  = isset( $_REQUEST["event_id"] )  ? $_REQUEST["event_id"] : NULL;
    $mydb       = $this->mydb;
    $sql        = 'SELECT * FROM '.$ms_prefix.'em_bookings WHERE event_id="'. $_event_id .'"';
    $bookings   = $mydb->query($sql);
    return count($bookings);
  }
  /*
		fn
	*/
  public function get_columns() {
	   $columns = array(
        'event_id'        => 'event_id',
        'event_name'      => 'event_name',
        'event_datum'     => 'event_datum',
        'booking_id'      => 'booking_id',
        'booking_date'    => 'booking_datum',
        'firstname'       => 'firstname',
        'lastname'        => 'lastname',
        'email_address'   => 'email_address',
        'mobile'          => 'mobile',
        'booking_web'     => 'booking_web',
        'booking_confirm' => 'booking_confirm',
        'result'          => 'Mobile Result',
        'email_result'    => 'Email Result'
    );
	  return $columns;
	}
  /*
		fn
	*/
  public function column_event_id($item){
    return $item["event_id"];
  }
  /*
		fn
	*/
  public function pagination($which){
    return '';
  }
  /*
		fn
	*/
  public function user_events_record_count() {
    global $wpdb;
    $_event_id      = isset($_REQUEST["event_id"])  ? $_REQUEST["event_id"] : NULL;
    $_event_datum   = isset( $_REQUEST["event_datum"] ) ? $_REQUEST["event_datum"]  : NULL;
    $sql            = 'SELECT COUNT(*) FROM wiml_users_events WHERE event_id="'. $_event_id .'" AND event_datum="'. $_event_datum .'"';
    return $wpdb->get_var( $sql );
  }
  /*
		fn
	*/
  public function clubs(){
    global $wpdb;
    $sql = "SELECT club FROM wiml_users GROUP BY club ASC";
    $clubs = $wpdb->get_results( $sql );
    ?>
    <select name="club" id="MSNewsletterEventsDataClubs">
      <option></option>
      <?php foreach( $clubs as $club ) : ?>
        <option value="<?php echo $club->club ?>"><?php echo $club->club ?></option>
      <?php endforeach; ?>
    </select>
    <?php
  }
}
