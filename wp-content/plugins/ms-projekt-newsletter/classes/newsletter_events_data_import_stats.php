<?php
class newsletterEventsDataImportStats{
  public $statsUserSql;
  public $statsMobileSql;
  public $dbUser    = 'wiml_users';
  public $dbMobile  = 'wiml_mobile';
  public $db_events = "wiml_users_events";
  public $colsUser;
  public $colsMobile;

  public function __construct(){
    $this->colsUser = array("uid", "event_anz", "email_address", "firstname", "lastname", "club", "herkunft", "herkunftsdatum", "booking_confirm");
    $this->colsMobile = array("id", "uid", "mobile", "name", "club", "herkunft", "herkunftsdatum", "netz");
    $this->userDelete();
  }
  public function display(){

    $args = array( "postbox_class" => array("first" => "col-3 first  fix-inside", "second" => "col-9 last scroll-x fix-inside") );

    $widgets = array(
      array("align" => "first", "title" => "Anzahl der neuen Users (blacklist=0 & active=1)", "height" => 50, "data" => $this->statsUsers() ),
      array("align" => "first", "title" => "Anzahl der neuen MobileNr (blacklist=0 & active=1)","height" => 50, "data" => $this->statsMobile() ),
      array("align" => "second", "title" => "Users", "height" => 40, "data" => $this->ListUsers()),
      array("align" => "second", "title" => "Mobile", "height" => 60, "data" => $this->ListMobile())
    );
    $MSDashboard = new MSDashboard( $widgets, $args );
    echo $MSDashboard->display();
  }
  public function statsUsers()
  {
    global $wpdb;
    $this->statsUserSql = 'SELECT COUNT(*) anz, DATE(importdatum) importDateOnly FROM ' . $this->dbUser . ' WHERE blacklist=0 AND active=1 GROUP BY importDateOnly ORDER BY importDateOnly desc LIMIT 10';
    $page               = isset($_REQUEST["page"])  ? $_REQUEST["page"] : NULL;
    $mobileimportdatum  = isset($_REQUEST["mobileimportdatum"]) ? $_REQUEST["mobileimportdatum"]  : NULL;
    $data               = $wpdb->get_results( $this->statsUserSql );

    $out                = '<table class="wp_list_table widefat striped">';
    foreach( $data as $d ){
      $out  .= '<tr>';
      $out  .= '<td>'.$d->importDateOnly.'</td>';
      $out  .= '<td>'.$d->anz.'</td>';
      $out  .= '<td><a href="'.admin_url("admin.php").'?page='.$page.'&importdatum='.$d->importDateOnly.'&mobileimportdatum='.$mobileimportdatum.'"><span class="dashicons dashicons-edit"></span></a></td>';
      $out  .= '</tr>';
    }
    $out    .= '</table>';

    return $out;
  }
  public function statsMobile()
  {
    global $wpdb;
    $page                 = isset($_REQUEST["page"])  ? $_REQUEST["page"] : NULL;
    $importdatum          = isset($_REQUEST["importdatum"]) ? $_REQUEST["importdatum"]  : NULL;

    $this->statsMobileSql = 'SELECT COUNT(*) anz, DATE(importdatum) importDateOnly FROM ' . $this->dbMobile . ' WHERE blacklist=0 AND aktiv_status!="deleted" GROUP BY importDateOnly ORDER BY importDateOnly desc LIMIT 10';
    /*var_dump( $this->statsMobileSql );*/
    $data                 = $wpdb->get_results( $this->statsMobileSql );
    $out                  = '<table class="wp-list-table widefat striped">';

    foreach( $data as $d ){
      $out .= '<tr>';
      $out .= '<td>'.$d->importDateOnly.'</td>';
      $out .= '<td>'.$d->anz.'</td>';
      $out .= '<td><a href="'.admin_url("admin.php").'?page='.$page.'&importdatum='.$importdatum.'&mobileimportdatum='.$d->importDateOnly.'"><span class="dashicons dashicons-feedback"></span></a></td>';
      $out .= '</tr>';
    }
    $out .= '</table>';

    return $out;
  }
  public function ListUsers()
  {
    global $wpdb;

    $_importdatum        = isset($_REQUEST["importdatum"]) ? $_REQUEST["importdatum"]  : NULL;
    $_herkunft           = isset($_REQUEST["herkunft"]) ? $_REQUEST["herkunft"]  : NULL;
    $_page               = isset($_REQUEST["page"])  ? $_REQUEST["page"] : NULL;
    $_mobileimportdatum  = isset($_REQUEST["mobileimportdatum"])  ? $_REQUEST["mobileimportdatum"] : NULL;
    $_orderby            = isset($_REQUEST["orderby"])  ? $_REQUEST["orderby"] : NULL;
    $_order              = isset($_REQUEST["order"])  ? $_REQUEST["order"] : NULL;

    if(!$_importdatum){
      return '';
    }

    $sql = 'SELECT u.*, e.mobile, e.booking_confirm FROM ' . $this->dbUser . ' u LEFT JOIN '.$this->db_events.' e ON u.uid=e.uid WHERE u.blacklist=0 AND u.active=1 AND DATE(u.importdatum)="'.$_importdatum.'" ';
    // var_dump( $sql );
    if (! empty( $_herkunft ) ) {
			$sql .= ' AND herkunft LIKE "%'.$_herkunft.'%"';
		}
    $sql .= ' GROUP BY u.email_address';
    if ( ! empty( $_orderby ) ) {
      $sql .= ' ORDER BY ' . esc_sql( $_orderby );
      $sql .= ! empty( $_order ) ? ' ' . esc_sql( $_order ) : ' ASC';
    } else {
      $sql .= ' ORDER BY event_anz desc ';
    }
    $data = $wpdb->get_results( $sql );

    $url = "?page=".$_page."&importdatum=".$_importdatum."&mobileimportdatum=".$_mobileimportdatum."&herkunft=".$_herkunft."&order=desc";
    $out = '<table class="wp-list-table widefat striped">';
    $out .= '<tr>';
    $out .= '<th></th>';
    foreach( $this->colsUser as $col){
      switch ($col) {
        case 'booking_confirm':
          $out .= '<th><a href="'.$url.'&orderby='.$col.'">Confirm</a></th>';
          break;
        case 'event_anz':
          $out .= '<th><a href="'.$url.'&orderby='.$col.'">Event</a></th>';
          break;

        default:
          $out .= '<th><a href="'.$url.'&orderby='.$col.'">'.$col.'</a></th>';
          break;
      }
    }
    $out .= '<th></th>';
    $out .= '</tr>';
    foreach( $data as $d ){
      $out .= '<tr id="row-'.$d->uid.'">';
      $out .= '<td><a href="'.admin_url("admin.php").'?page='.$_REQUEST["page"].'&delete=true&uid='.$d->uid.'&importdatum='.$_importdatum.'&mobileimportdatum='.$_mobileimportdatum.'&herkunft='.$_herkunft.'&orderby='.$_orderby.'&order='.$_order.'#row-'.$d->uid.'"><span class="dashicons dashicons-trash"></span></a></td>';
      foreach( $this->colsUser as $col){
        switch ($col) {
          case 'herkunft':
            $out .= '<td class="'.$col.'"><a href="'.$url .'&herkunft='.$d->$col.'">'.$d->$col.'</a></td>';
            break;

          default:
            $out .= '<td class="'.$col.'">'.$d->$col. ( ($col == "email_address") ? '<br>'.$d->mobile : '') .'</td>';
            break;
        }

      }
      $out .= '<td><a target="_blank" href="'.admin_url("admin.php").'?page=ath-users&action=edit&uid='.$d->uid.'"><span class="dashicons dashicons-edit"></span></a></td>';
      $out .= '</tr>';
    }
    $out .= '</table>';
    return $out;
  }
  public function ListMobile()
  {
    global $wpdb;

    $_mobileimportdatum = (isset( $_REQUEST["mobileimportdatum"] ) && !empty($_REQUEST["mobileimportdatum"])) ? $_REQUEST["mobileimportdatum"]  : current_time('Y-m-d') ;
    $sql                = 'SELECT * FROM ' . $this->dbMobile . ' WHERE blacklist=0 AND aktiv_status!="deleted" AND DATE(importdatum)="'.$_mobileimportdatum.'" ORDER BY id desc';
    $data               = $wpdb->get_results( $sql );

    $out                = '<table class="wp-list-table widefat striped">';
    $out                .= '<tr>';
    foreach( $this->colsMobile as $col){
      $out .= '<th>'.$col.'</th>';
    }
    $out .= '<th></th>';
    $out .= '</tr>';
    foreach( $data as $d ){
      $out .= '<tr>';
      foreach( $this->colsMobile as $col){
        $out .= '<td>'.$d->$col.'</td>';
      }
      $out .= '<td><a target="_blank" href="'.admin_url("admin.php").'?page=ath-mobile&action=edit&id='.$d->id.'" title="'.__("Show more details").'"><span class="dashicons dashicons-feedback"></span></a></td>';
      $out .= '</tr>';
    }
    $out .= '</table>';
    return $out;
  }
  public function userDelete(){
    global $wpdb;
    if( isset($_REQUEST["delete"]) && isset($_REQUEST["uid"] ) && $_REQUEST["delete"] && $_REQUEST["uid"] ){
      $wpdb->query("UPDATE " . $this->dbUser . " SET active=0 WHERE uid='". $_REQUEST["uid"] ."'");
      $wpdb->query("UPDATE " . $this->dbMobile . " SET aktiv_status='delete' WHERE uid='". $_REQUEST["uid"] ."'");
    }
  }
}
?>
