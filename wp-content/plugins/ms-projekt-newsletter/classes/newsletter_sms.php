<?php
class newsletter_sms
{
	public $event;
	public $title;
	public $newslettertyp = 'sms';
	public $limit;

	public function __construct()
	{
		global $wpdb;
    $limit  = isset( $_REQUEST["limit"] )  ?  $_REQUEST["limit"] : NULL;
		if(!$_REQUEST["id"]){
		  return '';
		}
		$event = $wpdb->get_row("select * from wiml_maillist_newsletter WHERE id='".$_REQUEST["id"]."'");
		$this->event =  $event;
		$this->title =  date_i18n( get_option( 'date_format' ), strtotime( $event->event_datum ) ) . ' - ' . apply_filters("the_title", $event->name);
		$this->limit =  $limit;
	}
	/*
		fn
	*/
	public function export_sql()
	{
    $_club = isset($_REQUEST["club"]) ? $_REQUEST["club"] : NULL;
		if ( $_club ){

			$sql = "SELECT u.mobile, u.name, u.club, u.blacklist, u.aktiv_status, u.id
			FROM wiml_senden ws
			LEFT JOIN wiml_mobile u ON ws.club=u.club
			WHERE ".$this->deactived("u")." AND ws.club='". $_REQUEST["club"] ."' AND ws.newslettertyp='".$this->newslettertyp."' AND ws.newsletterID='".$this->event->id."'";

		} else {

			$sql = "SELECT u.mobile, u.name, u.club, u.blacklist, u.aktiv_status, u.id
			FROM wiml_senden ws
			LEFT JOIN wiml_mobile u ON ws.club=u.club
			WHERE ".$this->deactived("u")." AND ws.newslettertyp='".$this->newslettertyp."' AND ws.newsletterID='".$this->event->id."'";
		}

		$sql .= ' ORDER BY id DESC ';
		/*$sql .= ' LIMIT 30';*/
		$sql .= ($this->limit) ? ' LIMIT ' . $this->limit :'';
		return $sql;
	}
	/*
		fn
	*/
	public function deactived( $alias='' ){
		$alias        = ($alias) ? $alias .'.' : '';
    $_aktiv_status = isset($_REQUEST["aktiv_status"]) ? $_REQUEST["aktiv_status"] : NULL;
    switch( $_aktiv_status )
		{
			case 'erfolgreich' :
				$aktiv_status = $alias ."aktiv_status='erfolgreich'";
				break;
			case 'unsicher' :
				$aktiv_status = $alias ."aktiv_status='unsicher'";
				break;

			case 'alle_ohne_erfolgreich' :
				$aktiv_status = $alias ."aktiv_status!='deleted' AND ". $alias ."aktiv_status!='erfolgreich'";
				break;

			default:
				$aktiv_status = $alias ."aktiv_status!='deleted'";
		}

		return $alias . 'blacklist=0 AND ' . $aktiv_status;
	}
	/*
		fn
	*/
	public function smslist_groupby_club_sql(){
		global $wpdb;
		$where	= 'WHERE ' .$this->deactived();

		$sql	= "select club, count(DISTINCT mobile) as 'anz' from wiml_mobile ".$where." GROUP BY club";
		if ( ! empty( $_REQUEST['orderby'] ) ) {

			$sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
			$sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';

		} else {

			$sql .= ' ORDER BY anz DESC ';
		}

		return $wpdb->get_results($sql);
	}
	/*
		fn
	*/
	public function smslist_groupby_club_display()
	{
		$clubs = $this->smslist_groupby_club_sql();
		$gesamt = 0;
		?>
		<table width="100%" class="wp-list-table fixed striped widefat">
			<thead>
			<tr>
				<th><a href="<?php echo $this->url() ?>&orderby=club&order=ASC">Club</a></th>
				<th><a href="<?php echo $this->url() ?>&orderby=anz&order=DESC">Anzahl</a></th>
				<th><?php _e("Add") ?></th>
			</tr>
			</thead>
			<tbody>
			<?php foreach( $clubs as $club ) : ?>
			<tr>
				<td><?php echo $club->club ?></td>
				<td><?php echo $club->anz ?></td>
				<td align="center"><a href="#" data-href="<?php echo admin_url("admin-ajax.php")?>?action=newsletter_add_to_smslist&id=<?php echo $this->event->id ?>&club=<?php echo $club->club ?>" class="button"><span class="dashicons dashicons-plus"></span></a></td>
			</tr>
			<?php
			$gesamt += $club->anz;
			endforeach; ?>
			</tbody>
            <tfoot>
            <tr><td>Gesamt:</td><td><?php echo $gesamt; ?></td><td></td></tr>
        </tfoot>
		</table>
		<?php
	}
	/*
		fn
	*/
	public function smslist_club_anzahl(){
		$data = array();
		$clubs = $this->smslist_groupby_club_sql();
		foreach( $clubs as $club ) :
		  $data[$club->club] = $club->anz;
		endforeach;
		return $data;
	}
	/*
		fn
	*/
	public function smslist_send_sql(){
		global $wpdb;

		$where	= 'WHERE newslettertyp="'.$this->newslettertyp.'" AND newsletterID="'.$this->event->id.'"';
		$sql	= "select * from wiml_senden ".$where." GROUP BY club";
		if ( ! empty( $_REQUEST['orderby'] ) && $_REQUEST['orderby'] != "anz") {

			$sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
			$sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';

		} else {

			$sql .= ' ORDER BY club asc ';
		}

		return $wpdb->get_results($sql);
	}
	/*
		fn
	*/
	public function smslist_send()
	{
		$clubs = $this->smslist_send_sql();
		$clubsAnzahl = $this->smslist_club_anzahl();
		$gesamt = 0;

		?>
        <table width="100%" class="wp-list-table fixed striped widefat">
            <thead>
                <tr>
                    <th><a href="<?php echo $this->url() ?>&orderby=club&order=ASC">Club</a></th>
                    <th>Anzahl</th>
                    <th><?php _e("Delete") ?></th>
                    <th><?php _e("Download") ?></th>

                </tr>
            </thead>
            <tbody>
				<?php foreach( $clubs as $club ) : ?>
                <tr>
                    <td><?php echo $club->club ?></td>
                    <td><?php echo $clubsAnzahl[$club->club] ?></td>
                    <td align="center" class="delete"><a href="#" data-href="<?php echo admin_url("admin-ajax.php")?>?action=newsletter_delete_to_mailinglist&id=<?php echo $this->event->id ?>&send_id=<?php echo $club->id ?>" class="button"><span class="dashicons dashicons-minus"></span></a></td>
                	<td align="center"><a target="_blank" title="Download diese Liste" href="<?php echo admin_url("admin-ajax.php")?>?action=newsletter_smslist_export&id=<?php echo $this->event->id ?>&club=<?php echo $club->club ?>&areacode=<?php echo $_REQUEST["areacode"] ?>" class="button"><span class="dashicons dashicons-media-spreadsheet"></span></a></td>
                </tr>
                <?php $gesamt += $clubsAnzahl[$club->club];
                endforeach; ?>
            </tbody>
            <tfoot>
            	<tr><td>Gesamt:</td><td><?php echo $this->update_db_smslist($gesamt); ?></td><td colspan="2"></td></tr>
            </tfoot>
        </table>
        <?php
	}
	/*
		fn
	*/
	public function sms_export()
	{

		global $wpdb;
    $_areacode  = isset( $_REQUEST["areacode"] ) ?  $_REQUEST["areacode"] : NULL;
		$sql = $this->export_sql();

		$sql = str_replace("\n", "",$sql);
		$sql = str_replace("<br />", "",$sql);
		$sql = preg_replace('/\s+/', ' ', $sql);

		echo '<p>Anzahl: ' . count( $wpdb->get_results($sql) ) .'</p>';
		echo '<p><textarea style="width:100%;height:150px;">' . $sql .'</textarea></p>';

		echo '<a href="'.admin_url("admin-ajax.php").'?action=newsletter_smslist_export&id='.$this->event->id.'&areacode='. $_areacode .'" class="button-primary" target="_blank">SMS Liste downloaden.</a>';
	}
	/*
		fn
	*/
	public function limit(){
		global $wpdb;
		$sql = 'SELECT COUNT(*) FROM wiml_mobile u WHERE u.club="ms-mitarbeiter"';
	?>
    	<p>Ziehe <strong>(<?php echo $wpdb->get_var( $sql ) ?>)</strong> Mitarbeiter ab.</p>

    	<form method="get" action="<?php echo admin_url("admin-ajax.php") ?>">

            <input type="hidden" name="action" value="newsletter_smslist_export">
            <input type="hidden" name="page" value="<?php echo $_REQUEST["page"] ?>">
            <input type="hidden" name="id" value="<?php echo $_REQUEST["id"] ?>">
            <input type="hidden" name="orderby" value="<?php echo $_REQUEST["orderby"] ?>">
            <input type="hidden" name="order" value="<?php echo $_REQUEST["order"] ?>">
            <input type="hidden" name="areacode" value="<?php echo $_REQUEST["areacode"] ?>">
            <table cellpadding="0" cellspacing="5">
        	<tr><td width="30%"><label for="limit_anzahl">Anzahl </label></td><td><input type="text" name="limit" value="<?php echo $this->limit ?>" id="limit_anzahl"> <br>Wieviel Daten sollen exportiert werden.</td></tr>
            <tr><td><label for="aktiv_status">Aktiv Status </label></td><td>
            	<select name="aktiv_status" id="aktiv_status">
                	<option>Alle</option>
                    <option value="erfolgreich">Erfolgreich</option>
                    <option value="unsicher">Unsicher</option>
                    <option value="alle_ohne_erfolgreich">Alle ohne Erfolgreich</option>
                 </select></td>
            </tr>
            <tr><td><label for="mitarbeiter">Mitrarbeiter </label> </td><td><input type="checkbox" name="mitarbeiter" value="1" id="mitarbeiter" checked></td></tr>
            <tr><td></td><td><?php submit_button( __("Submit") );?></td></tr>
            </table>

        </form>
    <?php
	}
	/*
		fn
	*/
	public function display()
	{
	?>
        <h3><?php echo $this->title ?></h3>
        <div id="dashboard-widgets" class="metabox-holder">
            <div id="postbox-mailinglist-groupby-club" class="postbox-container">
                <div class="meta-box-sortables ui-sortable">
                    <div class="postbox">
                        <h2 class="hndle ui-sortable-handle">SMS Liste (sortiert nach Club)</h2>
                        <div class="inside"><?php $this->smslist_groupby_club_display() ?></div>
                    </div>
                </div>
            </div>
            <div class="postbox-container">
                <div id="postbox-mailinglist-send" class="meta-box-sortables ui-sortable">
                    <div class="postbox">
                        <h2 class="hndle ui-sortable-handle">Send SMS Liste</h2>
                        <div class="inside" data-href="<?php echo admin_url("admin-ajax.php") ?>?action=newsletter_smslist_send&id=<?php echo $this->event->id ?>"><?php $this->smslist_send() ?></div>
                    </div>
                </div>
                <div id="postbox-mailinglist-sql-supermailer" class="meta-box-sortables ui-sortable">
                    <div class="postbox">
                        <h2 class="hndle ui-sortable-handle">SQL Export</h2>
                        <div class="inside" data-href="<?php echo admin_url("admin-ajax.php") ?>?action=newsletter_smslist_export_show&id=<?php echo $this->event->id ?>"><?php $this->sms_export() ?></div>
                    </div>
                </div>
                <div class="meta-box-sortables ui-sortable">
                    <div class="postbox">
                        <h2 class="hndle ui-sortable-handle">Export: Download die Handynr mit Vorwahl</h2>
                        <div class="inside">
                        	<?php
							$areacodes = array(
											""			=> "keine Vorwahl",
											"01"		=> "0 Vorwahl",
											"0049"		=> "0049 Vorwahl",
											"plus49"	=> "+49 Vorwahl",
										);
							foreach( $areacodes as $av => $ac ) :
							?>
                        	<a href="<?php echo $this->url() ?>&areacode=<?php echo $av ?>" class="button"><?php echo $ac ?></a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="meta-box-sortables ui-sortable">
                    <div class="postbox">
                        <h2 class="hndle ui-sortable-handle">Export: Download-Begrenzung</h2>
                        <div class="inside"><?php $this->limit() ?></div>
                    </div>

                </div>
            </div>
        </div>
    <?php
	}
	/*
	fn
	*/
	public function add_to_smslist(){
		global $wpdb;
		if(!$_REQUEST["club"]){
		  return '';
		}
		$newsletter_sms= new newsletter_sms;
		$ws = $wpdb->get_row("SELECT * FROM wiml_senden WHERE club='".$_REQUEST["club"]."' AND newsletterID='".$newsletter_sms->event->id."' AND newslettertyp='".$newsletter_sms->newslettertyp."'");

		if( $ws->id ){
			$result = FALSE;
		} else {
			$sql = "INSERT wiml_senden SET
			  club='".$_REQUEST["club"]."',
			  eventsid='".$newsletter_sms->event->event_id."',
			  datum='".current_time("timestamp")."',
			  newsletterID='".$newsletter_sms->event->id."',
			  newslettertyp='".$newsletter_sms->newslettertyp."'
			  ";
			$result = $wpdb->query( $sql );
		}
		if( $result ){
			$response['status'] = 'success';
			$response['statustext'] = __("Changes saved.");

		}else {
			$response['status'] = 'error';
			$response['statustext'] =  __("An unidentified error has occurred.");

		}
		wp_send_json( $response );
		wp_die();
	}
	/*
		fn
	*/
	public function delete_to_smslist(){
		global $wpdb;
		$newsletter_sms= new newsletter_sms;
		$result = $wpdb->delete( 'wiml_senden', array("id" => $_REQUEST["send_id"]),array("%d") );

		if( $result ){
			$response['status'] = 'warning';
			$response['statustext'] = __("Delete Selected");
		}else {
			$response['status'] = 'error';
			$response['statustext'] =  __("An unidentified error has occurred.");
		}
		wp_send_json( $response );
		wp_die();
	}
	/*
		fn
	*/
	public function update_db_smslist($anz){
		global $wpdb;
		$sql = "UPDATE wiml_maillist_newsletter SET sms='".$anz."' WHERE id='".$this->event->id."'";
		$wpdb->query( $sql );
		return $anz;
	}
	/*
		fn
	*/
	public function url()
	{
		$url = admin_url('admin.php') . '?page=' . $_REQUEST["page"] .'&action=' . $_REQUEST["action"] .'&id=' . $_REQUEST["id"];
		return $url;
	}
	/*
		fn
	*/
	public function ajax_smslist_send(){
		$newsletter_sms = new newsletter_sms;
		$newsletter_sms->smslist_send();
		wp_die();
	}
	/*
		fn
	*/
	public function ajax_sms_export_show(){
		$newsletter_sms = new newsletter_sms;
		$sql = $newsletter_sms->sms_export();
		wp_die();
	}
	/*
		fn
	*/
	public function ajax_sms_export(){
		global $wpdb;

		$newsletter_sms = new newsletter_sms;
		$sql = $newsletter_sms->export_sql();

		$fileName = ($_REQUEST["aktiv_status"]) ? $_REQUEST["aktiv_status"] .'_' : '';
		if ( $_REQUEST["club"] ){
			$fileName .= $newsletter_sms->event->id . '_' . $_REQUEST["club"]  .'_'.sanitize_title( $newsletter_sms->title ).'.csv';

		} else {
			$fileName .= $newsletter_sms->event->id .'_'.sanitize_title( $newsletter_sms->title ).'.csv';
		}


		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header('Content-Description: File Transfer');
		header('Content-Encoding: UTF-8');
		header("Content-type: text/csv; charset=UTF-8");
		header("Content-Disposition: attachment; filename=".$fileName."");
		header("Expires: 0");
		header("Pragma: public");

		$fh = @fopen( 'php://output', 'w' );

		$sql_results = $wpdb->get_results( $sql, ARRAY_A );

		/* Begin Mitarbeiter */
		if ( $_REQUEST["mitarbeiter"] ){
			$mitarbeiter_sql = "SELECT u.mobile, u.name, u.club, u.blacklist, u.aktiv_status, u.id FROM wiml_mobile u WHERE u.club='ms-mitarbeiter'";
			$mitarbeiter_results = $wpdb->get_results( $mitarbeiter_sql, ARRAY_A );
			$results = array_merge( $sql_results, $mitarbeiter_results );
		} else {
			$results = $sql_results;
		}
		/* END Mitarbeiter */

		$headerDisplayed = false;
		$delimiter = ';';
		$enclosure = '"';

		foreach ( $results as $data ) {

			$data["name"] = mb_convert_encoding($data["name"], 'UTF-16LE', 'UTF-8');
			//Replace Mobile
			$data = $newsletter_sms->export_replace_mobile( $data );
			//Replace Name
			$data = $newsletter_sms->export_replace_name( $data );

			// Add a header row if it hasn't been added yet
			if ( !$headerDisplayed ) {
				// Use the keys from $data as the titles
				fputcsv($fh, array_keys($data),$delimiter, $enclosure);
				$headerDisplayed = true;
			}

			// Put the data into the stream
			fputcsv($fh, $data, $delimiter, $enclosure);
		}
		// Close the file
		fclose($fh);
		// Make sure nothing else is sent, our file is done

		wp_die();
	}
	public function export_replace_mobile( $data ){

		if ( preg_match('/^0/', $data["mobile"]) ){
				$data["mobile"] = preg_replace("/^0/", '',$data["mobile"]);
		}

		if ( !preg_match('/^0/', $data["mobile"]) ){

			switch ( $_REQUEST["areacode"] ) {

				case "01":
					$data["mobile"] = '0' . $data["mobile"];
					break;
				case "0049":
					$data["mobile"] = '0049' . $data["mobile"];
					break;
				case "plus49":
					$data["mobile"] = '+49' . $data["mobile"];
					break;
				default:
					break;
			}
		}
		return $data;
	}
	public function export_replace_name( $data ){
		if ( strlen($data["name"]) < 3 ){
			$data["name"] = 'Berliner';
		}
		$data["name"] = trim( ucfirst($data["name"]) );
		return $data;
	}
}
?>
