<?php
/**
 *
 */
class newsletterUnsubscribe
{
  public $mydb;
  public $anz;
  public $cols;
  public $ajaxUrl;
  public $db_users = "wiml_users";
  public $db_mobile= "wiml_mobile";

  function __construct()
  {
    global $datenbank;
    $db = $datenbank[0];
    $mydb = new DB_MySQL($db['localhost'], $db['db'], $db['username'], $db['password']);
    $this->mydb = $mydb;
    $this->cols = array("post_date", "ID", "post_title", "post_content", "post_status", "email", "phone");
    $this->ajaxUrl = admin_url("admin-ajax.php") .'?action=newsletterUnsubscribe';
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
  public function display()
  {
    $data = $this->inboundList();
    $args = array(
      "postbox_class" => array("first" => "col-12"));

    $widgets = array(
      array(
        "align" => "first",
        "title" => "Abmeldungen (". $this->anz .")",
        "button" => array( array("title" => "Blacklist", "href" => "#", "id" => "newsletterUnsubscribeButton", "class" => "button-primary") ),
        "data" =>  $data),
    );
    $MSDashboard = new MSDashboard( $widgets, $args );
    echo $MSDashboard->display();
  }
  public function inboundList()
  {
    global $ms_prefix;
    $mydb = $this->mydb;
    $out  = '<table class="wp-list-table widefat striped">';
    $out .= '<thead><tr>';
    foreach( $this->cols as $col ){
      $out .= '<th class="'.$col.'">' . $col  . '</th>';
    }
    $out .= '<td>Result</td>';
    $out .= '</tr></thead><tbody>';

    $sql = "SELECT DISTINCT p.*,
    pm1.meta_value AS email,
    pm2.meta_value AS vorwahl,
    pm3.meta_value AS mobile
    FROM " . $ms_prefix . "posts p
    LEFT JOIN ".$ms_prefix."postmeta pm1 ON (
            pm1.post_id = p.ID  AND
            pm1.meta_key    = '_field_your-email')
    LEFT JOIN ".$ms_prefix."postmeta pm2 ON (
            pm2.post_id = p.ID  AND
            pm2.meta_key    = '_field_your-prefix')
    LEFT JOIN ".$ms_prefix."postmeta pm3 ON (
            pm3.post_id = p.ID  AND
            pm3.meta_key    = '_field_your-phone')
    WHERE p.post_status='publish' AND p.post_type='flamingo_inbound' AND p.post_title='Newsletter abmelden'
    GROUP BY p.ID
    ORDER BY p.ID DESC
    ";
    $unsubscribes = $mydb->get_results($sql);
    $this->anz    = count($unsubscribes);
    foreach($unsubscribes as $unsubscribe) {
      $phone='';
      $out .= '<tr>';
      foreach( $this->cols as $col ){
        switch ($col) {
          case 'phone':
            $out .= '<td class="phone">' . $unsubscribe->vorwahl  . $unsubscribe->mobile .'</td>';
            $phone = $unsubscribe->vorwahl  . $unsubscribe->mobile;
            break;

          default:
            $out .= '<td class="'.$col.'">' . $unsubscribe->$col  . '</td>';
            break;
        }
      }
      $out .= '<td class="result" data-href="'.$this->ajaxUrl.'&post_id='.$unsubscribe->ID.'&email='.$unsubscribe->email.'&mobile='.$phone.'"></td>';
      $out .= '</tr>';
    }
    $out .= '</tbody></table>';
    return $out;
  }
  public function blacklistEmail(){
    global $wpdb;
    $email = isset($_REQUEST["email"])  ? $_REQUEST["email"]  : NULL;

    if(!$email){
      return 'Keine E-Mailadresse';
    }
    $email = strtolower($email);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      return "Filter invalid";
    }
    $EmailUser = $wpdb->get_row("SELECT * FROM ".$this->db_users." WHERE email_address='".$email."'");
    if( $EmailUser->blacklist == 1 ){
      return 'Bereits auf Blacklist';
    }
    if( $EmailUser->uid ){
      $wpdb->query("UPDATE ".$this->db_users." SET blacklist=1, blacklist_datum='".current_time("Y-m-d H:i:s")."'  WHERE email_address='".$this->email."'");
      return $EmailUser->firstname .' '.$EmailUser->lastname . '<br>'.$EmailUser->email_address .' ist auf Blacklist.';
    }
    return 'E-Mailadresse existiert nicht.';
  }
  public function blacklistPhone(){
    global $wpdb;
    $mobile = isset($_REQUEST["mobile"])  ? $_REQUEST["mobile"] : NULL;
    if(!$mobile){
      return 'Keine Phone';
    }
    $MSMobileCheckActions = new MSMobileCheckActions;
    $netz = $MSMobileCheckActions->netz();
    if ( !$netz ){
      return 'Keine Handynummer';
    }
    $mobile = $MSMobileCheckActions->checkVorwahl();
    $row = $wpdb->get_row("SELECT * FROM ".$this->db_mobile." WHERE mobile='".$mobile."'");
    if ( $row->id ){
      $wpdb->query("UPDATE ".$this->db_users." SET blacklist=1, blacklist_datum='".current_time("Y-m-d H:i:s")."'  WHERE mobile='".$mobile."'");
      return $row->mobile .' ist auf Blacklist.';
    }
    return 'Mobil existiert nicht.';
  }
  public function ajax_newsletterUnsubscribe(){
    global $ms_prefix;

    $newsletterUnsubscribe = new newsletterUnsubscribe;
    $statustext = $newsletterUnsubscribe->blacklistEmail() .'<br>';
    $statustext .= $newsletterUnsubscribe->blacklistPhone();

    $mydb = $newsletterUnsubscribe->mydb;
    $sql = "UPDATE " . $ms_prefix . "posts SET post_status='trash' WHERE ID='".$_REQUEST["post_id"]."'";
    $mydb->query($sql);
    wp_send_json( array("statustext" => $statustext) );
    wp_die();
  }
}

?>
