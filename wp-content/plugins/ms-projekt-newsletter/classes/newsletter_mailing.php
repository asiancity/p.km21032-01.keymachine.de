<?php
class newsletter_mailing
{
	public $event;
	public $title;
	public $newslettertyp = 'email';

	/*
		fn
	*/
	public function __construct(){
		global $wpdb;
		if(!$_REQUEST["id"]){
		  return '';
		}
		$event = $wpdb->get_row("select * from wiml_maillist_newsletter WHERE id='".$_REQUEST["id"]."'");
		$this->event = $event;
		$this->title	= date_i18n( get_option( 'date_format' ), strtotime( $event->event_datum ) ) . ' - ' . apply_filters("the_title", $event->name);
	}
	/*
		fn
	*/
	public function deactived( $alias='' ){
		$alias = ($alias) ? $alias .'.' : '';
		return $alias . 'blacklist=0 AND '.$alias .'active=1';
	}
	/*
		fn
	*/
	public function mailinglist_sort_club_sql(){
		global $wpdb;

		$where	= 'WHERE ' . $this->deactived();

		$sql	= "select club, count(DISTINCT email_address) as 'anz' from wiml_users ".$where." GROUP BY club";
		if ( ! empty( $_REQUEST['orderby'] ) ) {

			$sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
			$sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';

		} else {

			$sql .= ' ORDER BY anz desc ';
		}

		return $wpdb->get_results($sql);
	}
	/*
		fn
	*/
	public function mailinglist_sort_club_display(){
		$clubs = $this->mailinglist_sort_club_sql();
		$gesamt = 0;
	?>
    <table width="100%" class="wp-list-table fixed striped widefat">
    	<thead>
        <tr>
        	<th><a href="<?php echo $this->url() ?>&orderby=club&order=ASC">Club</a></th>
            <th><a href="<?php echo $this->url() ?>&orderby=anz&order=DESC">Anzahl</a></th>
            <th><?php _e("Add Items") ?></th>
        </tr>
        </thead>
    	<tbody>
    	<?php foreach( $clubs as $club ) : ?>
        <tr>
        	<td><?php echo $club->club ?></td>
            <td><?php echo $club->anz ?></td>
            <td align="center"><a href="#" data-href="<?php echo admin_url("admin-ajax.php")?>?action=newsletter_add_to_mailinglist&id=<?php echo $this->event->id ?>&club=<?php echo $club->club ?>" class="button"><span class="dashicons dashicons-plus"></span></a></td>
        </tr>
		<?php
		$gesamt += $club->anz;
		endforeach; ?>
    	</tbody>
        <tfoot>
            <tr><td>Gesamt:</td><td><?php echo $gesamt; ?></td><td></td></tr>
        </tfoot>
    </table>
    <?php
	}
	/*
		fn
	*/
	public function mailinglist_club_anzahl(){
		$data = array();
		$clubs = $this->mailinglist_sort_club_sql();
		foreach( $clubs as $club ) :
		  $data[$club->club] = $club->anz;
		endforeach;
		return $data;
	}
	/*
		fn
	*/
	public function mailinglist_send_sql(){
		global $wpdb;

		$where	= 'WHERE newslettertyp="'.$this->newslettertyp.'" AND newsletterID="'.$this->event->id.'"';
		$sql	= "select * from wiml_senden ".$where." GROUP BY club";
		if ( ! empty( $_REQUEST['orderby'] ) && $_REQUEST['orderby'] != "anz") {

			$sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
			$sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';

		} else {

			$sql .= ' ORDER BY club asc ';
		}

		return $wpdb->get_results($sql);
	}
	/*
		fn
	*/
	public function mailinglist_send(){

		$clubs = $this->mailinglist_send_sql();
		$clubsAnzahl = $this->mailinglist_club_anzahl();
		$gesamt = 0;
  		?>
        <table width="100%" class="wp-list-table fixed striped widefat">
            <thead>
                <tr>
                    <th><a href="<?php echo $this->url() ?>&orderby=club&order=ASC">Club</a></th>
                    <th>Anzahl</th>
                    <th><?php _e("Add Items") ?></th>
                </tr>
            </thead>
            <tbody>
				<?php foreach( $clubs as $club ) : ?>
                <tr>
                    <td><?php echo $club->club ?></td>
                    <td><?php echo $clubsAnzahl[$club->club] ?></td>
                    <td align="center" class="delete"><a href="#" data-href="<?php echo admin_url("admin-ajax.php")?>?action=newsletter_delete_to_mailinglist&id=<?php echo $this->event->id ?>&send_id=<?php echo $club->id ?>" class="button"><span class="dashicons dashicons-minus"></span></a></td>
                </tr>
                <?php $gesamt += $clubsAnzahl[$club->club];
                endforeach; ?>
            </tbody>
            <tfoot>
            	<tr><td>Gesamt:</td><td><?php echo $this->update_db_maillist_newsletter($gesamt); ?></td><td></td></tr>
            </tfoot>
        </table>
	<?php
    }
	/*
		fn
	*/
    public function sql_supermailer(){
		global $wpdb;
		$sql = "SELECT u.email_address,u.fullname, u.club, u.blacklist, u.active FROM wiml_senden ws LEFT JOIN wiml_users u ON ws.club=u.club WHERE ".$this->deactived("u")." AND ws.newslettertyp='".$this->newslettertyp."' AND ws.newsletterID='".$this->event->id."'";
		echo '<p>Anzahl: ' . count( $wpdb->get_results($sql) ) .'</p>';
		echo '<p><textarea style="width:100%;height:150px;">' . $sql .'</textarea></p>';
    }
	/*
		fn
	*/
    public function display(){
    ?>
        <h3><?php echo $this->title ?></h3>
        <div id="dashboard-widgets" class="metabox-holder">
            <div id="postbox-mailinglist-groupby-club" class="postbox-container">
                <div class="meta-box-sortables ui-sortable">
                    <div class="postbox">
                        <h2 class="hndle ui-sortable-handle">Mailinglist (sortiert nach Club)</h2>
                        <div class="inside"><?php $this->mailinglist_sort_club_display() ?></div>
                    </div>
                </div>
            </div>
            <div class="postbox-container">
                <div id="postbox-mailinglist-send" class="meta-box-sortables ui-sortable">
                    <div class="postbox">
                        <h2 class="hndle ui-sortable-handle">Send Mailinglist</h2>
                        <div class="inside" data-href="<?php echo admin_url("admin-ajax.php") ?>?action=newsletter_mailing_send&id=<?php echo $this->event->id ?>"><?php $this->mailinglist_send() ?></div>
                    </div>
                </div>
                <div id="postbox-mailinglist-sql-supermailer" class="meta-box-sortables ui-sortable">
                    <div class="postbox">
                        <h2 class="hndle ui-sortable-handle">SQL Supermailer</h2>
                        <div class="inside" data-href="<?php echo admin_url("admin-ajax.php") ?>?action=newsletter_sql_supermailer&id=<?php echo $this->event->id ?>"><?php $this->sql_supermailer() ?></div>
                    </div>
                </div>
            </div>
        </div>
    <?php
	}
	/*
	fn
	*/
	public static function add_to_mailinglist(){
		global $wpdb;
		if(!isset($_REQUEST["club"]) && !$_REQUEST["club"]){
		  return '';
		}
		$newsletter_mailing= new newsletter_mailing;

		$ws = $wpdb->get_row("SELECT * FROM wiml_senden WHERE club='".$_REQUEST["club"]."' AND newsletterID='".$newsletter_mailing->event->id."' AND newslettertyp='".$newsletter_mailing->newslettertyp."'");
		if( $ws ){
			$result = FALSE;
		} else {
			$sql = "INSERT wiml_senden SET
			  club='".$_REQUEST["club"]."',
			  eventsid='".$newsletter_mailing->event->event_id."',
			  datum='".current_time("timestamp")."',
			  newsletterID='".$newsletter_mailing->event->id."',
			  newslettertyp='".$newsletter_mailing->newslettertyp."'
			  ";
			$result = $wpdb->query( $sql );
		}
		if( $result ){
			$response['status'] = 'success';
			$response['statustext'] = __("Changes saved.");

		}else {
			$response['status'] = 'error';
			$response['statustext'] =  __("An unidentified error has occurred.");

		}
		wp_send_json( $response );
		wp_die();
	}
	/*
		fn
	*/
	public static function delete_to_mailinglist(){
		global $wpdb;
		$newsletter_mailing= new newsletter_mailing;
		$result = $wpdb->delete( 'wiml_senden', array("id" => $_REQUEST["send_id"]),array("%d") );

		if( $result ){
			$response['status'] = 'warning';
			$response['statustext'] = __("Delete Selected");
		}else {
			$response['status'] = 'error';
			$response['statustext'] =  __("An unidentified error has occurred.");
		}
		wp_send_json( $response );
		wp_die();
	}
	/*
		fn
	*/
	public function update_db_maillist_newsletter($anz){
		global $wpdb;
		$sql = "UPDATE wiml_maillist_newsletter SET anzahlEmails='".$anz."' WHERE id='".$this->event->id."'";
		$wpdb->query( $sql );
		return $anz;
	}
	/*
		fn
	*/
	public static function ajax_mailinglist_send(){
		$newsletter_mailing = new newsletter_mailing;
		$newsletter_mailing->mailinglist_send();
		wp_die();
	}
	/*
		fn
	*/
	public static function ajax_sql_supermailer(){
		$newsletter_mailing = new newsletter_mailing;
		$newsletter_mailing->sql_supermailer();
		wp_die();
	}
	/*
		fn
	*/
	public function url(){
		$url = admin_url('admin.php') . '?page=' . $_REQUEST["page"] .'&action=' . $_REQUEST["action"] .'&id=' . $_REQUEST["id"];
		return $url;
	}
}
?>
