<?php
class newsletter_edit_template
{
	public $event;
	public $title;

	public function __construct(){
		global $wpdb;

    $_template_name  = isset($_POST["template_name"])  ? $_POST["template_name"] : NULL;
    $_id             = isset( $_REQUEST["id"] )        ? $_REQUEST["id"]         : NULL;
		if ( !$_REQUEST["id"] ){
			return '';
		}
		if( $_template_name ){
        	$wpdb->query("UPDATE wiml_maillist_newsletter SET template_name='".$_template_name."' WHERE id='".$_id."'");
   		}
		add_thickbox();

		$event			= $wpdb->get_row("select * from wiml_maillist_newsletter WHERE id='".$_id."'");
    $this->event	= $event;
		$this->title	= date_i18n( get_option( 'date_format' ), strtotime( $event->event_datum ) ) . ' - ' . apply_filters("the_title", $event->name);

	}
	public function display(){
		echo '<h3>Event: ' . $this->title .'</h3>';
		if ( $this->event->template_name == "" ){
			$this->template_selected();
		} else {
			$this->template_edit();
		}
	}
	public function template_selected(){
		global $wpdb;

		$tenplates   =  $wpdb->get_results("select * from wiml_maillist_newsletter_css"); ?>
		<form method="post">
			 <select name="template_name">
				<option>Template auswählen</option>
				<?php  foreach ( $tenplates as $tenplate) :?>
					<option value="<?php echo $tenplate->template_name ?>"><?php echo $tenplate->template_name ?></option>
				<?php endforeach; ?>
			 </select>
			 <?php submit_button( 'Speichern', 'primary', false, false, array( 'id' => 'post-query-submit' ) ); ?>
		</form>
         <?php
	}
	public function template_edit(){
		global $wpdb;
		echo '<h3>Template: ' . $this->event->template_name .'</h3>';
		$vorschau_url       = ATH_NEWLETTER_URL .'/' . $_REQUEST["id"] .".html";

		?>
        <table width="100%">
        	<tr>
                <td valign="top" width="100%">
                    <form method="post">
                        <input type="hidden" name="newsletter_erstellen" value="true" />

                        <?php
						            $content = $this->textfeld();
						            wp_editor( $content, 'description', array("editor_height" => 500, "textarea_rows" => 50, 'textarea_name' => 'description' ) ); ?>
                        <p>
                            <?php submit_button( __( 'Save'), 'primary', false, false, array( 'id' => 'post-query-submit' ) ); ?>

                            <a target="_blank" class="button" href="http://support.google.com/googleanalytics/bin/answer.py?hl=de&answer=55578">Google Kampagnenlinks erstellen</a>
                            <a  class="button" id="html-view" style="margin-right:10px" href="<?php echo $vorschau_url ?>" >Vorschau</a>
                        </p>
                    </form>
                    <?php  $tenplates   =  $wpdb->get_results("select * from wiml_maillist_newsletter_css"); ?>
               	<form method="post" style="float:right;">
                   <select name="template_name" >
                      <option>Template auswählen</option>
                      <?php  foreach ( $tenplates as $tenplate) :?>
                          <option value="<?php echo $tenplate->template_name ?>" <?php echo ($tenplate->template_name == $this->event->template_name) ? 'selected' : '' ?>><?php echo $tenplate->template_name ?></option>
                      <?php endforeach; ?>
                   </select>
                   <?php submit_button( __( 'Save' ), 'primary', false, false, array( 'id' => 'post-query-submit' ) ); ?>
                  </form>

                    <table width="100%" class="wp-list-table widefat">
                        <tr><th><h3>Urls im Newsletter</h3></th></tr>
                        <tr>
                            <td valign="top" class="report_urls"></td>
                        </tr>
                    </table>
                    <br><br>
                    <table width="100%" class="wp-list-table widefat">
                        <?php $layouts = $this->templates();  ?>
                        <?php foreach ( $layouts as $layout) :?>
                            <tr><td><a href="<?php echo ATH_PLUGIN_URL . 'templates/' . $layout["datei"]?>" target="_blank" ><?php echo $layout["datei"]?></a></td></tr>
                        <?php endforeach; ?>
                    </table>
                </td>
            </tr>
        </table>
        <?php
	}

	public function textfeld(){
    global $wpdb;
		$url = $this->datei_erstellen();
		$this->msg = $this->datei_save($url);
    $id         = isset($_REQUEST["id"]) ?  $_REQUEST["id"] : 0;
		//return $url;
		if (file_exists($url)){
      $sql       = "select n.* from wiml_maillist_newsletter n WHERE n.id='".$id."'";
      $tenplate  =  $wpdb->get_row( $sql );
			return $tenplate->template_content;
		}
		return '';
	}
	/*
		fn
	*/
	public function datei_save($url){
    global $wpdb;

    $description            = '';
    $id                     = '';
    $newsletter_erstellen   = '';
    $data                   = array();
    extract( $_REQUEST );
		if ($newsletter_erstellen){

      $data["template_content"] = stripslashes($description);
      $where["id"]              = $id;
      $format[]                 = "%s";
      $where_format[]           = "%d";
      $wpdb->update("wiml_maillist_newsletter", $data, $where, $format, $where_format);

      $ordner    = $url;
      $sql       = "select n.name, n.template_content, nc.* from wiml_maillist_newsletter n
                    LEFT JOIN wiml_maillist_newsletter_css nc ON n.template_name=nc.template_name
                    WHERE n.id='".$id."'";
                    // var_dump( $sql);
      $tenplate  =  $wpdb->get_row( $sql );

      //print_r ($_REQUEST["description"] );

      $text      = sprintf($tenplate->header, $tenplate->name, $tenplate->css_content);
      // $text     .= apply_filters("the_content", $tenplate->template_content);
      $text     .= $tenplate->template_content;
      $text     .= $tenplate->footer;

      $textdatei = fopen($ordner, "w");
			fwrite($textdatei, $text);
			fclose($textdatei);
			return 'gespeichert';
		}
	}

	/*
		fn
	*/
	public function datei_erstellen(){

		$pfad  = ATH_NEWLETTER_PATH;
    $id    = isset($_REQUEST["id"]) ? $_REQUEST["id"] : NULL;
		$datei = ($id) ? $id.'.html' : 'hallo.html';

		if (!file_exists($pfad.'/'.$datei)){
			$ordner = $pfad.'/'.$datei;
			$text = 'datei erstellt';
			$textdatei = fopen ($ordner, "w");
			//chmod($ordner, 0777);
			fwrite($textdatei, $text);
			fclose($textdatei);
		}
		return $pfad.'/'.$datei;
	}
	/*
		fn
	*/
	static function newsletter_add_editor_styles() {
		global $wpdb;

		if(isset($_REQUEST['action']) && isset($_REQUEST['page']) && $_REQUEST["action"] == "edit" && $_REQUEST["page"] == "ath-newsletter"){
			remove_editor_styles();
			$newsletter_edit_template = new newsletter_edit_template;
			$event = $newsletter_edit_template->event;
			if( $event->template_name ){
				add_editor_style( ATH_PLUGIN_URL . 'css/'.$event->template_name.'.css' );
			} else {
				add_editor_style( ATH_PLUGIN_URL . 'css/default_adagio.css' );
			}
		}
	}
	public function templates(){

		$pfad = ATH_PLUGIN_PATH.'templates/';
		$handle=opendir ($pfad);

		while ($datei = readdir ($handle)) {
			if(strlen($datei) > 2){
				$pfadDatei = $pfad.$datei;
				$files[] = $datei;
			}
		}
		closedir($handle);
		$datei = "";

		krsort($files);
		foreach($files as $datei) {
			if(strlen($datei) > 2){
				$pfadDatei = $pfad.$datei;
				$zeilen = file( $pfad.$datei );
				$anz = count($zeilen);

				$data[] = array(
		 			"datei" => $datei,
					"datum" => (strlen($datei) > 2) ? date("d.m.Y - H:i:s", filemtime($pfadDatei)) : '',
					"anzahl" => $anz
				);
			}
		}

		return $data;
	}
}
?>
