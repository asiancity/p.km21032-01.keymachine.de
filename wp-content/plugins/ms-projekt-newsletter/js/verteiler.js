jQuery(function($){
	var item_length = 100;
	var pause 		= 100;
	var gesamt 		= $(".export_list .displaying-num:eq(0) i").text();
	var render 		= Math.ceil(parseInt(gesamt)/parseInt(item_length))+2;
	$("#gesamteAnzahl").html( gesamt );
	$("#render").html( render );
	$("#dauer").html( Math.ceil((render*parseInt(pause))/3600) + 's' );
	if( $(".download").length > 0 ) {
			var prozent = 100;
			$("#progress .pb_text").text( prozent+'%' ).css({"left" : prozent+'%'});
			$("#progress .pb_before").width( prozent+'%' );	
	}

	$("body").on( "click", ".erstellen", function(){
			
			$(".verteiler_sql").load("/wp-admin/admin-ajax.php?action=verteilerlist_sql&newsletterID="+$("#newsletterID").text());
			var url = "/wp-admin/admin-ajax.php?action=verteilerlist_daten&newsletterID="+$("#newsletterID").text()+"&item_length="+item_length+"&item_start="+ (  parseInt( $("#anzahl").text() )*parseInt(item_length) );
			var html = '';					
			var anzahl = "";
			var rest, prozent;
			
			$.getJSON( url , function( data ) {
					$.each( data, function( key, val ) {
						html += '\r';
						html += val.firstname + ';';
						html += val.email + ';';
						html += val.lastname + ';';
						html += val.name + ';';
						html += val.id + ';';
						html += val.blacklist + ';';
						html += val.active + ';';
						
					});
			}).done(function() {
				anzahl 	= parseInt( $("#anzahl").text() );
				rest 	= Math.ceil( (parseInt(gesamt)/100) );
				$("#count_gesamteAnzahl").text( parseInt(item_length)*anzahl );
				prozent = Math.ceil((anzahl/parseInt(rest))*100);
				$("#timing").html( "/ " + Math.ceil((anzahl*parseInt(pause))/3600) + 's' );
				
				if ( anzahl == 0 ){
					$.getJSON( "/wp-admin/admin-ajax.php?action=verteilerlist_datei_erstellen&newsletterID="+$("#newsletterID").text() );
				}
				
				$("#export_data").prepend( html );
				//$("#progress").html( '<div style="background:#ff0000; height: 20px; width: '+prozent+'%;">'+prozent+'%</div>' );
				$("#progress .pb_text").text( prozent+'%' ).css({"left" : prozent+'%'});
				$("#progress .pb_before").width( prozent+'%' );
				
				if ( prozent >= 100 ){
					setTimeout( function(){
						$("#export_fields .button")[0].click();
					}, 200);
				} else { 
					$("#anzahl").text( anzahl+1 );
					$(".erstellen")[0].click();	
				}
			}).fail(function() {
				$(".erstellen")[0].click();	
			});
		return false;
	});

});