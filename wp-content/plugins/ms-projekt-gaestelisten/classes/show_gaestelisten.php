<?php
class show_gaestelisten{

	protected $data = '';
	protected $event = '';
	/*
		fn
	*/
	public function __construct()
	{
		global $wpdb;

		if( !isset($_GET["newsletterID"]) || !$_GET["newsletterID"] ){
			wp_die();
		}
		if( !isset($_GET["api_key"]) || !$_GET["api_key"] ){
			wp_die();
		}
		$events_sql = "select * from wiml_maillist_newsletter WHERE id = '".$_GET["newsletterID"]."'";
		$this->event = $wpdb->get_row($events_sql);
		$api_key = md5( $this->event->event_datum .'-'.$this->event->event_id);
		if( $_GET["api_key"] != $api_key){
			wp_die();
		}

		if( $_GET["importdatum"] )
		{
			$data = $wpdb->get_results("select * from wiml_gaesteliste WHERE nid = '".$_GET["newsletterID"]."' AND  importdatum='".$_GET["importdatum"]."' order by firstname", ARRAY_A);
		} else {
			$data = $wpdb->get_results("select * from wiml_gaesteliste WHERE nid = '".$_GET["newsletterID"]."'  order by firstname", ARRAY_A);
		}
		foreach ($data as $s){
		  	$new_data[]=array( "firstname" => $s["firstname"], "lastname" => $s["lastname"], "plus" => $s["plus"], "aktion" => (($s["aktion"])?$s["aktion"] : 'Aktion'), "importdatei" => $s["importdatei"]);
		}
		$this->data = msprint::array_multi_unique( $new_data);

	}
	/*
		fn
	*/
	public function display()
	{
		global $wpdb;
		$event = $this->event;
		$this->export();
		$page_title = date_i18n("l, d.m.Y", strtotime($event->event_datum) ) .' | '. $event->name;
		?>
        <html>
        <head>
        <title><?php echo $page_title ?></title>
        <link rel='stylesheet' href="<?php echo plugin_dir_url( __FILE__ ) . '../css/style.css' ?>" type='text/css' media='all'/>
        </head>
        <body class="gaesteliste_show_print">
        <div class="actions">
        	<a href="<?php echo admin_url('admin-ajax.php') ?>?action=<?php echo $_REQUEST["action"] ?>&newsletterID=<?php echo $_REQUEST["newsletterID"] ?>&api_key=<?php echo $_REQUEST["api_key"] ?>&export=csv" class="button">Export CSV</a>
            <a href="<?php echo admin_url('admin-ajax.php') ?>?action=<?php echo $_REQUEST["action"] ?>&newsletterID=<?php echo $_REQUEST["newsletterID"] ?>&api_key=<?php echo $_REQUEST["api_key"] ?>&export=xls" class="button">Export XLS</a>
            <a href="<?php echo admin_url('admin-ajax.php') ?>?action=<?php echo $_REQUEST["action"] ?>&newsletterID=<?php echo $_REQUEST["newsletterID"] ?>&api_key=<?php echo $_REQUEST["api_key"] ?>&export=pdf" class="button">Export PDF</a>
        	<div id="print" class="button" onClick="javascript:window.print();">Seite ausdrucken</div>
        </div>
        <h2><?php echo $page_title ?></h2>
        <h3>Aktion: <?php echo $event->aktion ?></h3>
        <h4>Anzahl: <?php echo count($this->data) ?></h4>
		<table>
		<thead>
    	<tr class="topic">
        <td class="nr"></td>
        <td>Vorname</td>
        <td>Nachname</td>
        <td>Plus</td>
        <td>Aktion</td>
        <td>Support</td>
			</tr>
    </thead>
		<tbody>
		<?php
		if( is_array( $this->data )){
			  $i = 1;
			  foreach ($this->data as $r){
			  ?>
        <tr>
          <td class="nr"><?php echo $i ?></td>
          <td><?php echo $r["firstname"]?></td>
          <td><?php echo $r["lastname"]; ?></td>
          <td><?php echo $r["plus"]; ?></td>
          <td><?php echo $r["aktion"]; ?></td>
          <td><?php echo $r["importdatei"]; ?></td>
        </tr>
			  <?php
			  $i++;
			  }
		}
		?>
        </tbody>
		</table>
        </body>
        </html>
		<?php
	}
	public function export(){
		$export = new export( $this->event, $this->data );
		if( $_REQUEST["export"] ){
			if( $_REQUEST["export"] == "xls"){
				$file = $export->xls_erstellen();
			}
			if( $_REQUEST["export"] == "pdf"){
				$file = $export->pdf_erstellen();
			}
			if( $_REQUEST["export"] == "csv"){
				$file = $export->csv_erstellen();
			}
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'.basename($file).'"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			readfile($file);
			wp_die();
		}
	}
}
?>
