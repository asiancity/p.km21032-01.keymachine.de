<?php
class upload_show extends WP_List_Table {
	public $url = '';

	public function __construct(){
        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'movie',     //singular name of the listed records
            'plural'    => 'movies',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );
		$this->url = meineGaesteliste::url();
	}
	function column_data(){
		$upload_dir = wp_upload_dir();
		$pfad = $upload_dir['basedir'].'/gaestelisten/';

		$handle=opendir ($pfad);
		while ($datei = readdir ($handle)) {
			if(strlen($datei) > 2){
				$pfadDatei = $pfad.$datei;
				$files[filemtime($pfadDatei)] = $datei;
			}
		}
		closedir($handle);
		$datei = "";

		krsort($files);
		foreach($files as $datei) {
			if(strlen($datei) > 2){
				$pfadDatei = $pfad.$datei;
				$zeilen = file( $pfad.$datei );
				$anz = count($zeilen)-1;

				$data[] = array(
		 			"datei" => $datei,
					"datum" => (strlen($datei) > 2) ? date("d.m.Y - H:i:s", filemtime($pfadDatei)) : '',
					"anzahl" => $anz,
					"show" => '<a  class="button" href="'.$this->url.'&show_uploaded_file=true&file='.$datei.'#postbox-3-2">anzeigen</a>'
				);
			}
		}

		return $data;
	}
	function get_columns(){
        $columns = array(
            //'cb'        => '<input type="checkbox" />', //Render a checkbox instead of text
            'datei'     => 'Dateiname',
						'datum' => 'Datum',
						'anzahl' => 'Anzahl',
						'show' => ''
        );
        return $columns;
    }
	 function column_default($item, $column_name){
        switch($column_name){
						case 'datum':
						case 'datei':
						case 'show':
						case 'anzahl':
                return $item[$column_name];
            default:
                return print_r($item,true); //Show the whole array for troubleshooting purposes
        }
    }

	function prepare_items() {

        /**
         * First, lets decide how many records per page to show
         */
        $per_page = 20;

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = array($columns, $hidden, $sortable);
        $data = $this->column_data();
        $current_page = $this->get_pagenum();
        $total_items = count($data);

        $data = array_slice($data,(($current_page-1)*$per_page),$per_page);
        $this->items = $data;

        $this->set_pagination_args( array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil($total_items/$per_page)   //WE have to calculate the total number of pages
        ) );
    }
}
?>
