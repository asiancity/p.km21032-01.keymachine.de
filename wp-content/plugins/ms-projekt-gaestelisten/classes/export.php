<?php
class export{

	private $event		= '';
	private $bookings	= '';
	private $path		= '';

	function __construct( $event='', $bookings='' ) {

		$this->event	= $event;
		$this->bookings	= $bookings;
		$this->path		= get_home_path() .'/tmp';
		$this->delete_files_in_tmp();
	}
	function eventname(){
		$event		= $this->event;
		return $event->event_name . ' am ' .  date_i18n( get_option( 'date_format' ), strtotime( $event->event_start_date ) );
	}
	function filename(){
		$event		= $this->event;
		return  $event->event_start_date . '_' . sanitize_title($event->event_name)  . '_e' . $event->event_id;
	}
	function get_data(){

		return $this->bookings;
	}
	function csv_erstellen(){
    $_col            = isset($_REQUEST["col"]) ? $_REQUEST["col"] : NULL;
		$event		= $this->event;
		$filename	= $this->filename() .'.csv';
		$path		= $this->path;

		$fp 		= fopen( $path. '/' .$filename, 'w');
		$data		= $this->get_data();
		$title		= ($_col == 'user_email') ? array("Vorname" => 1, "Nachname" => 2, "Plus" => 3, "Aktion" => 4, "Support" => 5, "E-Mail" => 6) : array("Vorname" => 1, "Nachname" => 2, "Plus" => 3, "Aktion" => 4, "Support" => 5);
		//echo '<table>';
		if( is_array( $data )){
			fputcsv($fp, array_keys($title));
			foreach ($data as $colname => $valname) {
				fputcsv($fp, $valname);
				//echo '<tr><td>'.$valname["first_name"].'</td><td>'.$valname["last_name"].'</td></tr>';
			}
		}
		//echo '</table>';
		fclose($fp);
		return  $path. '/' .$filename;
	}
	function xls_erstellen(){
    $_col            = isset($_REQUEST["col"]) ? $_REQUEST["col"] : NULL;
		$event		= $this->event;
		$filename	= $this->filename() .'.xls';
		$path		= $this->path;
		$fp 		= fopen( $path. '/' .$filename, 'w');
		$data		= $this->get_data();
		$title		= ($_col == 'user_email') ? array("Vorname" => 1, "Nachname" => 2, "Plus" => 3, "Aktion" => 4, "Support" => 5, "E-Mail" => 6) : array("Vorname" => 1, "Nachname" => 2, "Plus" => 3, "Aktion" => 4, "Support" => 5);

		$sub_string = '';
    	$str = '<table border=1><tr>';
		foreach($title as $field => $val) {
			  $str .= '<th>' . $field . '</th>';
		}
		$str .= '</tr>';
		fputs ($fp, $str);
		$str = '';
		$matches = array('[Vorname]', '[Nachname]', '[Plus]', '[Aktion]', '[Support]');
		foreach ($data as $colname => $valname) {

		  	$str = '<tr><td>' . $valname["firstname"] . '</td><td>' .$valname["lastname"]. '</td><td>' .$valname["plus"] . '</td><td>' .$valname["aktion"] . '</td><td>' .$valname["importdatei"] . '</td>'.(($_col == "user_email") ? '<td>'. $valname["user_email"] .'</td>': '').'</tr>';
			fputs ($fp, $str);
		}
		$str = '</table>';
		fputs ($fp, $str);
		fclose($fp);
		return  $path. '/' .$filename;
	}
	function pdf_erstellen(){
		require_once('library/html2pdf/html2pdf.class.php');

    $_col            = isset($_REQUEST["col"]) ? $_REQUEST["col"] : NULL;
		$bookings 	= $this->bookings;
		$event		= $this->event;
		$filename	= $this->filename() .'.pdf';
		$path		= $this->path;
		$data		= $this->get_data();
		$title		=   ($_col == 'user_email') ? array("Vorname" => 1, "Nachname" => 2, "Plus" => 3, "Aktion" => 4, "Support" => 5, "E-Mail" => 6) : array("Vorname" => 1, "Nachname" => 2, "Plus" => 3, "Aktion" => 4, "Support" => 5);

		if( is_array( $data )){
			$content = '<style type="text/css">';
   			$content .= 'td{border-bottom: 1px solid #111;} th{text-align:left;} td,th{padding-right:30px;}';
			$content .= '</style>';

			$content .= '<page>';
			$content .= '<table cellpadding="10" cellspacing="0"><tr>';
			foreach($title as $field => $val) {
				  $content .= '<th>' . $field . '</th>';
			}
			$content .= '</tr>';
			foreach ($data as $colname => $valname) {
					$content .= '<tr style="border-bottom:1px solid #111;"><td>' . ( $valname["firstname"] ) . '</td><td>' . ($valname["lastname"]) . '</td><td>' . ($valname["plus"]) . '</td><td>' . ($valname["aktion"]) . '</td><td>' .$valname["importdatei"] . '</td>'.(($_col == "user_email") ? '<td>'. $valname["user_email"] .'</td>': '').'</tr>';
			}
			$content .= '</table>';
			$content .= '</page>';
		}
		$html2pdf = new HTML2PDF('P','A4','de', true, 'UTF-8');
		$html2pdf->pdf->SetDisplayMode('real');
    	$html2pdf->WriteHTML($content, false);
    	$html2pdf->Output($path. '/' .$filename, "F");
		return  $path. '/' .$filename;
	}
	function delete_files_in_tmp(){

		if ($handle = opendir($this->path))
   		{
			while (false !== ($file = readdir($handle)))
			{
				if ($file != "." || $file != ".." || $file != ".hidden")
				{
				   $file   = $this->path.'/'.$file;
				   if(is_file($file)) {
					   unlink( $file );
				   }
				}
			}

			closedir($handle);
		}
	}
	function gl_array_unique($array) {
		if( is_array( $array ) ){
			foreach ($array as $k=>$na)
				$new[$k] = serialize($na);
			$uniq = array_unique($new);
			foreach($uniq as $k=>$ser)
				$new1[$k] = unserialize($ser);
			return ($new1);
		}
	}

}
?>
