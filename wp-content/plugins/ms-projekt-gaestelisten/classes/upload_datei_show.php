<?php
class upload_datei_show {

	public $url = '';
	public function __construct()
	{
		$this->url =  meineGaesteliste::url();
	}
	function data(){

		if (file_exists( $this->datei() )){

			$byte = fopen($this->datei(), "r");
			$tech = array();
			$anzahl_zeile = 0;
			$i = 1;
			$text = "";

			while ($data = fgetcsv ($byte, 1000, ";"))
			{
				$content = $data;

				$anzahl_spalte = 0;
				foreach($content as $ta){
						$sp[$anzahl_zeile][$anzahl_spalte] =  $ta;
						$anzahl_spalte++;
				}
				$anzahl_zeile++;
			}
			return $sp;
		}
	}
	function show(){
		global $wpdb;

		if($_GET["show_uploaded_file"] && $_GET['file']){
			$zeilen = file( $this->datei() );
			$anz = count($zeilen)-1;
			$data = $this->data();

			$output .= '<form action="'.$this->url.'">';
			$output .= '<input type="hidden" name="page" value="'.$_GET['page'].'">';
			$output .= '<input type="hidden" name="view" value="'.$_GET['view'].'">';
			$output .= '<input type="hidden" name="newsletterID" value="'.$_GET['newsletterID'].'">';
			$output .= '<input type="hidden" name="eventtime" value="'.$_GET['eventtime'].'">';
			$output .= '<input type="hidden" name="event_id" value="'.$_GET['event_id'].'">';
			$output .= '<input type="hidden" name="show_uploaded_file" value="'.$_GET['show_uploaded_file'].'">';
			$output .= '<input type="hidden" name="file" value="'.$_GET['file'].'">';

			$output .= '<TABLE BORDER="0" width="100%">';
			/* <td><input type="submit" name="delete" value="Löschen" class="button button-primary"></td> */

			$output .= '<tr>';
			$op_array = array('','Vorname', 'Nachname', 'Plus');
			foreach ($data[1] as $k1 => $sp1){

				$output .= '<td>';
				$output .= '<select name="opt[]">';
				foreach($op_array as $i => $op){
					$output .= '<option value="'.$op.'" '.(( ($k1 == 0 && $op == 'Vorname') || ($k1 == 1 && $op == 'Nachname')) ? 'selected' : '' ).'>'.$op.'</option>';
				}
				$output .= '</select>';
				$output .= '</td>';
			}
			$output .= '<td><input type="submit" name="save_uploaded_file" value="speichern" id="save_uploaded_file" class="button button-primary" data-redirect="'.$this->url.'"></td>';
			$output .=  "</TR>";
			$output .= '</form>';
			// alle anzeigen

			$time = time();
			$time = DATE("Y-m-d H:i:s" ,$time);
			$p = 0;

			/* remove first row */
			if($_GET["save_uploaded_file"] == "speichern")
				unset($data[0]);

			foreach ($data as $z){
				$output .= '<tr>';

						foreach($z as $k => $sp) {
							$output .= '<td>' .$sp.$sa[$k].'</td>';

						}
						if($_GET["save_uploaded_file"] == "speichern"){

							$cols = $_GET["opt"];
							foreach( $cols as $m => $op){
								$vorname	= '';
								$nachname	= '';
								switch ($op) {
										case 'Vorname':
											$update = 'firstname="'.$z[$m].'"';
											$vorname = $z[$m];
											break;
										case 'Nachname':
											$update .= ($update) ? ',' : '';
											$update .= ' lastname="'.$z[$m].'"';
											$nachname = $z[$m];
											break;
										case 'Plus':
											$update .= ($update) ? ',' : '';
											$update .= ' plus="'.$z[$m].'"';
											$plus = $z[$m];
											break;
								}
							}
							if($vorname || $nachname){

									$sql = 'INSERT wiml_gaesteliste SET '.$update .', importdatum="'.$time.'", importdatei="'.$_GET['file'].'", nid="'.$_GET["newsletterID"].'"';
									$res = $wpdb->query($sql);

									$output .= '<td class="result">'.$res.'<td>';
									$p = $p + $res;
									$update = '';

							}
						}
				$output .=  "</TR>";
			}
			$output .=  "</table>";
			return '<h2 class="hndle ui-sortable-handle">Anzahl: '.$anz.' | Anzahl der Uploaded:'.$p .'</h2><div class="inside">'.$output.'</div>';
		}
	}
	function datei(){
		$upload_dir = wp_upload_dir();
		return $upload_dir['basedir'].'/gaestelisten/'.$_GET['file'];
	}
}
?>
