<?php
class show_gaestelisten_wp_list_table extends WP_List_Table
{
	protected $data = "";
	
	public function __construct(){
        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'wplisttable_show_gaesteliste',     //singular name of the listed records
            'plural'    => 'wplisttable_show_gaestelisten',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );
	}
	private function table_data()
    {
		global $wpdb;
		if( $_GET["importdatum"] )
		{
			$sql = "select * from wiml_gaesteliste WHERE nid = '".$_GET["newsletterID"]."' AND importdatum='".$_GET["importdatum"]."'";		
		} else {
			$sql = "select * from wiml_gaesteliste WHERE nid = '".$_GET["newsletterID"]."'";	
		}
		if( $_GET["s"] )
		{
			$sql .= ' AND (firstname LIKE "%'.$_REQUEST["s"].'%" OR lastname LIKE "%'.$_REQUEST["s"].'%")';	
		}
		$sql .= '  order by firstname asc';
		$data = $wpdb->get_results($sql, ARRAY_A);
		return $data;
	}
	public static function record_count() {
		global $wpdb;
		if( $_GET["importdatum"] )
		{
			$sql = "select COUNT(*) from wiml_gaesteliste WHERE nid = '".$_GET["newsletterID"]."' AND  importdatum='".$_GET["importdatum"]."' order by firstname";		
		} else {
			$sql = "select COUNT(*) from wiml_gaesteliste WHERE nid = '".$_GET["newsletterID"]."'  order by firstname";	
		}
		return $wpdb->get_var( $sql );
	}
	function column_firstname($item){
		$actions = array(
			'delete'  => sprintf('<a href="'. meineGaesteliste::url() .'&importdatum=' .$_GET["importdatum"] .'&s='.$_REQUEST["s"] .'&action=delete_out_gaesteliste&uid='.$item['id'].'">Löschen</a>')
		);
		return sprintf('%1$s %2$s',$item['firstname'],$this->row_actions($actions));
	}
	public function column_default( $item, $column_name )
    {
		
        switch( $column_name ) {
            case 'firstname':
            case 'lastname':
			case 'plus':
			case 'aktion':
			case 'importdatei':
                return $item[ $column_name ];
			default:
                return print_r( $item, true ) ;
        }
    }
	function get_columns() {
	   $columns = array(
            'firstname'		=> 'Vorname',
            'lastname'		=> 'Nachname',
            'plus'			=> 'Plus',
			'aktion'		=> 'Aktion',
			'importdatei'	=> 'ImportDatei',
        );
	  return $columns;
	}
	public function prepare_items() {
		
		$this->_column_headers = array( 
			 $this->get_columns(),		// columns
			 $this->get_hidden_columns(),			// hidden
			 $this->get_sortable_columns(),	// sortable
		);
		
		/** Process bulk action */
		$this->process_bulk_action();
		$this->items = $this->table_data();
	}
	function extra_tablenav($which) {
		if($which == 'top'){
		$this->search_box('search', 'search_id');
		?>
		<div class="alignleft actions">
        	<a href="<?php echo meineGaesteliste::remove_parameter("&view=show") ?>&view=upload" class="button">Zurück</a>
        </div>
		<?php
		}
	}
	function process_bulk_action() {
		global $wpdb;
		//Detect when a bulk action is being triggered...
		if( 'delete_out_gaesteliste'===$this->current_action() && $_REQUEST["uid"]) {
			$sql = 'DELETE FROM wiml_gaesteliste where nid="'.$_GET["newsletterID"].'" and id="'.$_GET['uid'].'"';
			$wpdb->query($sql);
		}
	
	}
}
?>