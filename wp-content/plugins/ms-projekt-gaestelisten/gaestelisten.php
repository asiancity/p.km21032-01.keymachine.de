<?php
/*
Plugin Name: Gästelisten
Plugin URI:
Description: Bitte folgende Plugins installieren: HELP-TABS von ATH.
Version: 1.201703.14
Author: Anh-Tuan Hoang
Author URI:
*/
/*header('Access-Control-Allow-Origin: *');*/
define( 'ATHG', plugin_dir_path( __FILE__ ) );
if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
if(!class_exists('MSDashboard')){
  wp_die( sprintf( __("No plugins found for &#8220;%s&#8221;."), "MSDashboard - ms_main" ) .'<br><br>'. plugin_dir_path( __FILE__ ) );
}
if(!class_exists('MSCharts')){
  wp_die( sprintf( __("No plugins found for &#8220;%s&#8221;."), "MSCharts - ms_main" ) .'<br><br>'. plugin_dir_path( __FILE__ ) );
}

// add_action( 'wp_enqueue_scripts', 'prefix_add_my_stylesheet');

require(ATHG . '/classes/events_list.php');
require(ATHG . '/view_upload.php');
require(ATHG . '/classes/upload.php');
require(ATHG . '/classes/upload_show.php');
require(ATHG . '/classes/upload_datei_show.php');
require(ATHG . '/classes/show_gaestelisten.php');
require(ATHG . '/classes/show_gaestelisten_wp_list_table.php');
require(ATHG . '/classes/gaesteliste_insert_names.php');
require(ATHG . '/classes/export.php');
require(ATHG . '/classes/msprint.php');
require(ATHG . '/classes/load_external_events.php');
require('mainGaesteliste.php');
require('stats.php');

if(is_admin())
{
	new meineGaesteliste();
}

class meineGaesteliste{

	public function __construct()
	{
		add_action( 'admin_menu', array($this, 'add_menu_gaestelisten_list_table_page' ));
		add_filter(	'set-screen-option', array($this, 'gaestelisten_set_screen_option'), 10, 3);

		add_action( 'admin_enqueue_scripts', array($this, 'load_wp_admin_style') );
		add_action( 'wp_ajax_nopriv_gaestelisten_show', array($this, 'gaestelisten_show' ) );
		add_action( 'wp_ajax_gaestelisten_show', array($this, 'gaestelisten_show' ) );
	}
	/*
		fn
	*/
	public function add_menu_gaestelisten_list_table_page()
	{
		global $gaestelisten_page_settings;
		 $gaestelisten_page_settings = add_menu_page('Gästelisten', 'Gästelisten', 'edit_pages', 'mainstream-guestlist', array($this, "render_gaestelisten_pages"), '', 11);
		 add_action( "load-{$gaestelisten_page_settings}", array($this, 'add_options') );
		 add_action( 'admin_head-' . $gaestelisten_page_settings, array($this, 'help_tab') );

     $gaestelisten_01 = add_submenu_page( 'mainstream-guestlist', 'Anmeldungen Statistik', 'Statistik', 'edit_pages', 'mainstream-guestlist-stats', array($this, 'page_stats'));
	}
	/*
		fn
	*/
	public static function url(){
    $page           = isset($_REQUEST["page"]) ? $_REQUEST["page"] : NULL;
    $view           = isset($_REQUEST["view"]) ? $_REQUEST["view"] : NULL;
    $newsletterID   = isset($_REQUEST["newsletterID"]) ? $_REQUEST["newsletterID"] : NULL;
    $eventtime      = isset($_REQUEST["eventtime"]) ? $_REQUEST["eventtime"] : NULL;
    $event_id       = isset($_REQUEST["event_id"]) ? $_REQUEST["event_id"] : NULL;
		return  admin_url('admin.php'). '?page='.$page.'&view='.$view.'&newsletterID='. $newsletterID .'&eventtime=' .$eventtime.'&event_id=' .$event_id;
	}
	/*
		fn
	*/
	public static function url_admin_ajax_show_gaesteliste( $event, $parameter ){
		return admin_url("admin-ajax.php") . '?action=gaestelisten_show' . $parameter .'&api_key=' . md5( $event->event_datum .'-'.$event->event_id);
	}
	/*
		fn
	*/
	public function remove_parameter( $parameter ){
		return str_replace($parameter, "", self::url() );
	}
	/*
		fn
	*/
	public function render_events_list_page(){

		$event_list_table = new event_list_table();
		$event_list_table->prepare_items();?>
		<a href="?page=<?php echo $_REQUEST["page"] ?>&view=load-external-events" class="button alignright">External Events loaden</a>
		<form id="movies-filter" method="get">
		<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
		<?php $event_list_table->display() ?>
		</form>
		<?php
	}
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

  public function page_stats(){
    ?>
      <div class="wrap">
          <h2 class="page-title"><span class="dashicons dashicons-admin-users"></span> <?php echo $GLOBALS['title'] ?></h2>
          <?php
          $gaestelistenStats = new gaestelistenStats;
          return $gaestelistenStats->display();
          ?>
      </div>
    <?php
  }
	/*
		fn
	*/
	public function add_options() {
		global $gaestelisten_page_settings;

		$screen = get_current_screen();

		if(!is_object($screen) || $screen->id != $gaestelisten_page_settings)
			return;

		$option = 'per_page';
		$args = array(
		'label' => 'Events',
		'default' => 10,
		'option' => 'newsletter_per_page'
		);
		add_screen_option( $option, $args );
	}
	public function gaestelisten_set_screen_option($status, $option, $value) {

		if ( 'newsletter_per_page' == $option ) {
			return $value;
		}
	}
	/*
		fn
	*/
	public function load_wp_admin_style(){
		if( isset($_REQUEST["page"]) && $_REQUEST["page"] == "mainstream-guestlist"){
		wp_enqueue_style( 'gaesteliste_css', plugin_dir_url( __FILE__ ) . '/css/style.css' );
		wp_enqueue_script( 'gaesteliste_js', plugins_url( '/js/script.js', __FILE__ ), array(), '1.0.0', true );
		}
	}
	/*
		fn
	*/
	public function render_gaestelisten_pages(){
		?>
		<div class="wrap">

			<div id="icon-users" class="icon32"><br/></div>
			<h2><?php echo $GLOBALS['title'] ?></h2>
			<?php
			if( isset($_REQUEST["view"]) && $_REQUEST["view"] == "upload" ) :
				new view_upload();
				$this->js_redirect();
			elseif( isset($_REQUEST["view"]) && $_REQUEST["view"] == "show" ) :
				$this->show_gaestelisten_wp_list_table();
			elseif( isset($_REQUEST["view"]) && $_REQUEST["view"] == "load-external-events" ) :
				new load_external_events;
			else :
				$this->render_events_list_page();
			endif; ?>
		</div>
		<?php
	}
	/*
		fn
	*/
	public function gaestelisten_show(){
		global $wpdb;
		$show_gaestelisten = new show_gaestelisten;
		$show_gaestelisten->display();
		wp_die();
	}
	/*
		fn
	*/
	public function show_gaestelisten_wp_list_table(){
		global $wpdb;
		$event = $wpdb->get_row( "select * from wiml_maillist_newsletter WHERE id = '".$_GET["newsletterID"]."'" );
		$show_gaestelisten_wp_list_table = new show_gaestelisten_wp_list_table;
		$show_gaestelisten_wp_list_table->prepare_items();
		?>
        <h2><?php echo date_i18n("l, d.m.Y", strtotime($event->event_datum) ) ?> - <?php echo $event->name ?> | Anzahl: <?php echo $show_gaestelisten_wp_list_table->record_count(); ?></h2>
       	<?php if( $_REQUEST["importdatum"] ) : ?><h3>Liste: <?php echo $_REQUEST["importdatum"] ?></h3><?php endif; ?>
		<form id="movies-filter" method="get">
		<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
        <input type="hidden" name="newsletterID" value="<?php echo $_REQUEST['newsletterID'] ?>" />
        <input type="hidden" name="view" value="<?php echo $_REQUEST['view'] ?>" />
        <input type="hidden" name="importdatum" value="<?php echo $_REQUEST['importdatum'] ?>" />
		<?php $show_gaestelisten_wp_list_table->display() ?>
		</form>
		<?php
	}
	/*
		fn
	*/
	public function js_redirect(){
    $newsletterID = isset( $_GET['newsletterID'] ) ? $_GET['newsletterID'] : NULL;
    $delete_now   = isset($_GET['delete_now'])  ? $_GET['delete_now'] : NULL;
    if( !$newsletterID || $delete_now ){
			$url = admin_url("admin.php") . '?page=mainstream-guestlist';
			if( $delete_now ){
				$url = $this->url();
			}
			echo'
			<script type="text/javascript">
				setTimeout( function(){ window.location.href = "'.$url.'"; },500 );
			</script>';
		}
	}
	/*
		fn
	*/
	public function help_tab() {

		if (!class_exists('help_tabs')) {
			$class = 'notice notice-error';
			$message = 'No Class "help_tabs". ' . sprintf(__("Install %s now"), 'Plugin (help_tabs)');
			printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message );
			return '';
		}

		$screen		= get_current_screen();

		$dir = ATHG . '/helps';
		$help_tabs = new help_tabs( $screen, $dir );
	}
}
?>
