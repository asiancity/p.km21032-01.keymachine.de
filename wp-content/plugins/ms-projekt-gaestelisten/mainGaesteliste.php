<?php
error_reporting(-1);
ini_set('display_errors', 'On');

$mainGaesteliste = new mainGaesteliste();
class mainGaesteliste
{
	public $mydb = "";
	public $em_events = "";
	public $em_bookings = "";
	public $em_tickets = "";
  public $terms = "";
  public $term_taxonomy = "";
  public $term_relationships = "";
  public $posts = "";
	public $postmeta = "";
	public $users = "";
	public $data = "";
	public $event = "";
  public $error = "";

	public function __construct()
	{
		global $ms_prefix;
		add_action( 'wp_ajax_nopriv_myguestlist', array($this, 'events' ) );
		add_action( 'wp_ajax_myguestlist', array($this, 'events' ) );
		add_action( 'wp_ajax_nopriv_print_mainstream_guestlist_today', array($this, 'print_mainstream_guestlist_today' ) );
		add_action( 'wp_ajax_print_mainstream_guestlist_today', array($this, 'print_mainstream_guestlist_today' ) );

    $this->terms                = $ms_prefix."terms";
    $this->term_taxonomy        = $ms_prefix."term_taxonomy";
    $this->term_relationships   = $ms_prefix."term_relationships";
    $this->posts                = $ms_prefix."posts";

		$this->em_events            = $ms_prefix."em_events";
		$this->em_bookings          = $ms_prefix."em_bookings";
		$this->em_tickets           = $ms_prefix."em_tickets";
		$this->postmeta             = $ms_prefix."postmeta";
		$this->users                = $ms_prefix."users";

    $this->error                = 'Fehler';
	}
	/*
		fn
	*/
	public function sql_events()
	{
    $_mode = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : NULL;
    $_e    = isset($_REQUEST["e"]) ? $_REQUEST["e"] : NULL;
    $_term = isset($_REQUEST["term"]) ? $_REQUEST["term"] : NULL;
    $error = 'Fehler';
    $sql   = '';


    if( $_mode == "last-events" && !is_null($_term) )
    {
      if($_term !== 'ladylike'){
        wp_die($error);
      }
      $sql = "select tm.slug, e.*, t.ticket_description,
          (SELECT count(booking_id) FROM ".$this->em_bookings." b WHERE b.event_id=e.event_id) as count_guestlist
          from ".$this->terms." tm
          LEFT JOIN ".$this->term_taxonomy." tmt ON tm.term_id=tmt.term_id
          LEFT JOIN ".$this->term_relationships." tmr ON tmt.term_taxonomy_id=tmr.term_taxonomy_id
          LEFT JOIN ".$this->posts." p ON tmr.object_id=p.ID
          LEFT JOIN ".$this->em_events."  e ON p.ID=e.post_id
					LEFT JOIN ".$this->em_tickets." t ON t.event_id=e.event_id
					WHERE e.recurrence=0 AND e.event_start_date < '".current_time("Y-m-d")."' AND tm.slug='".$_term."' AND e.event_slug LIKE 'ladylike%' ORDER BY e.event_start_date DESC";

      // echo $sql;
    }
    else if( $_mode == "current" )
    {
			$sql = "select t.ticket_description,
					e.*,
					(SELECT count(booking_id) FROM ".$this->em_bookings." b WHERE b.event_id=e.event_id) as count_guestlist
					from ".$this->em_events." e
					LEFT JOIN ".$this->em_tickets." t ON t.event_id=e.event_id
					WHERE e.recurrence=0 AND e.event_start_date = '".current_time("Y-m-d")."' ORDER BY e.event_start_date ASC";

          // wp_die('kein Mode');
		}
    else {
      if ( $_e ){
  			$sql = "select t.ticket_description,
  					e.*,
  					(SELECT count(booking_id) FROM ".$this->em_bookings." b WHERE b.event_id=e.event_id) as count_guestlist
  					from ".$this->em_events." e
  					LEFT JOIN ".$this->em_tickets." t ON t.event_id=e.event_id
  					WHERE e.recurrence=0 AND  e.event_id = '".$_e."' ORDER BY e.event_start_date ASC";

  		}
    }
    if ( !$sql ){
      wp_die();
    }
		return $sql;
	}
	/*
		fn
	*/

	public function mydb()
	{
		global $datenbank, $ms_prefix;
		$db = $datenbank[0];
		return new DB_MySQL($db['localhost'], $db['db'], $db['username'], $db['password']);
	}
	public function codierung( $event='' ){
    if( !$event )
      return '';

		return md5($event->event_start_date .'-p'.$event->post_id.'-'.$event->event_date_created .'-e'. $event->event_id);
	}
	public function events()
	{
		global $datenbank, $ms_prefix;

		$mydb					          = $this->mydb();
		$this->mydb		          = $mydb;
		$sql					          = $this->sql_events();

		$events 			          = $mydb->get_results( $sql );
		$time_sendEmail         = '00:00:00';
		$time_strto_sendEmail   = strtotime( $time_sendEmail );
    $_mode                  = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : NULL;
    $_term                  = isset($_REQUEST["term"]) ? $_REQUEST["term"] : NULL;
    $i                      = 1;
		?>

    <link rel='stylesheet' href="<?php echo plugin_dir_url( __FILE__ ) . '/css/style.css' ?>" type='text/css' media='all'/>
    <h2>Uhrzeit: <span class="curretime"><?php echo current_time("d.m.Y H:i:s") ?></span> - Sendeuhrzeit: ab <span><?php echo $time_sendEmail ?></span></h2>
    <ul class="mainGuestlist">
		<?php
		foreach( $events as $event ) {
      // var_dump( $event );
			if ( $time_strto_sendEmail <= strtotime(current_time("H:i:s")) && $_mode == 'current'){
				$this->load_extern_events_gaesteliste( $event );
			}
			$users_array = $this->get_postmeta( $event->post_id );
      if( $_term == 'ladylike'){
        $url = admin_url("admin-ajax.php") . '?action=print_mainstream_guestlist_today&col=user_email&e=' .$event->event_id .'&api_key=' . $this->codierung($event);
      } else {
        $url = admin_url("admin-ajax.php") . '?action=print_mainstream_guestlist_today&e=' .$event->event_id .'&api_key=' . $this->codierung($event);
      }
      ?>

        	<li>
            	<div class="event_id" data-eventid="<?php echo $event->event_id ?>"><?php echo $i ?></div>
              <div class="event_name"><?php echo $event->event_name ?></div>
            	<div class="event_start_date"><?php echo date_i18n("l, d.m.Y", strtotime($event->event_start_date) ) ?></div>
              <div class="event_start_time"><?php echo $event->event_start_time ?></div>
              <div class="event_start_time"><?php echo $event->event_rsvp_time ?></div>
              <div class="count_guestlist"><?php echo $event->count_guestlist ?></div>
							<div class="count_guestlist"><a target="_blank" href="<?php echo $url ?>">Drucken | Export</a></div>
                <ol>
                	<?php
									$users = explode(";", $users_array);
									if( is_array( $users ) ){
										foreach ($users	as $user ){
											if( $user ) : ?>
                         <li><?php echo esc_html( $user ) ?></li>
                       <?php
											endif;
										}
									}
									?>
                </ol>
            </li>
        <?php
        $i++;
        }
        ?>
        </ul>
		<?php
		wp_die();
	}
	/*
		fn
	*/
	function load_extern_events_gaesteliste( $this_event ){
		global $ms_prefix;
    $event              = $this_event;
		$event->event_id	  = $this_event->event_id;
		$event->id				  = $event->event_id;
		$event->event_datum	= $this_event->event_start_date;
		$event->name		    = $this_event->event_name;
		$mydb 						  = $this->mydb;
    $_mode              = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : NULL;

		if(!$event->event_id){
			return '';
		}
    if($_mode !== 'current'){
			wp_die('Fehler: Modus');
		}

		$eventname		= $event->name . ' am ' .  date_i18n( get_option( 'date_format' ), strtotime( $event->event_datum ) );
		$url 			    = admin_url("admin-ajax.php").'?action=print_mainstream_guestlist_today&e='.$event->event_id.'&api_key=' .$this->codierung($this_event);
		$headers[] 		= 'Content-Type: text/html; charset=UTF-8';
		$headers[]		= 'From: MainStream Gästeliste <admin@km21032-01.keymachine.de>';
		$headers[] 		= 'Bcc: Anh Tuan Hoang <anhtuanh@hotmail.com>';
		$headers[] 		= 'Bcc: Anh Tuan Hoang <admin@km21032-01.keymachine.de>';

		$to					  = $this->get_postmeta( $this_event->post_id );
		$user[]			  = $to;
		$subject		  = 'Gästeliste - ' . $eventname;

		//HTML Emails
		ob_start();
			include('emails/mail.php');
			$message = ob_get_contents();
		ob_end_clean();

		$variables	= array("[eventname]", "[ticket_description]", "[url]");
		$value			= array($eventname, $this_event->ticket_description, $url );
		$message		= str_replace($variables, $value, $message);

		$attachments[]	= '';

		$sent_message 	= wp_mail( $to, $subject, $message, $headers, $attachments );

		if ( $sent_message ) {
			echo '<div class="success">';
			echo 'Die Gästeliste &quot;'. $eventname .'&quot; wurde gesendet an: <br>';
			foreach( $user as $toIndex => $t ){
			echo ($toIndex+1) .') ' . $t . '<br>';
			}
			echo '</div>';
		} else {
			echo '<div class="error">';
			echo 'Es gibt Problem mit der GL-Sendung';
			echo '</div>';
		}

		// return $out;
	}
	public function print_mainstream_guestlist_today()
	{
    $_e              = isset($_REQUEST["e"])        ? $_REQUEST["e"]       : NULL;
    $_apiKey         = isset($_REQUEST["api_key"])  ? $_REQUEST["api_key"] : NULL;
    $_col            = isset($_REQUEST["col"]) ? $_REQUEST["col"] : NULL;

		if( !is_numeric($_e ) ){
			wp_die($this->error . ' NO e');
		}
		if( !$_apiKey ){
			wp_die($this->error . ' NO API');
		}

		$mydb 				= $this->mydb();
		$sql 					= $this->sql_events();

		$event		= $mydb->fetch_row( $sql );

		if( $_apiKey != $this->codierung($event) ){
			wp_die($this->error .' API wrong');
		} //codierung

		// $event->event_id	= $this_event->event_id;
		// $event->id			= $event->event_id;
		// $event->event_datum	= $this_event->event_start_date;
		// $event->name		= $this_event->event_name;

		$page_title = date_i18n("l, d.m.Y", strtotime($event->event_start_date) ) .' | '. $event->event_name;
		$sql = "select * from ".$this->em_bookings." WHERE booking_status=1 and event_id = '".$event->event_id."'";

		$rootData = $mydb->get_results( $sql );

		foreach($rootData as $r) {
			$booking_meta  = maybe_unserialize($r->booking_meta);
			$s             = $booking_meta["registration"];
			$firstname     = ucfirst($s["first_name"]);
			$lastname      = ucfirst($s["last_name"]);
      $key           = preg_replace('/\s+/', '', strtolower($firstname.$lastname.$s["user_email"]));
			$new_data[$key]    = array(
        "firstname"     => $firstname,
        "lastname"      => $lastname,
        "plus"          => "",
        "aktion"        => 'Aktion',
        "importdatei"   => "MainStream",
        "user_email"    => ( $_col == 'user_email' ) ? $s["user_email"] : ''
      );
		}
    // $new_data = array_map('unserialize', array_unique(array_map('serialize', $new_data)));
		$this->data   = $new_data;
		$this->event  = $event;
		$this->export();
		?>
		<html>
        <head>
        <title><?php echo $page_title ?></title>
        <link rel='stylesheet' href="<?php echo plugin_dir_url( __FILE__ ) . '/css/style.css' ?>" type='text/css' media='all'/>
        </head>
        <body class="gaesteliste_show_print">
        <div class="actions">
        	<div id="print" class="button" onClick="javascript:window.print();">Sofort ausdrucken</div>
        	<a href="<?php echo admin_url('admin-ajax.php') ?>?action=<?php echo $_REQUEST["action"] ?>&e=<?php echo $_REQUEST["e"] ?>&api_key=<?php echo $_REQUEST["api_key"] ?>&export=csv&col=<?php echo $_REQUEST["col"] ?>" class="button">Export CSV</a>
          <a href="<?php echo admin_url('admin-ajax.php') ?>?action=<?php echo $_REQUEST["action"] ?>&e=<?php echo $_REQUEST["e"] ?>&api_key=<?php echo $_REQUEST["api_key"] ?>&export=xls&col=<?php echo $_REQUEST["col"] ?>" class="button">Export XLS</a>
          <a href="<?php echo admin_url('admin-ajax.php') ?>?action=<?php echo $_REQUEST["action"] ?>&e=<?php echo $_REQUEST["e"] ?>&api_key=<?php echo $_REQUEST["api_key"] ?>&export=pdf&col=<?php echo $_REQUEST["col"] ?>" class="button">Export PDF</a>
        </div>
        <h2><?php echo $page_title ?></h2>
        <h3>Aktion: <?php echo $event->ticket_description ?></h3>
        <h4>Anzahl: <?php echo count($new_data) ?></h4>
		<table>
		<thead>
        	<tr class="topic">
		        <td><strong>Vorname</strong></td>
						<td><strong>Nachname</strong></td>
						<td><strong>Plus</strong></td>
						<td><strong>Aktion</strong></td>
		        <td><strong>Support</strong></td>
            <?php if ( $_col == 'user_email') : ?>
            <td><strong>E-Mail</strong></td>
            <?php endif; ?>
					</tr>
    </thead>
		<tbody>
		<?php
		if( is_array( $new_data )){
			  $i = 1;
        asort( $new_data );
			  foreach ($new_data as $r)
			  {
			  ?>
              <tr>
                    <td><?php echo $r["firstname"]?></td>
                    <td><?php echo $r["lastname"]; ?></td>
                    <td><?php echo $r["plus"]; ?></td>
                    <td><?php echo $r["aktion"]; ?></td>
                    <td><?php echo $r["importdatei"]; ?></td>
                    <?php if ( $_col == 'user_email') : ?>
                    <td><?php echo $r["user_email"]; ?></td>
                    <?php endif; ?>
              </tr>
			  <?php
			  $i++;
			  }
		}
		?>
    </tbody>
		</table>
</body>
</html>
		<?php
		wp_die();
	}
	public function export(){
		$export = new export( $this->event, $this->data );
    $_export  = isset($_REQUEST["export"]) ? $_REQUEST["export"]  : NULL;

		if( $_export ){
			if( $_export == "xls"){
				$file = $export->xls_erstellen();
			}
			if( $_export == "pdf"){
				$file = $export->pdf_erstellen();
			}
			if( $_export == "csv"){
				$file = $export->csv_erstellen();
			}
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'.basename($file).'"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			readfile($file);
			wp_die();
		}
	}
	public function get_postmeta( $post_id='' ){

		$mydb = $this->mydb();
		$to = '';
    $sqlUsers = '';
    $sql = "select * from ".$this->postmeta." WHERE post_id = '".$post_id."' AND meta_key='_event_get_bookinglist'";
		$postmetas = $mydb->get_results( $sql );

    if( $postmetas->rowCount() < 1){
      return '';
    }
		foreach ( $postmetas as $idx => $postmeta ){
			$sqlUsers .= ($idx == 0) ? "ID='".$postmeta->meta_value."'" : " OR ID='".$postmeta->meta_value."'";
		}
    $users = $this->get_user( $sqlUsers );
    foreach ($users as $user ){
       $to .= $user->display_name .' <' . $user->user_email .'>;';
    }
		/*$to .= 'Anh Tuan Hoang <admin@km21032-01.keymachine.de>';*/
		return $to;
	}
	public function get_user( $sqlUsers = '' ){
		$mydb = $this->mydb();
		$sql = "select * from ".$this->users." WHERE ".$sqlUsers;

		return $mydb->get_results($sql);
	}

  public function unique_multidim_array($array, $key) {
    $temp_array = array();
    $i = 0;
    $key_array = array();

    foreach($array as $val) {
        if (!in_array($val[$key], $key_array)) {
            $key_array[$i] = $val[$key];
            $temp_array[$i] = $val;
        }
        $i++;
    }
    return $temp_array;
  }
}
?>
