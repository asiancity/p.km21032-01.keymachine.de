<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>[eventname]</title>
<style>
.button {
background: none repeat scroll 0% 0% #E14D43;
border-color: #D02C21 #BA281E #BA281E;
color: #FFF;
box-shadow: 0px 1px 0px #BA281E;
text-shadow: 0px -1px 1px #BA281E, 1px 0px 1px #BA281E, 0px 1px 1px #BA281E, -1px 0px 1px #BA281E;
display: inline-block;
text-decoration: none;
font-size: 13px;
line-height: 26px;
height: 28px;
margin: 0px;
padding: 0px 10px 1px;
cursor: pointer;
border-width: 1px;
border-style: solid;
border-radius: 3px;
white-space: nowrap;
box-sizing: border-box;
}
</style>
</head>

<body>
<h3>MainStream Gästeliste</h3>
<h4>[eventname]</h4>
<p><strong>Aktion:</strong><br>[ticket_description]</p>
<p>Klicken Sie bitte auf den Button, um die Gästeliste drucken zu können.</p>
<p><a class="button" href="[url]"><strong>Gästeliste drucken</strong></a></p>
<p>Liebe Grüße,<br>MainStream Team</p>
</body>
</html>