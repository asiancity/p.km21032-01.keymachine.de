<?php 
class Event {
		
		function datum($string){
			$txt = explode("-", $string);
			$txt = trim($txt[0]);
			
			$search = array(" ", "Jan", "Feb", "Mrz", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez");
			$replace = array("-", 01, 02, 03, 04, 05, 06, 07, 08, 09, 10,11,12);
			$datum = str_replace($search, $replace, $txt);
			$datum = explode("-", $datum);
			$tag = $datum[0];
			$monat = $datum[1];
			$jahr = '20'.$datum[2];
			
			$datum = $jahr.'-'.$monat.'-'.$tag;
			return $datum;
		}
}
?>