<?php
/*
Plugin Name: ATH Mobile
Plugin URI:
Description: ATH Mobile
Version: 1.201702.24
Author: Anh-Tuan Hoang
Author URI:
*/

if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
if(!class_exists('MSDashboard')){
    wp_die( sprintf( __("No plugins found for &#8220;%s&#8221;."), "MSDashboard - ms_main" ) .'<br><br>'. plugin_dir_path( __FILE__ ) );
}

require('classes/mobile_list.php');
require('classes/mobile_list_single.php');
require('classes/mobile_check.php');
require('classes/mobile_check_actions.php');
require('classes/mobile_report_upload_files.php');
require('classes/mobile_stats.php');
require('classes/mobile_stats_export.php');
require('classes/mobile_users_mobile.php');

if( is_admin() )
{
	new ms_mobile;
}

class ms_mobile
{
  public $dashicon = 'dashicons-phone';
  /*
    fn
  */
  public function __construct()
  {
    add_action( 'admin_menu', array($this, 'admin_menu' ));
    add_filter( 'set-screen-option', array($this, 'set_screen_option'), 10, 3);
    add_action( 'admin_enqueue_scripts', array($this, 'enqueue') );

    add_action( 'wp_ajax_mobile_update_user', array('MsMobileListSingle', 'ajax_update_user') );
		add_action( 'wp_ajax_nopriv_mobile_update_user', array('MsMobileListSingle' ,'ajax_update_user') );

    add_action( 'wp_ajax_mobile_userinfo', array('MsMobileListSingle', 'ajax_userinfo') );
		add_action( 'wp_ajax_nopriv_mobil_userinfo', array('MsMobileListSingle' ,'ajax_userinfo') );

    add_action( 'wp_ajax_mobile_check_mobile', array('MSMobileCheckActions', 'ajax_check_mobile') );
    add_action( 'wp_ajax_nopriv_mobile_check_mobile', array('MSMobileCheckActions' ,'ajax_check_mobile') );
    add_action( 'wp_ajax_mobile_check_netz', array('MSMobileCheckActions', 'ajax_netz') );
		add_action( 'wp_ajax_nopriv_mobile_check_netz', array('MSMobileCheckActions' ,'ajax_netz') );
    add_action( 'wp_ajax_mobile_check_aktiv_status', array('MSMobileCheckActions', 'ajax_aktiv_status') );
    add_action( 'wp_ajax_nopriv_mobile_check_aktiv_status', array('MSMobileCheckActions' ,'ajax_aktiv_status') );

    add_action( 'wp_ajax_mobile_report_check_aktiv_status', array('MSMobileReportUploadFiles', 'ajax_netz_status') );
    add_action( 'wp_ajax_nopriv_mobile_report_check_aktiv_status', array('MSMobileReportUploadFiles' ,'ajax_netz_status') );

    add_action( 'wp_ajax_mobile_stats_export', array('MSMobileStatsExport', 'ajax_sms_export') );
    add_action( 'wp_ajax_nopriv_mobile_stats_export', array('MSMobileStatsExport' ,'ajax_sms_export') );
  }
  /*
		fn
	*/
	public function admin_menu()
  {
		global $user_page_settings;
		$user_page_settings = add_menu_page('MS Mobil', 'MS Mobil', 'edit_pages', 'ath-mobile', array($this, 'user_page_actions'), $this->dashicon, 13);
		add_action( "load-{$user_page_settings}", array($this, 'add_options') );
    $user_page_settings_02 = add_submenu_page( 'ath-mobile', 'Users mit mehreren Handynummer', 'Users-Mobile', 'edit_pages', 'ms-mobile-users-mobile', array($this, 'MSMobileUsersMobile'));
    add_action( "load-{$user_page_settings_02}", array($this, 'add_options') );

    $user_page_settings_03 = add_submenu_page( 'ath-mobile', 'Report upload', 'Report upload', 'edit_pages', 'ms-mobile-report-upload', array($this, 'MSMobileReportUploadFiles'));
    $user_page_settings_04 = add_submenu_page( 'ath-mobile', 'Statistik', 'Statistik', 'edit_pages', 'ms-mobile-stats-01', array($this, 'MSMobileStats'));
    $user_page_settings_05 = add_submenu_page( 'ath-mobile', 'Prüf Nummer', 'Prüf Nummer', 'edit_pages', 'ms-mobile-check', array($this, 'MSMobileCheck'));

    add_action( 'admin_head-' . $user_page_settings, array($this, 'help_tab') );
    add_action( 'admin_head-' . $user_page_settings_02, array($this, 'help_tab') );
    add_action( 'admin_head-' . $user_page_settings_03, array($this, 'help_tab') );
    add_action( 'admin_head-' . $user_page_settings_04, array($this, 'help_tab') );
    add_action( 'admin_head-' . $user_page_settings_05, array($this, 'help_tab') );
	}
  /*
    fn
  */
  public function enqueue() {
    wp_enqueue_style( 'plugin-mobile', plugin_dir_url(__FILE__) .'css/style.css' );
    wp_enqueue_script( 'plugin-mobile-script', plugin_dir_url(__FILE__). 'js/script.js' );
  }
  /*
    fn
  */
  public function user_page_actions()
  {
    $_action  = isset( $_REQUEST["action"] ) ? $_REQUEST["action"] : NULL;
  ?>
    <div class="wrap">
		    <h2 class="page-title"><span class="dashicons <?php echo $this->dashicon ?>"></span> <?php echo $GLOBALS['title'] ?></h2>
        <?php
        switch( $_action ) {
            case 'edit':
              $MsMobileListSingle = new MsMobileListSingle;
              return $MsMobileListSingle->display();
            default:
              return $this->user_list();
              break;
        }
        ?>
    </div>
  <?php
  }
  public function MSMobileCheck(){
  ?>
    <div class="wrap">
        <h2 class="page-title"><span class="dashicons <?php echo $this->dashicon ?>"></span> <?php echo $GLOBALS['title'] ?></h2>
        <?php
        $MSMobileCheck = new MSMobileCheck;
        return $MSMobileCheck->display();
        ?>
    </div>
  <?php
  }
  public function MSMobileReportUploadFiles()
  {
  ?>
      <div class="wrap">
          <h2 class="page-title"><span class="dashicons <?php echo $this->dashicon ?>"></span> <?php echo $GLOBALS['title'] ?></h2>
          <?php
          $MSMobileReportUploadFiles = new MSMobileReportUploadFiles;
          return $MSMobileReportUploadFiles->display();
          ?>
      </div>
  <?php
  }
  public function MSMobileStats()
  {
  ?>
      <div class="wrap">
          <h2 class="page-title"><span class="dashicons <?php echo $this->dashicon ?>"></span> <?php echo $GLOBALS['title'] ?></h2>
          <?php
          $MSMobileStats = new MSMobileStats;
          return $MSMobileStats->display();
          ?>
      </div>
  <?php
  }
  /*
    fn
  */
  public function MSMobileUsersMobile(){
  ?>
      <div class="wrap">
          <h2 class="page-title"><span class="dashicons <?php echo $this->dashicon ?>"></span> <?php echo $GLOBALS['title'] ?></h2>
          <p>Users haben sich mit mehreren Handynummer angemeldet. Wann haben sie sich zuletzt angemeldet.</p>
          <?php
          $MSMobileUsersMobile = new MSMobileUsersMobile;
      		$MSMobileUsersMobile->prepare_items(); ?>
      		<form method="get">
      		<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
          <?php $MSMobileUsersMobile->search_box('search', 'search_id'); ?>
      		<?php $MSMobileUsersMobile->display() ?>
      		</form>
      </div>
  <?php
  }
  /*
		fn
	*/
  public function user_list(){

		$MsMobileList = new MsMobileList;
		$MsMobileList->prepare_items(); ?>
		<form method="get">
		<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
    <?php $MsMobileList->search_box('search', 'search_id'); ?>
		<?php $MsMobileList->display() ?>
		</form>
    <?php
	}
  /*
		fn
	*/
	public function add_options()
  {
		global $user_page_settings, $user_page_settings_02;
		$screen = get_current_screen();

		if(!is_object($screen))
			return;

		$option = 'per_page';
		$args = array(
			'label' => 'Users',
			'default' => 30,
			'option' => 'users_per_page'
		);

		add_screen_option( $option, $args );
		$MsMobileList = new MsMobileList;
    $MSMobileUsersMobile = new MSMobileUsersMobile;
	}
  /*
		fn
	*/
	public function set_screen_option($status, $option, $value) {
		if ( 'users_per_page' == $option ) return $value;
	}
  public function help_tab() {

  		if (!class_exists('help_tabs')) {
  			$class = 'notice notice-error';
  			$message = 'No Class "help_tabs". ' . sprintf(__("Install %s now"), 'Plugin (help_tabs)');
  			printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message );
  			return '';
  		}

  		$screen		= get_current_screen();

  		$dir = plugin_dir_path( __FILE__ ) . '/helps';
  		$help_tabs = new help_tabs( $screen, $dir );
  }
}
