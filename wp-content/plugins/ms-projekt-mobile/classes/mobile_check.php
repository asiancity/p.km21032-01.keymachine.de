<?php
/**
 *
 */
class MSMobileCheck
{

  public $dbtable = 'wiml_mobile';
  public $per_page = 1000;

  public function __construct()
  {

  }
  public function cols(){
    global $wpdb;
    return $wpdb->get_col( "DESC " . $this->dbtable, 0 );
  }
  public function formSql(){
		if ( $_REQUEST["data_show"] == true ){
			set_transient( 'transient_call_ms_mobile_check_old_numbers', stripslashes( $_REQUEST['sql'] ), 0 );
		}
		if( $_POST["transient_call_ms_mobile_check_old_numbers"] ){
			set_transient( 'transient_call_ms_mobile_check_old_numbers', stripslashes( $_POST['transient_call_ms_mobile_check_old_numbers'] ), 0 );
		}


		if ( false === ( $transient_call_ms_mobile_check_old_numbers = get_transient( 'transient_call_ms_mobile_check_old_numbers' ) ) ) {
			$sql = 'SELECT * FROM '.$this->dbtable.' WHERE blacklist = 0 and netz IS NULL ORDER BY id asc';
		} else {
			$sql = $transient_call_ms_mobile_check_old_numbers;
		}
		return $sql;
	}
  public function form()
  {
    ?>
    <form action="?page=<?php echo $_REQUEST["page"] ?>" method="post">
    <textarea name="transient_call_ms_mobile_check_old_numbers" style="width:100%"><?php echo $this->formSql() ?></textarea>
    <?php echo submit_button(); ?>
    </form>
    <p><a href="<?php echo admin_url("admin.php") ?>?page=<?php echo $_REQUEST["page"] ?>&sql=<?php echo $_REQUEST["sql"] ?>&start=<?php echo ($_REQUEST["start"]+$this->per_page) ?>" class="me1 button">Weitere Seiten</a></p>
    <?php
  }
  public function record_count()
  {
    global $wpdb;
    $sql  = (strpos($this->formSql(), 'ORDER') !== false) ? substr($this->formSql(), 0, strpos($this->formSql(), 'ORDER')+0 ) : $this->formSql();
    $sql  = str_replace('*','COUNT(*)', $sql);
    $data = $wpdb->get_var($sql);
    return $data;
  }
  public function table_data(){
    global $wpdb;
    //return $wpdb->get_results( $this->formSql() );
    $anzahl = $this->per_page;
    return $wpdb->get_results( $this->formSql() .' LIMIT '. ( ($_REQUEST["start"]) ? $_REQUEST["start"] : 0) . ', ' . $anzahl );
  }
  public function table_display(){
    $data = $this->table_data();
    $cols = $this->cols();
    if ( count($data) < 1 )
      wp_die( __('You attempted to edit an item that doesn&#8217;t exist. Perhaps it was deleted?') );

    echo '<table class="wp-list-table widefat striped">';
    echo '<thead>';
    echo '<tr>';
    foreach( $cols as $col ){
      echo '<th class="'.$col.'"><a href="'.admin_url("admin.php").'?page='.$_REQUEST["page"].'&data_show=true&sql='.urlencode($this->formSql().' ORDER BY '. $col.' DESC').'">'. $col.'</a></th>';
    }
    echo '<th></th>';
    echo '</tr>';
    echo '<thead>';
    echo '<tbody>';
    foreach( $data as $row ){
      echo '<tr>';
      foreach( $cols as $col ){
        echo '<td class="'.$col.'" '. $this->dataHref( $col, $row ) .'>'. $row->$col.'</td>';
      }
      echo '<td><a target="_blank" href="'.admin_url("admin.php").'?page=ath-mobile&action=edit&id='.$row->id.'"><span class="dashicons dashicons-edit"></span></a></td>';
      echo '</tr>';
    }
    echo '</tbody>';
    echo '</table>';
  }
  public function dataHref( $col, $row ){
    switch ( $col ) {

      case 'mobile':
        return 'data-href="'.admin_url("admin-ajax.php").'?action=mobile_check_mobile&id='.$row->id.'&mobile='.$row->mobile.'"';
        break;
      case 'netz':
        return 'data-href="'.admin_url("admin-ajax.php").'?action=mobile_check_netz&id='.$row->id.'&mobile='.$row->mobile.'"';
        break;
      case 'aktiv_status':
        return 'data-href="'.admin_url("admin-ajax.php").'?action=mobile_check_aktiv_status&id='.$row->id.'&netz_status='.$row->netz_status.'"';
        break;
      default:
        break;
    }
  }
  public function actions(){
    $actions = array(
        array("name" => "ms_mobile_check_netz", "title" => "Netz", "column" => "netz"),
        array("name" => "ms_mobile_check_mobile", "title" => "Mobile", "column" => "mobile"),
        array("name" => "ms_mobile_check_aktiv_status", "title" => "aktiv_status", "column" => "aktiv_status"),
    );
    foreach( $actions as $action){
        echo '<a href="#" id="'.$action["name"].'" class="button" data-column="'.$action["column"].'">'. $action["title"] .'</a>';
    }
    echo '<div style="margin-top:10px;">';
    echo '<div>Gesamt: <span>'.$this->record_count() .'</span></div>';
    echo '<div>Anzahl dieser Seite: <span>'.count($this->table_data()).'</span></div>';
    echo '</div>';
  }
  public function display()
  {
  ?>
      <div id="dashboard-widgets" class="metabox-holder">
        <div id="postbox-mobile-form" style="width:100%">
            <div class="meta-box-sortables ui-sortable">
                <div class="postbox">
                    <h2 class="hndle ui-sortable-handle">SQL</h2>
                    <div class="inside" data-href=""><?php $this->form() ?></div>
                </div>
            </div>
        </div>
        <div id="postbox-mobile-table" style="width:100%">
            <div class="meta-box-sortables ui-sortable">
                <div id="postbox-mobile-actions" class="postbox">
                    <h2 class="hndle ui-sortable-handle">Actions (Klick auf Button, um Spalte zu aktuelisieren).</h2>
                    <div class="inside" data-href=""><?php $this->actions() ?></div>
                </div>
            </div>
            <div class="meta-box-sortables ui-sortable">
                <div class="postbox">
                    <h2 class="hndle ui-sortable-handle">Mobile</h2>
                    <div class="inside" data-href=""><?php $this->table_display() ?></div>
                </div>
            </div>
        </div>
      </div>
  <?php
  }
}

?>
