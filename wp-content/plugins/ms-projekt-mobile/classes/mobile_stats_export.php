<?php
/**
 *
 */
class MSMobileStatsExport
{
    public $sql;
    public $filename;
    public function __construct(){
      $sql = stripslashes( $_REQUEST["sql"] );
      $sql  = str_replace('*','mobile, name, club, blacklist, aktiv_status, id', $sql);
      //var_dump( $sql );
      $this->sql = $sql;
      $this->filename = current_time("Y-m-d") .'_'. $_REQUEST["filename"];
    }
    /*
      fn
    */
    public function ajax_sms_export(){
      global $wpdb;

      $MSMobileStatsExport = new MSMobileStatsExport;
      $sql = $MSMobileStatsExport->sql;
      $fileName = $MSMobileStatsExport->filename.'.csv';

      header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
      header('Content-Description: File Transfer');
      header('Content-Encoding: UTF-8');
      header("Content-type: text/csv; charset=UTF-8");
      header("Content-Disposition: attachment; filename=".$fileName."");
      header("Expires: 0");
      header("Pragma: public");

      $fh = @fopen( 'php://output', 'w' );
      $sql_results = $wpdb->get_results( $sql, ARRAY_A );

      /* Begin Mitarbeiter */
      $mitarbeiter_sql = "SELECT u.mobile, u.name, u.club, u.blacklist, u.aktiv_status, u.id FROM wiml_mobile u WHERE u.club='ms-mitarbeiter'";
      $mitarbeiter_results = $wpdb->get_results( $mitarbeiter_sql, ARRAY_A );
      $results = array_merge( $sql_results, $mitarbeiter_results );
      /* END Mitarbeiter */

      $headerDisplayed = false;
      $delimiter = ';';
      $enclosure = '"';

      foreach ( $results as $data ) {

        $data["name"] = mb_convert_encoding($data["name"], 'UTF-16LE', 'UTF-8');
        //Replace Mobile
        $data = $MSMobileStatsExport->export_replace_mobile( $data );
        //Replace Name
        $data = $MSMobileStatsExport->export_replace_name( $data );

        // Add a header row if it hasn't been added yet
        if ( !$headerDisplayed ) {
          // Use the keys from $data as the titles
          fputcsv($fh, array_keys($data),$delimiter, $enclosure);
          $headerDisplayed = true;
        }

        // Put the data into the stream
        fputcsv($fh, $data, $delimiter, $enclosure);
      }
      // Close the file
      fclose($fh);
      // Make sure nothing else is sent, our file is done

      wp_die();
    }
    public function export_replace_mobile( $data ){

      if ( preg_match('/^0/', $data["mobile"]) ){
          $data["mobile"] = preg_replace("/^0/", '',$data["mobile"]);
      }

      if ( !preg_match('/^0/', $data["mobile"]) ){

        switch ( $_REQUEST["areacode"] ) {

          case "01":
            $data["mobile"] = '0' . $data["mobile"];
            break;
          case "0049":
            $data["mobile"] = '0049' . $data["mobile"];
            break;
          case "plus49":
            $data["mobile"] = '+49' . $data["mobile"];
            break;
          default:
            break;
        }
      }
      return $data;
    }
    public function export_replace_name( $data ){
      if ( strlen($data["name"]) < 3 ){
        $data["name"] = 'Berliner';
      }
      $data["name"] = trim( ucfirst($data["name"]) );
      return $data;
    }
}
?>
