<?php
class MSMobileCheckActions
{
  public $dbtable = 'wiml_mobile';
  public $mobile;
  public $id;
  /*
    fn
  */
  public function __construct(){
    $this->mobile = isset($_REQUEST["mobile"])  ? $_REQUEST["mobile"] : NULL;
    $this->id     = isset($_REQUEST["id"])      ? $_REQUEST["id"] : NULL;
  }
  /*
    fn
  */
  public function ajax_check_mobile(){
    global $wpdb;
    $MSMobileCheckActions = new MSMobileCheckActions;

    //IST Kein Netz, Löschen.
    $netz = $MSMobileCheckActions->netz();
    if ( !$netz ){
      $wpdb->query("UPDATE " . $MSMobileCheckActions->dbtable ." SET updatetime='".current_time("Y-m-d H:i:s")."', aktiv_status='deleted' WHERE id='".$MSMobileCheckActions->id."'");
      wp_send_json( array("statustext" => false ) );
    }

    $mobile = $MSMobileCheckActions->checkVorwahlMobileNr($MSMobileCheckActions->mobile);

    //IST Keine Nummer, Löschen.
    if( !is_numeric($mobile) ){
      $wpdb->query("UPDATE " . $MSMobileCheckActions->dbtable ." SET updatetime='".current_time("Y-m-d H:i:s")."', aktiv_status='deleted' WHERE id='".$MSMobileCheckActions->id."'");
      wp_send_json( array("statustext" => $MSMobileCheckActions->mobile . '<--Löschen' ) );
    } else {
      $result = $wpdb->query("UPDATE " . $MSMobileCheckActions->dbtable ." SET updatetime='".current_time("Y-m-d H:i:s")."', mobile='".$mobile."' WHERE id='".$MSMobileCheckActions->id."'");

      if(!$result){
        //Nicht Update, löschen, wenn die bereits existiert.
        if ( $MSMobileCheckActions->mobile != $mobile){
          $wpdb->query("UPDATE " . $MSMobileCheckActions->dbtable ." SET updatetime='".current_time("Y-m-d H:i:s")."', aktiv_status='deleted' WHERE id='".$MSMobileCheckActions->id."'");
          $mobile = $mobile .'<-Deleted';
        } else {
          $mobile = '<span>' .$mobile .'</span><span class="dashicons dashicons-yes"></span>';
        }
      } else {
        $mobile = '<span>' .$mobile .'</span><span class="dashicons dashicons-yes"></span>';
      }
      wp_send_json( array("statustext" => $mobile ) );
    }
    wp_die();
  }
  /*
    fn
  */
  public function ajax_aktiv_status(){
    global $wpdb;
    $MSMobileCheckActions = new MSMobileCheckActions;
    $status = array(
        'DELIVERED'     => 'erfolgreich',
        'TRANSMITTED'   => 'erfolgreich',
        'ACCEPTED'   => 'erfolgreich',
        'BUFFERED' => 'deleted',
        'NOT_DELIVERED' => 'deleted',
        '-' => 'deleted',

        'Die Nachricht wurde versendet.'  => 'erfolgreich',
        'Keine weiteren Auslieferungsversuche, Empfänger unbekannt oder gesperrt.'  => 'deleted',
        'Nachricht wurde nicht ausgeliefert, Empfänger unbekannt.'  => 'deleted',
        'Nachricht beim Netzbetreiber gespeichert, es folgen weitere Auslieferungsversuche.'  => 'deleted',
    );
    if (array_key_exists($_REQUEST["netz_status"], $status)) {
      $wpdb->query("UPDATE " . $MSMobileCheckActions->dbtable ." SET updatetime='".current_time("Y-m-d H:i:s")."', aktiv_status='".$status[$_REQUEST["netz_status"]]."' WHERE id='".$MSMobileCheckActions->id."'");
      wp_send_json( array("statustext" => $status[$_REQUEST["netz_status"]] ) );
    } else{
      wp_send_json( array("statustext" => false) );
    }
  }
  /*
    fn
  */
  public function ajax_netz(){
    global $wpdb;
    $MSMobileCheckActions = new MSMobileCheckActions;
    $netz = $MSMobileCheckActions->netz();
    if ( $netz ){
      $wpdb->query("UPDATE " . $MSMobileCheckActions->dbtable ." SET updatetime='".current_time("Y-m-d H:i:s")."', netz='".$netz."' WHERE id='".$MSMobileCheckActions->id."'");
      wp_send_json( array("statustext" => $netz ) );
    } else{
      wp_send_json( array("statustext" => false) );
    }
  }
  /*
    fn
  */
  public function netz(){

    $mobile = $this->checkVorwahl();
    $netze = array(

  			"151" => "d1",
  			"160" => "d1",
  			"170" => "d1",
  			"171" => "d1",
  			"175" => "d1",

  			"152" => "d2",
  			"162" => "d2",
  			"172" => "d2",
  			"173" => "d2",
  			"174" => "d2",

  			"157" => "eplus",
  			"163" => "eplus",
  			"177" => "eplus",
  			"178" => "eplus",

  			"159" => "o2",
  			"176" => "o2",
  			"179" => "o2"
  	);

  	if( strlen( $mobile ) >= 10 && strlen( $mobile ) < 12 ){
  		$vorwahl 	= substr( $mobile, 0, 3 );

  		if ( $netze[ $vorwahl ] ){
  			return $netze[ $vorwahl ];
  		} else{
  			return false;
  		}
  	}
  	return false;
  }
  public function checkVorwahl(){
      $mobile = $this->mobile;
      $mobile = $this->checkVorwahlMobileNr($mobile);
      return $mobile;
  }
  public function checkVorwahlMobileNr( $mobile ){
      // 1. Replace Chars to Only Numbers
      $mobile = preg_replace("/[^0-9]+/", '', $mobile);

      // 2. Check the first 4 Numbers of all
      $first_four_chars				= substr( $mobile, 0, 4);
      if( $first_four_chars == '0049'){
        $mobile = substr( $mobile, 4 );
      }
      // 2. Check the first 4 Numbers of all
      $first_four_chars				= substr( $mobile, 0, 4);
      if( $first_four_chars == '0049'){
        $mobile = substr( $mobile, 4 );
      }

      // 3. Check the first 2 Numbers of all
      $first_two_chars				= substr( $mobile, 0, 2);
      if( $first_two_chars == '01'){
        $mobile = substr( $mobile, 1 );
      }
      elseif ( $first_two_chars == '49' ) {
        $mobile = substr( $mobile, 2 );
      }
      else{
        // 4. Check the 1 Number of all
        $first_one_chars			= substr( $mobile, 0, 1);
        if( $first_one_chars == '1' ){
        $mobile = $mobile;
        } else {
        $mobile = '<span style="color:#ff0000;">' . $mobile  .' is invalid</span>';
        }
      }
      return $mobile;
  }
}
?>
