
jQuery(document).ready( function($) {
	$(document).on('submit', '.users-form-user', function(e){
		e.preventDefault();
		var users_form_user = $(this);
		var postbox_users_form_user = users_form_user.parents('.postbox');

		$.ajax({
				url: users_form_user.attr("action"),
				data: users_form_user.serializeArray(),
				dataType: 'jsonp',
				type:'post',
				beforeSend: function(formData, jqForm, options) {
					$(".notice").remove();
				},
				complete : function(jqXHR, textStatus) {
					var response = $.parseJSON( jqXHR.responseText );
					var msg = '<div class="notice notice-'+response.status+'"><p>'+response.statustext+'</p></div>';
					postbox_users_form_user.find('.inside').load(  postbox_users_form_user.find('.inside').data("href") );
					$("#postbox-user-mobile").load(  $("#postbox-user-mobile").data("href") );

					$('#dashboard-widgets').before( msg );
					users_form_user.find(".submit").append( msg );
				},
		});
		return false;
	});
	$(document)
		.on('click', '#postbox-user-userinfo .fullname a', function(){
				$(this).parent().find("input").val( $("#postbox-user-userinfo .firstname input").val() + ' ' + $("#postbox-user-userinfo .lastname input").val() );
				return false;
		})
		.on('click', '#postbox-user-events-2 a', function(){
				$("#postbox-user-userinfo .firstname input").val( $(this).parents("tr").find(".firstname").text() );
				$("#postbox-user-userinfo .lastname input").val( $(this).parents("tr").find(".lastname").text() );
				//console.log( $(this).parents("tr").find(".firstname").text() )
				return false;
		});
});
