<?php
/*
Plugin Name: ATH Users
Plugin URI:
Description: ATH Users
Version: 1.201702.24
Author: Anh-Tuan Hoang
Author URI:
*/

if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
if(!class_exists('MSDashboard')){
    wp_die( sprintf( __("No plugins found for &#8220;%s&#8221;."), "MSDashboard - ms_main" ) .'<br><br>'. plugin_dir_path( __FILE__ ));
}

require('classes/user_list.php');
require('classes/user_list_single.php');
require('classes/user_events.php');
require('classes/user_mobile.php');
require('classes/user_stats.php');

if( is_admin() )
{
	new ms_users();
}

class ms_users{

	var $dashicon = 'dashicons-admin-users';
	/*
		fn
	*/
	public function __construct()
	{
		add_action( 'admin_menu', array($this, 'admin_menu' ));
		add_filter( 'set-screen-option', array($this, 'set_screen_option'), 10, 3);
		add_action( 'admin_enqueue_scripts', array($this, 'enqueue') );

		add_action( 'wp_ajax_users_update_user', array('ath_userlist_single', 'ajax_update_user') );
		add_action( 'wp_ajax_nopriv_users_update_user', array('ath_userlist_single' ,'ajax_update_user') );

		add_action( 'wp_ajax_users_userinfo', array('ath_userlist_single', 'ajax_userinfo') );
		add_action( 'wp_ajax_nopriv_users_userinfo', array('ath_userlist_single' ,'ajax_userinfo') );

    add_action( 'wp_ajax_users_mobile_form', array('user_mobile', 'ajax_mobile_form') );
		add_action( 'wp_ajax_nopriv_users_mobile_form', array('user_mobile' ,'ajax_mobile_form') );

    add_action( 'wp_ajax_users_mobile_list', array('user_mobile', 'ajax_display') );
		add_action( 'wp_ajax_nopriv_users_mobile_list', array('user_mobile' ,'ajax_display') );

	}
	/*
		fn
	*/
	public function enqueue() {
   		wp_enqueue_script( 'ath-users-script', plugin_dir_url(__FILE__) . 'js/script.js' );
	}
	/*
		fn
	*/
	public function admin_menu(){

		global $ath_page_settings;

		$ath_page_settings = add_menu_page('MS Users', 'MS Users', 'edit_pages', 'ath-users', array($this, 'user_page_actions'), $this->dashicon, 12);
		add_action( "load-{$ath_page_settings}", array($this, 'add_options') );

    $ath_page_settings_01 = add_submenu_page( 'ath-users', 'Statistik', 'Statistik', 'edit_pages', 'ms-user-stats', array($this, 'userStats'));
	}
	/*
		fn
	*/
	public function user_page_actions(){
		//var_dump( $GLOBALS );
    $_action  = isset( $_REQUEST["action"] )  ? $_REQUEST["action"] : NULL;
		?>
		<div class="wrap">
			<h2 class="page-title"><span class="dashicons <?php echo $this->dashicon ?>"></span> <?php echo $GLOBALS['title'] ?></h2>
			<?php
            switch( $_action ) {

                 case 'edit':
                 	  $ath_userlist_single = new ath_userlist_single();
                    return $ath_userlist_single->display();

                 case 'delete':
                    ath_userlist_single::delete();
                    return $this->user_list();

                 case 'blacklist':
                    ath_userlist_single::blacklist();
                    return $this->user_list();

                 default:
                    return $this->user_list();
            }
            ?>
         </div>
         <?php
	}
  /*
		fn
	*/
  public function userStats(){
  ?>
    <div class="wrap">
      <h2 class="page-title"><span class="dashicons <?php echo $this->dashicon ?>"></span> <?php echo $GLOBALS['title'] ?></h2>
      <?php
      $userStats = new userStats;
      $userStats->display();
      ?>
    </div>
  <?php
  }

	/*
		fn
	*/
	public function user_list(){

		$user_list = new ath_user_list;
		$user_list->prepare_items(); ?>

		<form method="get">
		<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
    <?php $user_list->search_box('search', 'search_id'); ?>
		<?php $user_list->display() ?>
		</form>
    <div class="notice"><p><?php echo $user_list->sql; ?></p></div>
        <?php
	}
  /*
		fn
	*/
	public function add_options() {
		global $ath_page_settings;

		$screen = get_current_screen();

		if(!is_object($screen) || $screen->id != $ath_page_settings)
			return;

		$option = 'per_page';
		$args = array(
			'label' => 'Users',
			'default' => 30,
			'option' => 'ath_per_page'
		);

		add_screen_option( $option, $args );
		$ath_user_list = new ath_user_list;
	}
  /*
		fn
	*/
	public function set_screen_option($status, $option, $value) {
		if ( 'ath_per_page' == $option ) return $value;
	}
}
?>
