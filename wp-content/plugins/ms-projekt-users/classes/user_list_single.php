<?php
class ath_userlist_single
{
	public $uid;
	public $cols;

	public function __construct()
	{
		global $wpdb;

		if ( !$_REQUEST["uid"] )
			wp_die( __('You attempted to edit an item that doesn&#8217;t exist. Perhaps it was deleted?') );

		$this->uid = $_REQUEST["uid"];
		$this->cols = $wpdb->get_col( "DESC wiml_users" );
	}
	/*
		fn
	*/
	public function user_sql()
	{
		global $wpdb;

		$sql = "SELECT * FROM wiml_users WHERE uid='". $this->uid ."'";
		return $wpdb->get_row( $sql );
	}
	/*
		fn
	*/
	public function userinfo()
	{
		global $wpdb;
    ob_start();
		$user = $this->user_sql();
		?>
        <form action="<?php echo admin_url("admin-ajax.php") ?>" class="users-form-user" method="post">
        <input type="hidden" name="action" value="users_update_user">
        <input type="hidden" name="uid" value="<?php echo $user->uid ?>">
        <table class="widefat striped">
          <tr><td colspan="2" align="center"><?php submit_button( __("Save") );?></td></tr>
		<?php
		foreach( $this->cols as $col )
		{
			$type = '';
			switch( $col ){
				case 'uid' :
					$args = array('type' => 'text', 'name' => $col, 'value' => $user->$col, 'disable' => true );
					break;
				case 'club' :

					$sql	= "SELECT club FROM wiml_users WHERE blacklist=0 AND active=1 GROUP BY club ORDER BY club ASC";
					$clubs = $wpdb->get_results($sql);

					foreach( $clubs as $club )
						$options[ $club->club ] = $club->club;

					$args = array('type' => 'select', 'name' => $col, 'value' => $user->$col, 'options' => $options, 'class' => '' );
					break;

				case 'blacklist' :
				case 'active' :
				case 'confirm' :
					$args = array('type' => 'checkbox', 'name' => $col, 'value' => $user->$col );
					break;

				case 'fullname' :
						$args = array('type' => 'text', 'name' => $col, 'value' => $user->$col, 'icon' => array('dashicons-admin-customizer', 'Vorname & Nachname übernehmen.') );
						break;

				default :
					$args = array('type' => 'text', 'name' => $col, 'value' => $user->$col );
					break;
			}
			?>
      <tr class="<?php echo $col ?>"><td><?php echo $col ?></td><td><?php echo $this->input( $args ); ?></td></tr>
      <?php
		}
		?>
      <tr><td></td><td><?php submit_button( __("Save") );?></td></tr>
      </table>
		</form>
		<?php
    $out = ob_get_contents();
    ob_end_clean();
    return $out;
	}
	/*
		fn
	*/
	public function display()
	{
    ?>
    <a href="<?php echo admin_url('admin.php') . '?' . str_replace(array('&action=edit', '&uid=' .$this->uid), '', $_SERVER['QUERY_STRING']) ?>"><?php _e("Go back") ?></a>
    <?php
    $args = array( "postbox_class" => array("first" => "col-3 first", "second" => "col-9 last scroll-x") );
    $user_mobile = new user_mobile;
    $user_events = new user_events;
    $events_args = array("colname" => "email_address", "value" => $this->user_sql()->email_address );

    $dataHref_users_userinfo    = admin_url('admin-ajax.php') ."?action=users_userinfo&uid=". $this->uid;
    $dataHref_users_usermobile  = admin_url('admin-ajax.php') . "?action=users_mobile_list&uid=" . $this->uid;
    $widgets = array(
      array("align" => "first",   "id"  =>  "postbox-user-userinfo",  "title" => "UserInfo",  "height" => 50, "data" => $this->userinfo(), "dataHref" => $dataHref_users_userinfo   ),
      array("align" => "first",  "title" => "Events (" . count( $user_events->event_sql( $events_args ) ) . ") | User Ohne Userinfo (" . $this->user_sql()->email_address . ").",
          "height" => 50,
          "data" => $user_events->display( $events_args ) ),

      array("align" => "second",  "id"  =>  "postbox-user-events",    "title" => "Mobile",    "height" => 60, "data" => $user_mobile->display(), "dataHref" => $dataHref_users_usermobile ),
      array("align" => "second",  "id"  =>  "postbox-user-events-2",  "title" => "Events (".count( $user_events->event_sql() ).") | User wurde zu UserInfo zugewiesen (UID).",
          "height" => 40,
          "data" => $user_events->display()),

    );
    $MSDashboard = new MSDashboard( $widgets, $args );
    echo $MSDashboard->display();
	}
	/*
		fn
	*/
	public function ajax_update_user(){
		global $wpdb;
		$ath_userlist_single = new ath_userlist_single;

		if( !$ath_userlist_single->uid ) return '';

		$table			= 'wiml_users';
		$data			= array();
		$cols			= array_diff( $ath_userlist_single->cols ,array("uid") );

		foreach($cols as $col ) $data[ $col ] = $_REQUEST[ $col ];

		/* NEW DATA */
		$data["updatetime"]	=	current_time("Y-m-d H:i:s");
		if( $data["fullname"] == 'NoName' || strlen($data["fullname"]) < 3 )
		{
				if( strlen($data["firstname"]) > 2 || strlen($data["lastname"]) > 2 )
				{
					$data["fullname"] = (strlen($data["firstname"]) > 2) ? trim($data["firstname"]) .' ' . trim($data["lastname"]) : trim($data["lastname"]);
				}

		}

		$format			= '%s';

		$where			= array('uid' => $ath_userlist_single->uid );
		$whereformat	= '%d';

		$result = $wpdb->update( $table, $data, $where, $format, $where_format );

		$wpdb->query("UPDATE wiml_mobile SET name='".$data["fullname"]."' WHERE uid='".$ath_userlist_single->uid."'");

		if( $result ){
			$response['status'] = 'success';
			$response['statustext'] = __("Changes saved.");

		}else {
			$response['status'] = 'error';
			$response['statustext'] =  __("An unidentified error has occurred.");

		}
		wp_send_json( $response );
		wp_die();
	}
	/*
		fn
	*/
	public function ajax_userinfo(){
		$ath_userlist_single = new ath_userlist_single;
		$ath_userlist_single->userinfo();
		wp_die();
	}
	/*
		fn
	*/
	public function input( $args=array() )
	{
		$disable  = '';
    $icon     = '';
    $class    = '';
    $args     = is_array( $args ) ? extract( $args  ) : array();
    $ar_icon  = '';

		if($args["disable"] == true){
			$disable = 'disabled';
		}
		if( is_array( $icon ) )
		{
			$ar_icon = ' <a href="#" title="'. $icon[1] .'"><span class="dashicons '.$icon[0].'"></span></a>';
		}
    if($type == "checkbox"){
      return '<input type="checkbox" name="'.$name.'" value="1"class="'.$class.'" '.(($value == 1) ? 'checked' : '').'>' . $ar_icon;
    }
    if($type == "select"){
      $out = '';
      $out .= '<select name="'.$name.'" class="'.$class.'">';

      foreach( $options as $optk => $optv ){
        $out .= '<option value="'.$optv.'" '. ( ($optv == $value ) ? 'selected' : '' ) .'>' . $optv . '</option>';
      }
      $out .= '</select>' . $ar_icon;
			return $out;
		}
    return '<input type="text" name="'.$name.'" value="'.$value.'" class="'.$class.'" style="width:'.(($icon) ? '85%' : '100%').'" '.$disable.'>'. $ar_icon;
	}
	/*
		fn
	*/
	public function delete()
  {
    global $wpdb;
    $_uid   = isset($_REQUEST["uid"]) ? $_REQUEST["uid"] :  0;
    $table = "wiml_users";
    $result = $wpdb->query("UPDATE " . $table . " SET active=0 WHERE uid='".$_uid."'");
  }
	public function blacklist()
  {
    global $wpdb;
    $_uid   = isset($_REQUEST["uid"]) ? $_REQUEST["uid"] :  0;
    $table = "wiml_users";
    $result = $wpdb->query("UPDATE " . $table . " SET blacklist=1 WHERE uid='".$_uid."'");
  }
}
?>
