<?php
class user_events
{
	public $uid;
	public $cols;
	/*
		fn
	*/
	public function __construct()
	{
		$this->uid = $_REQUEST["uid"];
		//"event_datum",
		$this->cols = array("booking_datum", "event_name", "event_id", "uid", "firstname", "lastname", "email_adress", "mobile");
	}
	public function event_sql( $args=array() )
	{
		global $wpdb;
    $colname  = '';
    $value    = '';
    $type     = '';
    if ( is_array( $args ) ) extract( $args );

		if ( $colname == "email_address" ){
			$sql = "SELECT * FROM wiml_users_events WHERE uid!='". $this->uid ."' AND ".$type."='". $value ."' ORDER BY booking_datum DESC";
			return $wpdb->get_results( $sql );
		}
		$sql = "SELECT * FROM wiml_users_events WHERE uid='". $this->uid ."' ORDER BY booking_datum DESC";
    // var_dump( $sql );
		return $wpdb->get_results( $sql );
	}
	/*
		fn
	*/
	public function display( $args=array() )
	{
    global $wpdb;
    ob_start();
		echo '<table class="widefat striped">';
		echo '<thead><tr>';
		foreach( $this->cols as $col ){
				echo '<td class="' . $col. '">' . $col. '</td>';
		}
		echo '<td></td>';
		echo '</tr></thead>';
		foreach ( $this->event_sql( $args ) as $event ){
				echo '<tr>';
				foreach( $this->cols as $col ){
					echo '<td class="' . $col. '">' .  $event->$col . '</td>';
				}
				echo '<td><a target="_blank" href="#" title="Vorname & Nachname übernehmen."><span class="dashicons dashicons-admin-customizer"></span></td>';
				echo '</tr>';
		}
		echo '</table>';
    $out = ob_get_contents();
    ob_end_clean();
    return $out;
	}
}
?>
