<?php
class user_mobile
{
  public $uid;
  public $mobile_id;
	public $cols;

  public function __construct()
  {
    $this->uid = $_REQUEST["uid"];
    $this->mobile_id = isset($_REQUEST["mobile_id"]) ? $_REQUEST["mobile_id"] : 0;
    $this->cols = array("uid", "mobile", "name", "club", "aktiv_status", "netz_status", "netz_statusdatum","blacklist");
  }
  public function display()
  {
    global $wpdb;
		$cols = $this->cols;
		$sql = "SELECT * FROM wiml_mobile WHERE uid='". $this->uid ."' ORDER BY aktiv_status DESC";
		$mobiles = $wpdb->get_results( $sql );
    ob_start();
		//var_dump( $mobiles );
		echo '<table class="widefat striped">';
		echo '<tr>';
		echo '<th>ID</th>';
		foreach( $cols as $col ){
			echo '<th>' . $col .'</th>';
		}
		echo '<th></th>';
		echo '</tr>';
		foreach( $mobiles as $mk => $mv)
		{
			echo '<tr>';
			echo '<td>' . $mv->id .'</td>';
			foreach( $cols as $col ){ ?>
            	<td><?php echo $mv->$col ?></td>
            <?php
			}
			echo '<td><a target="_blank" href="'.admin_url("admin.php").'?page=ath-mobile&action=edit&id='.$mv->id.'" title="'.__("Edit").'" data-href="'.admin_url("admin-ajax.php").'?action=users_mobile_form&mobile_id='.$mv->id.'"><span class="dashicons dashicons-edit"></span></a></td>';
			echo '</tr>';
		}
		echo '</table>';

    $out = ob_get_contents();
    ob_end_clean();
    return $out;
  }
  public function ajax_display(){
    $user_mobile = new user_mobile;
    $user_mobile->display();
    wp_die();
  }
  public function ajax_mobile_form()
  {
    global $wpdb;
    if (!$_REQUEST["mobile_id"])
      wp_die();

    $user_mobile = new user_mobile;
    $sql = "SELECT * FROM wiml_mobile WHERE id='". $user_mobile->mobile_id ."'";
    $mobile = $wpdb->get_row( $sql );

    $cols = $wpdb->get_col( "DESC wiml_mobile" ); ?>
    <form>
    <input type="hidden" name="id" value="<?php echo $mobile->id ?>">
    <table class="widefat striped">
    <?php
    foreach( $cols as $col )
    {
      $type = '';
      switch( $col ){
        case 'id' :
					$args = array('type' => 'text', 'name' => $col, 'value' => $mobile->$col, 'disable' => true );
					break;
        case 'blacklist' :
  			case 'controll' :
  					$args = array('type' => 'checkbox', 'name' => $col, 'value' => $mobile->$col );
  					break;
        default :
          $args = array('type' => 'text', 'name' => $col, 'value' => $mobile->$col );
          break;
      }
      ?>
      <tr class="<?php echo $col ?>"><td><?php echo $col ?></td><td><?php echo ath_userlist_single::input( $args ); ?></td></tr>
      <?php
    }
    ?>
    <tr><td></td><td><?php submit_button( __("Save") );?></td></tr>
    </table>
    </form>
    <?php
    wp_die();
  }
}
?>
