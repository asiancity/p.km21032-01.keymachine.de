<?php
/**
 *
 */
class FbGeneralClasses
{
  public $uid;
  public $user;
  public $names;
  public $items;

  public static function getUserByUid( $uid )
  {
    global $wpdb;
    return $wpdb->get_row("SELECT * FROM " . DB_USERS . " WHERE uid='".$uid."'");
  }
  public function managedAccounts( $page='' )
  {
    global $wpdb;
    $sql = "SELECT fbuid as uid, name as fullname, anzahl as anz  FROM " . DB_ACCOUNTS . " WHERE website='facebook' AND fbuid!='' GROUP BY fbuid ORDER BY anzahlDatum DESC";
    $users = $wpdb->get_results( $sql );

    $out = '<table class="wp-list-table widefat striped">';
    foreach( $users as $user ){
      $this->getUser($user);

      $out .= '<tr '. ((isset($_REQUEST["uid"]) && $user->uid == $_REQUEST["uid"]) ? 'class="selected"' : '') .'>';
      $out .= '<td>'.$this->accountpic(array("page" => ( ($page) ? $page : "page=" .$_REQUEST["page"] ) )).'</td>';
      $out .= '<td title="'.$user->uid.'">'.$user->fullname.'<br>'.$user->uid.'</td>';
      $out .= '<td>'.$user->anz.'</td>';
      $out .= '</tr>';
    }
    $out .= '</table>';
    return $out;
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

  public static function userColsNoHidden()
  {
    global $wpdb;
    $cols = $wpdb->get_col( "DESC " . DB_USERS, 0 );
    array_unshift($cols, "userpic");
    array_push($cols, "age");
    return $cols;
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

  public function userCols()
  {
    global $wpdb;
    $cols = $this->userColsNoHidden();
    /*var_dump( $cols );*/
    /*if(($key = array_search('names', $cols)) !== false) {
        unset($cols[$key]);
    }*/
    $hiddenColumns = $this->get_hidden_columns();
    foreach($hiddenColumns as $hiddenCol){
      if(($key = array_search($hiddenCol, $cols)) !== false) {
          unset($cols[$key]);
      }
    }
    return $cols;
  }



  public function get_hidden_columns()
  {
		$columns =  (array) get_user_option( MSHiddenColumns );
		if ( count( $columns )  < 1 ){
			$columns = array('nophoto', 'del', 'api_error', 'work', 'education', 'locale', 'updatetime');
		}
    return $columns;
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
  public function getUser( $user )
  {
    if( is_object($user) ){
      $user = get_object_vars($user);
    }
    $this->user = $user;
    $this->names = maybe_unserialize(@$this->user["names"]);
    return $this->user;
  }
  public function id(){
    return $this->user["id"];
  }
  public function uid(){
    return $this->user["uid"];
  }
  public function names(){
    return $this->names["fullname"];
  }
  public function first_name(){
    return $this->user["first_name"];
  }
  public function last_name(){
    return $this->names["last_name"];
  }
  public function fullname(){
    return $this->names["fullname"];
  }
  public function username(){
    return $this->user["username"];
  }
  public function wohnort(){
    return $this->user["wohnort"];
  }
  public function eu(){
    $class="MSFbApiUpdate";
    $func = "updateEu";
    $url = admin_url("admin-ajax.php") . '?action='.$class.'_'.$func.'&class='.$class.'&func='.$func.'&uid=' .$this->user["uid"];
    return '<a href="#" class="ajax-update" data-href="'.$url.'&eu='.($this->user["eu"] ? 0 : 1).'">'.$this->user["eu"].'</a>';
    /*return $this->user["eu"];*/
  }
  public function keinviet_mask(){
    return @$this->user["keinviet_mask"];
  }
  public function gender(){
    return @$this->user["gender"];
  }
  public function updatetime(){
    return @$this->user["updatetime"];
  }
  public function aus(){
    return @$this->user["aus"];
  }
  public function geburtsdatum(){
    /*return $this->user["geburtsdatum"];*/
    return date_i18n("d.m.Y", strtotime($this->user["geburtsdatum"]));
  }
  public function age(){
    return @$this->user["age"] ;
  }
  public function mobile(){
    return @$this->user["mobile"];
  }
  public function locale(){
    return @$this->user["locale"];
  }
  public function education(){
    return @$this->user["education"];
  }
  public function work(){
    return @$this->user["work"];
  }
  public function api_error(){
    return @$this->user["api_error"];
  }
  public function del(){
    return @$this->user["del"];
  }
  public function nophoto(){
    return @$this->user["nophoto"];
  }
  public function userlink( $arg=array() ){
    if (!is_object($this->user)) {
      return '<a href="'.admin_url("admin.php") . '?' .$arg['page'].'&uid='.$this->user["uid"].'">'. ( (isset($arg['title'])) ? $arg['title'] : $this->user["uid"] ) .'</a>';
    }
    return '<a href="'.admin_url("admin.php") . '?' .$arg['page'].'&uid='.$this->user->uid.'">'. ((isset($arg['title'])) ? $arg['title'] : $this->user->uid ) .'</a>';
  }
  public function accountpic( $arg=array() ){
    if (!is_object($this->user)) {
      return $this->userlink(array("page" => (isset($arg['page'])) ? $arg['page'] : '', "title" => '<img src="https://graph.facebook.com/'.$this->user["uid"].'/picture'.( (@$arg["size"]) ? '?type='.$arg["size"] : '' ).'" '.( (@$arg["class"]) ? 'class="'.$arg["class"].'"' : '' ).'>') );
    }
    return $this->userlink(array("page" => $arg['page'], "title" => '<img src="https://graph.facebook.com/'.$this->user->uid.'/picture'.( (@$arg["size"]) ? '?type='.@$arg["size"] : '' ).'" '.( ($arg["class"]) ? 'class="'.$arg["class"].'"' : '' ).'>') );
  }
  public function userpic( $arg=array() ){
    extract( $arg, EXTR_PREFIX_SAME, null );
    if (!is_object($this->user)) {
      $uid = $this->user["uid"];
      return '<a target="_blank" href="https://www.facebook.com/'.$uid.'"><img src="https://graph.facebook.com/'.$uid.'/picture'.( ( isset($size) ) ? '?type='.$size : '' ).'" '.( ( isset($class) ) ? 'class="'.$class.'"' : '' ).'></a>';
    }
  }
  public function useredit($target=''){
    return '<a '.(($target) ? 'target="'.$target.'"' : '').' href="'.admin_url("admin.php") . '?page=ath-facebook&action=edit&uid='.$this->user["uid"].'"><span class="dashicons dashicons-edit"></span></a>';
  }
  /**
 	 * DB DB_AKT_FRIENDS
 	 *
 	 * @param type
 	 * @return void
	 */
  public function itime(){
    return (strtotime($this->user["itime"]) > 0) ? date_i18n("d.m.Y", strtotime($this->user["itime"])) : '';
  }
  public function anz(){
    return @$this->user["anz"];
  }
  public function fromuid(){
    return $this->user["fromuid"];
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
   public function search_text( $alias='' )
   {
     $s = $_REQUEST["s"];
     if( empty($s) )
       return '';

     /* AGE */
     if ( $_REQUEST["column"] == "age") {
       if( !$s ){
         return " AND TIMESTAMPDIFF(YEAR,geburtsdatum,CURDATE()) <= 35 and TIMESTAMPDIFF(YEAR,geburtsdatum,CURDATE()) >= 16";
       }
       $age = explode("-", $s);
       return " AND TIMESTAMPDIFF(YEAR,geburtsdatum,CURDATE()) <= ".$age[1]." and TIMESTAMPDIFF(YEAR,geburtsdatum,CURDATE()) >= " .$age[0];
     }
     /* WOHNORT */
     if ( $_REQUEST["column"] == "wohnort") {
       return " AND (wohnort LIKE '%".$s."%' OR aus LIKE '%".$s."%') ";
     }
     if ( $_REQUEST["column"] == "wohnort") {
       return " AND (aus LIKE '%".$s."%') ";
     }

     return ($_REQUEST["column"]) ?  " AND (".$_REQUEST["column"]." LIKE '%".$s."%') " : "";
   }
}
?>
