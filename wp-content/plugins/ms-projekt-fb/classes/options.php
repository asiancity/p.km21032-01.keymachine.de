<?php
/**
 *
 */
class MSFbOptions
{

  function __construct()
  {
    $this->update();
  }
  public function addNew()
  {
    $data = '';
    if( isset($_REQUEST["view"]) && $_REQUEST["view"] == "addnew"){
      $data = (object) [
        'name' => '<input type="text" name="newName" value="">',
        'value' => '<input type="text" name="newValue" value="">',
      ];
    }
    return $data;
  }
  public function display(){
    $data = array();
    $args =  '';
    $settings	= get_option( 'fba_options' );
    foreach( $settings as $settingK => $settingV ){
      switch ($settingK) {
        case 'foto_markieren_auto':
        case 'left_col_hidden':
        case 'right_col_hidden':
          $args = array("type" => "checkbox", "name" => $settingK, "value" => $settingV);
          break;
        case 'access_token':
          $args = array("type" => "text", "name" => $settingK, "value" => $settingV, "description" => '<a href="https://developers.facebook.com/tools/accesstoken/" target="_blank">accesstoken</a>');
          break;

        default:
          $args = array("type" => "text", "name" => $settingK, "value" => $settingV);
          break;
      }

      $newSettingK = array(
        'name' => $settingK,
        'value' => MSInput::display( $args )
      );
      $data[] = (object) $newSettingK;
    }
    $data[] = $this->addNew();
    $data[] = (object) [
      'name' => "",
      'value' => '<input type="submit" class="button button-primary" value="'.__("Save").'">',
    ];
    $args = array(
      "data" =>  $data,
      "cols" => array("name","value"),
      "button" => "");
    $MSTableSimple = new MSTableSimple( $args );

    $out = '<form method="POST">';
    $out .= MSInput::display( array("type" => "hidden", "name" => "mode", "value" => "update") );
    $out .= $MSTableSimple->display();
    $out .= '</form>';
    return $out;
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
  public function update()
  {
    if( !isset($_POST["mode"]) || $_POST["mode"] != "update"){
      return '';
    }

    $newSettings = '';
    $settings	= get_option( 'fba_options' );
    foreach( $settings as $settingK => $settingV ){
      $newSettings[ $settingK ] = @$_POST[ $settingK ];
    }
    if( isset($_POST["newName"]) && $_POST["newName"] && isset($_POST["newValue"]) && $_POST["newValue"]){
      $newSettings[ $_POST["newName"] ] = $_POST["newValue"];
    }
    update_option( 'fba_options', $newSettings );
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
  public static function api_settings()
  {
    	$settings	= get_option( 'fba_options' );
      wp_send_json( $settings );
      wp_die();
  }
}
?>
