<?php
/**
 *
 */
class MSFbGeo extends MSTable
{
  public $country;
  public $bundesland;
  public $stadt;
  public $uid;
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

  function __construct( $args )
  {
    if( is_array($args) ){
      $args["country"] = "DE";
    }
    $this->country  = $args["country"];

    parent::__construct();

    $this->bundesland = isset($_REQUEST["bundesland"]) ? $_REQUEST["bundesland"]  : NULL;
    $this->stadt      = isset($_REQUEST["stadt"])      ? $_REQUEST["stadt"]       : NULL;
    $this->uid        = isset($_REQUEST["uid"])        ? $_REQUEST["uid"]         : NULL;
    $this->db         = DB_USERS;
    $this->FbGeneral  = new FbGeneralClasses;

    $request = isset( $_GET ) ? $_GET : array();
    $request["column"] = isset($request["column"]) ? $request["column"] : NULL;
    if ( isset($request["column"]) ) unset($request["column"]);
    $this->request = $request;
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

  public function states()
  {
    global $wpdb;
    $sql = "SELECT * FROM " . DB_CITIES ." WHERE land='".$this->country."' GROUP BY bundesland ORDER BY bundesland asc";
    $states = $wpdb->get_results($sql);
    /*var_dump( $states );*/
    if( count($states) < 1 )
      return '';

    $args = array(
      "data" => $states,
      "cols" => array("land","bundesland", "button", "button2"),
      "button" => array(
        "button" => array("url" => admin_url("admin.php") .'?page=' .$_REQUEST["page"].'&eu=1', "id" => "bundesland", "icon" => "dashicons-admin-site"),
        "button2" => array("url" => admin_url("admin.php") .'?page=' .$_REQUEST["page"].'&eu=0', "id" => "bundesland", "icon" => "dashicons-universal-access-alt"))
    );
    $MSTableSimple = new MSTableSimple( $args );
    return $MSTableSimple->display();
  }
  /**
   	 * Block comment
   	 *
   	 * @param type
   	 * @return void
  	 */

  public function sql(){
    global $wpdb;

    if( !$this->bundesland ){
      return '';
    }
    if( $this->uid ){

      $sql = "SELECT u.*, TIMESTAMPDIFF(YEAR,geburtsdatum,CURDATE()) AS age
      FROM " . DB_CITIES ." c
      LEFT JOIN ".DB_USERS." u ON c.stadt=u.wohnort
      LEFT JOIN ".DB_AKT_FRIENDS." af ON u.uid=af.uid
      WHERE u.uid!=''
      AND af.itime IS NULL AND (af.fromuid='".$this->uid."' OR af.fromuid IS NULL)
      AND u.eu='". ($_REQUEST['eu'] ? 1 : 0) ."'
      AND u.del=0
      AND c.bundesland='".$this->bundesland."'" . $this->search_text("u.");

      $sql .= " GROUP BY uid";
    } else {

      $sql = "SELECT u.*, TIMESTAMPDIFF(YEAR,geburtsdatum,CURDATE()) AS age
      FROM " . DB_CITIES ." c
      LEFT JOIN ".DB_USERS." u ON c.stadt=u.wohnort
      WHERE u.uid!='' AND u.eu='". ($_REQUEST['eu'] ? 1 : 0) ."' AND u.del=0 AND c.bundesland='".$this->bundesland."'" . $this->search_text("u.");
    }

    if ( ! empty( $_REQUEST['orderby'] ) ) {
      $sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
      $sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
    } else {
      $sql .= ' ORDER BY u.updatetime desc ';
    }

    $sql .= " LIMIT " . $this->per_page;
    $sql .= ' OFFSET ' . ( $this->page_number - 1 ) * $this->per_page;
    /*var_dump( $sql );*/
    return $sql;
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
	public function search_text( $alias )
  {
    $s = isset($_REQUEST["s"]) ? $_REQUEST["s"] : NULL;
    if( empty($s) )
      return '';

    $search = $this->FbGeneral->search_text( $alias );
    if( $search ){
      return $search;
    }
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

  public function table_data(){
    global $wpdb;
    return $wpdb->get_results( $this->sql() );
  }
  public function record_count()
  {
    global $wpdb;
    if (!$this->sql()){
      return 0;
    }
    if( $this->uid ){
      $sql  = substr($this->sql(), 0, strpos($this->sql(), 'GROUP')-1);
    } else {
      $sql  = substr($this->sql(), 0, strpos($this->sql(), 'ORDER')-1);
    }
    //$sql  = str_replace('u.*','COUNT(*)', $sql);
    $sql  = str_replace('u.*','COUNT(distinct u.uid)', $sql);
    $sql  = str_replace(', COUNT(ef.uid) as anz','', $sql);

    $data = $wpdb->get_var($sql);
    return $data;
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
  public function cols(){
    $cols = $this->FbGeneral->userCols();
    return $cols;
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
  public function display(){

    $cols = $this->cols();
    array_push($cols, "anz");

    $this->items = $this->table_data();
    $this->set_pagination_args( array(
      'total_items' => $this->record_count(),
      'per_page'    => $this->per_page
    ) );
    $args = array("type" => "select", "name" => "column", "label" => "Column name", "data" => $cols);

    $out = '<form>';
    $out .= '<div class="tablenav">';
    $out .= '<div class="alignleft">' . $this->extra_tablenav( $args ) . ' ' . $this->search_box( __("Search") ) .'</div>';
    $out .= '<div class="alignright">' . $this->pagination("top") .'</div>';
    $out .= '</div>';
    $out .= '<table class="wp-list-table widefat striped">';
    $out .= '<tr>';

    foreach( $cols as $col ){
      $out .= '<th class="'.$col.'">'.$this->tableCols($col).'</th>';
    }
    $out .= '<th></th>';
    $out .= '</tr>';

    if ( is_array( $this->items ) ){
    foreach( $this->items as $user ){
      $this->FbGeneral->getUser($user);
      $out .= '<tr>';
      foreach( $cols as $col ){
        $out .= '<td class="'.$col.'">'.$this->FbGeneral->$col().'</td>';
      }
      $out .= '<td class="edit">'.$this->FbGeneral->useredit('_blank').'</td>';
      $out .= '</tr>';
      }
    }
    $out .= '</table>';
    $out .= '<div class="tablenav">';
    $out .= '<div class="alignright">' . $this->pagination("top") .'</div>';
    $out .= '</div>';
    $out .= '</form>';

    return $out;
  }
}

?>
