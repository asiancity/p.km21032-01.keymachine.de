<?php
/**
 *
 */
class MSFbForeignAccounts extends MSTable
{
  public $uid;
  public $FbGeneral;
  function __construct()
  {
    parent::__construct();
    $this->FbGeneral = new FbGeneralClasses;
    $request = $_GET;
    unset($request["column"]);
    $this->request = $request;
  }
  public function sql(){
    global $wpdb;
    $sql = "SELECT u.*, TIMESTAMPDIFF(YEAR,geburtsdatum,CURDATE()) AS age, f.* FROM " . DB_FRIENDS . " f
    LEFT JOIN ".DB_USERS." u ON f.uid=u.uid
    WHERE u.uid!=''" . $this->search_text("u.");

    if ( ! empty( $_REQUEST['orderby'] ) ) {
      $sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
      $sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
    } else {
      $sql .= ' ORDER BY anz desc ';
    }

    $sql .= " LIMIT " . $this->per_page;
    $sql .= ' OFFSET ' . ( $this->page_number - 1 ) * $this->per_page;
    /*var_dump($sql);*/
    return $sql;
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
	public function search_text( $alias )
  {
    $s = isset($_REQUEST["s"]) ? $_REQUEST["s"] : NULL;
    if( empty($s) )
      return '';

    $search = $this->FbGeneral->search_text( $alias );
    if( $search ){
      return $search;
    }

    return " AND ".$alias."names LIKE '%".$s."%'";
  }
  public function table_data(){
    global $wpdb;
    return $wpdb->get_results( $this->sql() );
  }
  public function record_count()
  {
    global $wpdb;
    $sql  = substr($this->sql(), 0, strpos($this->sql(), 'ORDER')-1);

    $sql  = str_replace('u.*','COUNT(*)', $sql);
    $sql  = str_replace(', f.*','', $sql);
    /*var_dump($sql);*/
    $data = $wpdb->get_var($sql);
    return $data;
  }
  /**
   * Block comment
   *
   * @param type
   * @return void
   */
  public function cols(){
    $cols = $this->FbGeneral->userCols();
    return $cols;
  }

  /**
   * Block comment
   *
   * @param type
   * @return void
   */
  public function display(){

    $cols = $this->cols();
    array_push($cols, "anz");

    $this->items = $this->table_data();
    $this->set_pagination_args( array(
      'total_items' => $this->record_count(),
      'per_page'    => $this->per_page
    ) );
    $args = array("type" => "select", "name" => "column", "label" => "Column name", "data" => $cols);

    $out = '<form><div class="tablenav">';
    $out .= '<div class="alignleft">' . $this->extra_tablenav( $args ) . ' ' . $this->search_box( __("Search") ) .'</div>';
    $out .= '<div class="alignright">' . $this->pagination("top") .'</div>';
    $out .= '</div>';
    $out .= '<table class="wp-list-table widefat striped">';
    $out .= '<tr>';

    foreach( $cols as $col ){
    $out .= '<th class="'.$col.'">'.$this->tableCols($col).'</th>';
    }
    $out .= '<th></th>';
    $out .= '</tr>';

    foreach( $this->items as $user ){
      $this->FbGeneral->getUser($user);

      $out .= '<tr>';
      foreach( $cols as $col ){
      $out .= '<td class="'.$col.'">'.$this->FbGeneral->$col().'</td>';
      }
      $out .= '<td class="edit">'.$this->FbGeneral->useredit('_blank').'</td>';
      $out .= '</tr>';
    }
    $out .= '</table></form>';

    return $out;
  }

  public function checkForeignAccount(){
    global $wpdb;
    $anzahl = $wpdb->get_var( "SELECT COUNT(*) FROM fb_friends WHERE uid ='". $_REQUEST["fromuid"] ."'" );
    if ($anzahl > 0){
      $arr = array("status" =>"Account existiert" );
    }
    else if ($anzahl != $_REQUEST["anz"]){
      $arr = array("status" =>"Es gibt neue Freunde" );
    } else {
      $arr = array("status" =>"Der Vorgang kann fortgesetzt werden." );
    }
    wp_send_json($arr);
    wp_die();
  }
}

?>
