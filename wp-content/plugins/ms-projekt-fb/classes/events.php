<?php
/**
 *
 */
class MSFbEvents extends MSTable
{
  public $uid;
  public $FbGeneral;
  function __construct()
  {
    parent::__construct();
    $this->uid = isset($_REQUEST["uid"]) ? $_REQUEST["uid"] : NULL;
    $this->db = DB_USERS;
    $this->FbGeneral = new FbGeneralClasses;
  }
  public function sql(){
    global $wpdb;
    $sql = "SELECT u.*, COUNT(ef.uid) as anz, TIMESTAMPDIFF(YEAR,geburtsdatum,CURDATE()) AS age FROM " . DB_EVENTS_FRIENDS . " ef
    LEFT JOIN ".DB_EVENTS." e ON ef.eventfbid=e.eventfbid
    LEFT JOIN ".DB_USERS." u ON ef.uid=u.uid
    WHERE u.uid!='' AND e.cat='asia' GROUP BY uid";

    if( $this->uid ){
      $sql = "SELECT u.*, COUNT(ef.uid) as anz, TIMESTAMPDIFF(YEAR,geburtsdatum,CURDATE()) AS age, af.itime
      FROM " . DB_EVENTS_FRIENDS . " ef
      LEFT JOIN ".DB_EVENTS." e ON ef.eventfbid=e.eventfbid
      LEFT JOIN ".DB_USERS." u ON ef.uid=u.uid
      LEFT JOIN ".DB_AKT_FRIENDS." af ON af.uid=u.uid
      WHERE u.uid!='' AND e.cat='asia'
      AND u.uid IN (SELECT uid FROM ".DB_AKT_FRIENDS." af2 WHERE af2.fromuid='".$this->uid."' )
      GROUP BY uid";
    }
    if ( ! empty( $_REQUEST['orderby'] ) ) {
      $sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
      $sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
    } else {
      $sql .= ' ORDER BY anz desc ';
    }

    $sql .= " LIMIT " . $this->per_page;
    $sql .= ' OFFSET ' . ( $this->page_number - 1 ) * $this->per_page;
    return $sql;
  }
  public function table_data(){
    global $wpdb;
    return $wpdb->get_results( $this->sql() );
  }
  public function record_count()
  {
    global $wpdb;
    $sql  = substr($this->sql(), 0, strpos($this->sql(), 'GROUP')-1);

    $sql  = str_replace('u.*','COUNT(distinct u.uid)', $sql);
    $sql  = str_replace(', COUNT(ef.uid) as anz','', $sql);
    /*var_dump($sql);*/
    $data = $wpdb->get_var($sql);
    return $data;
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
  public function cols(){
    $cols = $this->FbGeneral->userCols();
    return $cols;
  }
  public function userSingleEvents()
  {
    global $wpdb;
    $cols = $wpdb->get_col( "DESC " . DB_EVENTS, 0 );
    $sql = "SELECT e.* FROM " . DB_EVENTS_FRIENDS . " ef LEFT JOIN " . DB_EVENTS ." e ON ef.eventfbid=e.eventfbid WHERE ef.uid='". $this->uid."'";
    if ( ! empty( $_REQUEST['orderby'] ) ) {
      $sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
      $sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
    } else {
      $sql .= ' ORDER BY eid desc ';
    }

    $events = $wpdb->get_results( $sql );
    $iconFB = '<span class="dashicons dashicons-facebook black"></span>';

    $out = '<table class="wp-list-table widefat striped">';
    $out .= '<tr>';

    foreach( $cols as $col ){
    $out .= '<th class="'.$col.'">'.$this->tableCols($col).'</th>';
    }
    $out .= '</tr>';

    foreach( $events as $event ){

      $out .= '<tr>';
      foreach( $cols as $col ){
        if ($col == 'eventfbid') {
          $out .= '<td class="'.$col.'"><a target="_blank" href="https://www.facebook.com/'.$event->$col.'">'.$iconFB.'</a> '.$event->$col.'</td>';
        } else {
          $out .= '<td class="'.$col.'">'.$event->$col.'</td>';
        }
      }
      $out .= '</tr>';
    }
    $out .= '</table>';
    return $out;
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
  public function display(){

    $cols = $this->cols();
    array_push($cols, "anz");

    $this->items = $this->table_data();
    $this->set_pagination_args( array(
      'total_items' => $this->record_count(),
      'per_page'    => $this->per_page
    ) );
    $args = array("type" => "select", "name" => "column", "label" => "Column name", "data" => $cols);

    $out = '<div class="tablenav">';
    if( $this->uid ){
    $out .= '<div class="alignleft">Hier sind Deine Freunde | '. $this->uid.'.</div>';
    }

    $out .= '<div class="alignright">' . $this->pagination("top") .'</div>';
    $out .= '</div>';
    $out .= '<table class="wp-list-table widefat striped">';
    $out .= '<tr>';

    foreach( $cols as $col ){
    $out .= '<th class="'.$col.'">'.$this->tableCols($col).'</th>';
    }
    $out .= '<th></th>';
    $out .= '</tr>';

    foreach( $this->items as $user ){
      $this->FbGeneral->getUser($user);

      $out .= '<tr>';
      foreach( $cols as $col ){
      $out .= '<td class="'.$col.'">'.$this->FbGeneral->$col().'</td>';
      }
      $out .= '<td class="edit">'.$this->FbGeneral->useredit('_blank').'</td>';
      $out .= '</tr>';
    }
    $out .= '</table>';

    return $out;
  }
  public static function api(){
    global $wpdb;
    $MSFbEvents = new MSFbEvents;
    $sql = $MSFbEvents->sql();
    $data = $wpdb->get_results( $sql );
    wp_send_json($data);
    wp_die();
  }
  public static function allEvents(){
    global $wpdb;
    $sql  = "SELECT * FROM " . DB_EVENTS ." ORDER BY cat asc";
    $data  = $wpdb->get_results( $sql );
    wp_send_json($data);
    wp_die();
  }
  public static function eventUsers(){
    global $wpdb;
    $eventfbid  = isset( $_REQUEST['eventfbid'] ) ? $_REQUEST['eventfbid'] : '';
    // $sql = "SELECT ef.* FROM " . DB_EVENTS ." e LEFT JOIN  " . DB_EVENTS_FRIENDS . " ef ON e.eventfbid=ef.eventfbid WHERE ef.eventfbid='". $eventfbid ."'";
    $sql = "SELECT u.*, COUNT(ef.uid) as anz, TIMESTAMPDIFF(YEAR,geburtsdatum,CURDATE()) AS age FROM " . DB_EVENTS_FRIENDS . " ef
    LEFT JOIN ".DB_EVENTS." e ON ef.eventfbid=e.eventfbid
    LEFT JOIN ".DB_USERS." u ON ef.uid=u.uid
    WHERE u.uid!='' AND ef.eventfbid='". $eventfbid ."' GROUP BY uid";

    $data  = $wpdb->get_results( $sql );
    wp_send_json($data);
    wp_die();
  }
  public static function eventbyuser(){
    global $wpdb;
    $uid      = isset( $_REQUEST['uid'] ) ? $_REQUEST['uid']           : '';
    $username = isset( $_REQUEST["username"] ) ? $_REQUEST["username"] : '';

    if ( !is_numeric($uid) ){
      $sql    = "SELECT * FROM ". DB_USERS ." WHERE username='".$username."'";
      $user   = $wpdb->get_row( $sql );
      $uid    = ($user) ? $user->uid : wp_die();
    }

    $sql      = "SELECT COUNT(*) as anz FROM " . DB_EVENTS_FRIENDS . " WHERE eventfbid='9999' AND uid='".$uid."'";
    $data     = $wpdb->get_row( $sql );
    wp_send_json($data);
    wp_die();
  }
  public static function tableSimple(){
    return '<div class="events"></div>';
  }
}

?>
