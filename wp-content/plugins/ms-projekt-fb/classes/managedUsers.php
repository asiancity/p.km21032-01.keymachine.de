<?php
/**
 *
 */
class MSFbManagedUsers extends MSTable
{
  public $uid;
  public $FbGeneral;
  public function __construct()
  {
    parent::__construct();
    $this->uid = isset($_REQUEST["uid"]) ? $_REQUEST["uid"] : '';
    $this->db = DB_USERS;
    $this->FbGeneral = new FbGeneralClasses;

    $request = $_GET;
    unset($request["column"]);
    $this->request = $request;

  }
  /**
   * Block comment
   *
   * @param type
   * @return void
   */

  public function managedUsers()
  {
    global $wpdb;
    $sql = "SELECT u.*, COUNT(u.uid) as anz FROM " . DB_AKT_FRIENDS . " af LEFT JOIN ".DB_USERS." u ON af.fromuid=u.uid WHERE u.uid!='' GROUP BY af.fromuid ORDER BY u.first_name ASC";
    return $wpdb->get_results( $sql );
  }
  /**
   * Block comment
   *
   * @param type
   * @return void
   */

  public function userlist()
  {
    $users  = $this->managedUsers();
    $out    = '<table class="wp-list-table widefat striped">';
    foreach( $users as $user ){
      $this->FbGeneral->getUser($user);

      $out .= '<tr '. (isset($_REQUEST["uid"]) && ($user->uid == $_REQUEST["uid"]) ? 'class="selected"' : '') .'>';
      $out .= '<td>'.$this->FbGeneral->accountpic(array("page" => "page=ath-facebook-managed-users")).'</td>';
      $out .= '<td>'.$this->FbGeneral->fullname().'</td>';
      $out .= '<td>'.$user->anz.'</td>';
      $out .= '</tr>';
    }
    $out .= '</table>';
    return $out;
  }

  /**
   * Block comment
   *
   * @param type
   * @return void
   */
  public function usersql(){
    global $wpdb;
    $sql = "SELECT af.itime, u.*, TIMESTAMPDIFF(YEAR,geburtsdatum,CURDATE()) AS age FROM " . DB_AKT_FRIENDS . " af
    LEFT JOIN ".DB_USERS." u ON af.uid=u.uid
    WHERE u.uid!='' and af.fromuid='".$this->uid."' ".$this->search_text("u.");

    if ( ! empty( $_REQUEST['orderby'] ) ) {
      $sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
      $sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
    } else {
      $sql .= ' ORDER BY u.updatetime desc ';
    }

    $sql .= " LIMIT " . $this->per_page;
    $sql .= ' OFFSET ' . ( $this->page_number - 1 ) * $this->per_page;
    /*var_dump( $sql );*/
    return $sql;
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
	public function search_text( $alias )
  {
    $s = isset($_REQUEST["s"]) ? $_REQUEST["s"] : '';
    if( empty($s) )
      return '';

    $search = $this->FbGeneral->search_text( $alias );
    if( $search ){
      return $search;
    }

    return " AND ".$alias."names LIKE '%".$s."%'";
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

  public function table_data(){
    global $wpdb;
    return $wpdb->get_results( $this->usersql() );
  }
  public function record_count()
  {
    global $wpdb;
    $sql  = substr($this->usersql(), 0, strpos($this->usersql(), 'ORDER')-1);
    $sql  = str_replace('u.*','COUNT(*)', $sql);
    $sql  = str_replace('af.itime,','', $sql);
    $data = $wpdb->get_var($sql);

    return $data;
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
  public function cols(){
    $cols = $this->FbGeneral->userCols();
    return $cols;
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
  public function display(){

    $cols = $this->cols();
    array_push($cols, "itime");

    $this->items = $this->table_data();
    $this->set_pagination_args( array(
      'total_items' => $this->record_count(),
      'per_page'    => $this->per_page
    ) );
    $args = array("type" => "select", "name" => "column", "label" => "Column name", "data" => $cols);

    $out = '<div class="tablenav">';
    $out .= '<div class="alignleft"><form>' . $this->extra_tablenav( $args ) . ' ' . $this->search_box( __("Search") ) .'</form></div>';
    $out .= '<div class="alignright">' . $this->pagination("top") .'</div>';
    $out .= '</div>';
    $out .= '<table class="wp-list-table widefat striped">';
    $out .= '<tr>';

    foreach( $cols as $col ){
    $out .= '<th class="'.$col.'">'.$this->tableCols($col).'</th>';
    }
    $out .= '<th></th>';
    $out .= '</tr>';

    foreach( $this->items as $user ){
      $this->FbGeneral->getUser($user);

      $out .= '<tr>';
      foreach( $cols as $col ){
      $out .= '<td class="'.$col.'">'.$this->FbGeneral->$col().'</td>';
      }
      $out .= '<td class="edit">'.$this->FbGeneral->useredit('_blank').'</td>';
      $out .= '</tr>';
    }
    $out .= '</table>';

    return $out;
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

  public function ourFriends()
  {
    global $wpdb;
    $sql = "SELECT u.* FROM " . DB_AKT_FRIENDS ." af LEFT JOIN ".DB_USERS." u ON af.fromuid=u.uid WHERE af.uid='".$this->uid."' ORDER BY u.updatetime desc";
    $users = $wpdb->get_results($sql);

    if( count($users) < 1 )
      return '';

    $args = array(
      "data" => $users,
      "cols" => $this->FbGeneral->userCols()
    );
    $MSTableSimple = new MSTableSimple( $args );
    return $MSTableSimple->display();
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

  public static function apiOurFriends()
  {
    global $wpdb;
    $MSFbManagedUsers = new MSFbManagedUsers;
    $sql = "SELECT u.*, af.itime FROM " . DB_AKT_FRIENDS ." af LEFT JOIN ".DB_USERS." u ON af.uid=u.uid WHERE
    af.fromuid='".$MSFbManagedUsers->uid."'
    AND u.eu=0
    AND u.wohnort LIKE '%erli%'
    AND af.uid NOT IN (SELECT uid FROM ".DB_POSTS.")
    ORDER BY af.itime desc";
    $sql .= " LIMIT " . $MSFbManagedUsers->per_page;
    $sql .= ' OFFSET ' . ( $MSFbManagedUsers->page_number - 1 ) * $MSFbManagedUsers->per_page;
    $users = $wpdb->get_results($sql);
    //var_dump($sql);
    wp_send_json($users);
    wp_die();
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
	public static function apifriendship(){
    global $wpdb;
    $fromuid          = isset( $_REQUEST["fromuid"] ) ? $_REQUEST["fromuid"] : '';
    if(!$fromuid) wp_die('KEINE fromuid');
    $MSFbManagedUsers = new MSFbManagedUsers;
    $sql              = "SELECT * FROM " . DB_AKT_FRIENDS . " WHERE uid='".$MSFbManagedUsers->uid."' AND fromuid='". $_REQUEST["fromuid"] ."'";
    $user             = $wpdb->get_row( $sql );

    wp_send_json($user);
    wp_die();
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
	public static function apiAddfriends(){
    global $wpdb;

  	if ( !$_REQUEST["fromuid"] ){
      return 'Error';
    }
    if( !is_numeric($_REQUEST["eu"]) ){
      return 'Error';
    }
    $fromuid = $_REQUEST["fromuid"];
    $eu = $_REQUEST["eu"];

    $fb_akt_friends = $wpdb->get_row("SELECT * FROM ".DB_ACCOUNTS." WHERE website='facebook' and fbuid = '".$fromuid."'");
    $uids = $_REQUEST["link"];
    $uids = explode(",", $uids);

    $c = 1;
    $now = current_time( 'Y-m-d H:i:s' );
    foreach ($uids as $uid) {
        if ( $uid ){
            if( $fb_akt_friends->id ){
              $sql = "INSERT ".DB_AKT_FRIENDS." SET uid ='".trim($uid)."', fromuid='".trim($fromuid)."', itime='".$now."'";
              if (!$wpdb->query( $sql ) ){
                $sql = "UPDATE ".DB_AKT_FRIENDS." SET itime='".$now."' WHERE uid ='".trim($uid)."' and fromuid='".trim($fromuid)."'";
                $wpdb->query( $sql );
              }
            }
            $sql = "INSERT ".DB_FRIENDS_FRIENDS." SET uid ='".trim($uid)."', fromuid='".trim($fromuid)."', updatetime='".$now."'";
            if (!$wpdb->query( $sql ) ){
              $sql = "UPDATE ".DB_FRIENDS_FRIENDS." SET updatetime='".$now."' WHERE uid ='".trim($uid)."' and fromuid='".trim($fromuid)."'";
              $wpdb->query( $sql );
            }

            $sql = "INSERT ".DB_USERS." SET uid ='".trim($uid)."', eu='".$eu."'";
            if ($wpdb->query( $sql ) ){
              $c++;
            }
        }
    }
    $data = array("result" => 1, "anz"=> $c);
    wp_send_json( $data );
  	wp_die();
  }
}
?>
