<?php
/**
 *
 */
class MSFbUsertable extends WP_List_Table
{
  public $uid;
  public $user;
  public $names;
  public $hiddenColumns = MSHiddenColumns;
  public function __construct()
  {

    //Set parent defaults
    parent::__construct( array(
        'singular'  => 'user',     //singular name of the listed records
        'plural'    => 'users',    //plural name of the listed records
        'ajax'      => true        //does this table support ajax?
    ));
  }
  /*
		fn
	*/
  public function get_cols(){
    global $wpdb;
    $FbGeneralClasses = new FbGeneralClasses;

    /*$cols = $wpdb->get_col( "DESC " . DB_USERS, 0 );
    if(($key = array_search('names', $cols)) !== false) {
        unset($cols[$key]);
    }*/
    return $FbGeneralClasses->userColsNoHidden();
	}

  /*
		fn
	*/
	public function sql( $per_page = 10, $page_number = 1  )
	{
		$sql = "SELECT * FROM ". DB_USERS ." WHERE uid!=''" . $this->search_text();

		if ( ! empty( $_REQUEST['orderby'] ) ) {
			$sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
			$sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
		} else {
			$sql .= ' ORDER BY updatetime desc ';
		}

    $sql .= " LIMIT ".$per_page;
		$sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;
		return $sql;
	}
  public function search_text()
  {
    $sql = '';
    /*$search = MSFbManagedUsers::search_text( $alias );
    if( $search ){
      return $search;
    }*/
    if ( ! empty( $_REQUEST['s'] ) ) {
				$sql .= " AND (
        first_name LIKE '%".$_REQUEST["s"]."%' OR
        names LIKE '%".$_REQUEST["s"]."%' OR
        uid LIKE '%".$_REQUEST["s"]."%'
        )";
		}
    return $sql;
  }
  /*
    fn
  */
  public function table_data( $per_page = 10, $page_number = 1 )
  {
    global $wpdb;

    $sql       = $this->sql( $per_page, $page_number );
    $data      = $wpdb->get_results($sql, ARRAY_A);

    return $data;
  }
  /*
    fn
  */
  public function record_count()
  {
    global $wpdb;
    $sql  =  substr($this->sql(), 0, strpos($this->sql(), 'ORDER')-1);
    $sql  = str_replace('*','COUNT(*)', $sql);
    $data = $wpdb->get_var($sql);

    return $data;
  }
  /*
		fn
	*/
	public function get_hidden_columns()
  {
		$columns =  (array) get_user_option( $this->hiddenColumns );
    //var_dump( $columns );
		if ( count( $columns )  < 1 ){
			$columns = array('nophoto', 'del', 'api_error', 'work', 'education', 'locale', 'updatetime');
		}
    return $columns;
  }
  /*
    fn
  */
  public function get_sortable_columns()
  {
    $sortable_columns = array();
    $cols = $this->get_cols();

    foreach( $cols as $col ){
      $sortable_columns[ $col ] = array( $col, true );
    }

    return $sortable_columns;
  }
  /*
    fn
  */
  public function column_default( $item, $column_name )
  {
    $FbGeneralClasses = new FbGeneralClasses;
    $FbGeneralClasses->getUser($item);
    switch ($column_name) {

      default:
        return $FbGeneralClasses->$column_name();
        break;
    }
  }
  /*
		fn
	*/
	public function column_uid($item){
    $page     = '';
    $paged    = '';
    $orderby  = '';
    $order    = '';
    $s        = '';
    extract( $_REQUEST );
		$actions = array(
  			'edit'  => sprintf('<a href="?page=%s&action=%s&uid=%s&paged=%d&orderby=%s&order=%s&s=%s">Edit</a>',
        $page,
        'edit',
        $item['uid'],
        $paged,
        $orderby,
        $order,
        $s),
		);
		return sprintf('%1$s %2$s',$item['uid'],$this->row_actions($actions));
	}
  /*
    fn
  */
  function get_columns()
  {
    $column_name = array();
    $cols = $this->get_cols();

    foreach( $cols as $col ){
      $column_name[ $col ] = $col;
    }

    return $column_name;
  }

  /*
    fn
  */
  public function get_table_classes() {
    return array( 'widefat', '', 'striped', $this->_args['plural'] );
  }
  /*
    fn
  */
  public function prepare_items()
  {
    /** Process bulk action */
    $this->process_bulk_action();

    $user			      = get_current_user_id();
    $screen			    = get_current_screen();
    $screen_option	= $screen->get_option('per_page', 'option');
    $per_page		    = get_user_meta($user, $screen_option, true);
    $current_page	  = $this->get_pagenum();
    $total_items 	  = $this->record_count();

    if ( empty ( $per_page ) || $per_page < 1 ) {
      $per_page	= $screen->get_option( 'per_page', 'default' );
    }

    $this->set_pagination_args( array(
      'total_items' => $total_items, //WE have to calculate the total number of items
      'per_page'    => $per_page //WE have to determine how many items to show on a page
    ) );

    $columns  = $this->get_columns();
    $hidden   = $this->get_hidden_columns();
    $sortable = $this->get_sortable_columns();
    $this->_column_headers = array($columns, $hidden, $sortable);
    $this->items = $this->table_data( $per_page, $current_page );
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

  public function singleItem()
  {
    global $wpdb;
    $sql = "SELECT * FROM ". DB_USERS ." WHERE uid='". $_REQUEST["uid"] ."'";
    return $wpdb->get_row( $sql );
  }
  public function SingleUpdate(){
    global $wpdb;

    if( !isset($_POST["mode"]) || $_POST["mode"] != "update")
      return "";

    $cols = $this->get_cols();
    $delCols = array("userpic", "names", "age", "uid", "id");
    foreach( $delCols as $delCol ){
      if(($key = array_search($delCol, $cols)) !== false) {
          unset($cols[$key]);
      }
    }

    foreach($cols as $col ) $data[ $col ] = $_REQUEST[ $col ];

		$format			  = '%s';
		$where			  = array('uid' => $_POST["uid"]  );
		$whereformat	= '%d';

		$result = $wpdb->update( DB_USERS, $data, $where, $format, $where_format );

    if( $result ){
      echo '<div class="updated">'. __("Update changes") .'</div>';
    } else {
      echo '<div class="error">'. __("hallo") .'</div>';
    }
  }
  public function singleDisplay()
  {
    $this->SingleUpdate();
    $FbGeneralClasses = new FbGeneralClasses;
    $FbGeneralClasses->getUser($this->singleItem());
    $cols = $this->get_cols();

    $out = '<form method="POST"><input type="hidden" name="mode" value="update">';
    $out .= '<table class="wp-list-table widefat striped">';
    foreach( $cols as $col ){
      switch ($col) {
        case 'userpic':
        case 'age':
          $args = array("type" => "empty", "value" => $FbGeneralClasses->$col() );
          break;

        case 'eu':
        case 'keinviet_mask':
        case 'del':
        case 'nophoto':
          $args = array("type" => "checkbox", "name" => $col, "value" => $FbGeneralClasses->$col() );
          break;

        case 'id':
        case 'uid':
        case 'username':
          $args = array("type" => "hiddenShowValue", "name" => $col, "value" => $FbGeneralClasses->$col() );
          break;

        case 'geburtsdatum':
          $args = array("type" => "text", "name" => $col, "value" => $this->singleItem()->$col );
          break;

        default:
          $args = array("type" => "text", "name" => $col, "value" => $FbGeneralClasses->$col() );
          break;
      }
      $out .= '<tr><td>'.$col.'</td><td>'. MSInput::display( $args ) .'</td></tr>';
    }
    $out .= '<tr><td colspan="2" align="right">'. get_submit_button( __("Save Changes"), 'primary', 'save', false  ) .'</td></tr>';
    $out .= '</table>';
    $out .= '</form>';
    return $out;
  }
}
?>
