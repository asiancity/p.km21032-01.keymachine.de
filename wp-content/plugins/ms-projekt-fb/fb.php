<?php
/*
Plugin Name: ATH Facebook
Plugin URI:
Description: ATH Facebook
Version: 1.201703.14
Author: Anh-Tuan Hoang
Author URI:
*/
if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
if(!class_exists('MSDashboard')){
    wp_die( sprintf( __("No plugins found for &#8220;%s&#8221;."), "MSDashboard - ms_main" ) .'<br><br>'. plugin_dir_path( __FILE__ ) );
}
define("DB_ACCOUNTS", "fb_accounts");
define("DB_USERS", "fb_users");
define("DB_AKT_FRIENDS", "fb_akt_friends");
define("DB_EVENTS", "fb_events");
define("DB_EVENTS_FRIENDS", "fb_events_friends");
define("DB_RESERVATION", "fb_users_reservation");
define("DB_FRIENDS", "fb_friends");
define("DB_FRIENDS_FRIENDS", "fb_friends_Of_friends");
define("DB_CITIES", "fb_german_cities");
define("DB_POSTS", "fb_posts");
define("DB_FOTO_MASK", "fb_foto_markieren");
define("MSHiddenColumns", "managetoplevel_page_ath-facebookcolumnshidden");

require("classes/usertable.php");	//FbGeneralClasses
require("classes/general.php");	//FbGeneralClasses
require("classes/pages.php");
require("classes/managedUsers.php"); //MSFbManagedUsers
require("classes/events.php"); //MSFbEvents
require("classes/foreignAccounts.php"); //MSFbForeignAccounts
require("classes/geo.php"); //MSFbGeo
require("classes/options.php"); //MSFbOptions
require("classes/apiUpdate.php"); //MSFbApiUpdate
require("classes/postedByUsers.php"); //MSFbPostedByUsers
require("classes/photoPostByUsers.php"); //MSFbPhotoPostByUsers
require("classes/reservation.php");
require("classes/user.php");

require_once("admin/index.php");
/**
 *
 */
class MSFacebook extends MSFbPages
{

  public $dashicon = 'dashicons-facebook';

	/**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
  public function __construct()
  {
		parent::__construct();
    add_action( 'admin_menu', array($this, 'admin_menu' ));
    add_action( 'admin_enqueue_scripts', array($this, 'enqueue') );

    $class  = ( isset($_REQUEST['class']) ) ? $_REQUEST['class'] : '';
    $func   = ( isset($_REQUEST['func']) ) ? $_REQUEST['func'] : '';
    if( $class ){
      // var_dump($class);
      add_action( 'wp_ajax_'.$class.'_'.$func, array($class, $func) );
		  add_action( 'wp_ajax_nopriv_'.$class.'_'.$func, array($class , $func) );
    }
  }

	/**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

  public function admin_menu()
  {
    global $fb_page_settings;
    $fb_page_settings = add_menu_page('Facebook', 'Facebook', 'edit_pages', 'ath-facebook', array($this, 'page_actions_users'), $this->dashicon, 13);
		$fb_page_settings_02 = add_submenu_page( 'ath-facebook', 'verwaltete Accounts', 'verwaltete Accounts', 'edit_pages', 'ath-facebook-managed-users', array($this, 'page_actions_managedUsers'));
		$fb_page_settings_03 = add_submenu_page( 'ath-facebook', 'Freundschaftsanfragen', 'Freundesanfragen', 'edit_pages', 'ath-facebook-friends-request', array($this, 'page_actions_friendsRequest'));
		$fb_page_settings_04 = add_submenu_page( 'ath-facebook', 'Gecrawlte Fremd-Accounts', 'Foreign Accounts', 'edit_pages', 'ath-facebook-foreign-users', array($this, 'page_actions_foreignAccounts'));
		$fb_page_settings_05 = add_submenu_page( 'ath-facebook', 'Events', 'Events', 'edit_pages', 'ath-facebook-events', array($this, 'page_actions_events'));
		$fb_page_settings_06 = add_submenu_page( 'ath-facebook', 'Geografisch', 'Geografisch', 'edit_pages', 'ath-facebook-geo', array($this, 'page_actions_geo'));
		$fb_page_settings_07 = add_submenu_page( 'ath-facebook', 'Gruppen', 'Gruppen', 'edit_pages', 'ath-facebook-groups', array($this, 'page_actions_groups'));
    $fb_page_settings_08 = add_submenu_page( 'ath-facebook', 'Einstellungen', 'Einstellungen', 'edit_pages', 'ath-facebook-options', array($this, 'page_actions_options'));
		add_action( "load-{$fb_page_settings}", array($this, 'add_options') );
		//add_action( "load-{$fb_page_settings_02}", array($this, 'add_options') );

    add_action( "admin_head-" . $fb_page_settings, array($this, "help_tab") );
    add_action( "admin_head-" . $fb_page_settings_02, array($this, "help_tab") );
    add_action( "admin_head-" . $fb_page_settings_05, array($this, "help_tab") );
    add_action( "admin_head-" . $fb_page_settings_06, array($this, "help_tab") );
    add_action( "admin_head-" . $fb_page_settings_08, array($this, "help_tab") );
	}
  /*
    fn
  */
  public function enqueue() {
    wp_enqueue_style( 'plugin-msfacebook', plugin_dir_url(__FILE__) .'css/style.css' );
    wp_enqueue_script( 'plugin-msfacebook-script', plugin_dir_url(__FILE__). 'js/script.js' );
  }
	/**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

	public function add_options()
	{
		global $fb_page_settings, $fb_page_settings_02;
		$screen = get_current_screen();

		if(!is_object($screen))
			return;

		$option = 'per_page';
		$args = array(
			'label' => 'Users',
			'default' => 30,
			'option' => 'users_per_page'
		);

		add_screen_option( $option, $args );
		$MSFbUsertable = new MSFbUsertable;
	}
  public function help_tab() {

  		if (!class_exists('help_tabs')) {
  			$class = 'notice notice-error';
  			$message = 'No Class "help_tabs". ' . sprintf(__("Install %s now"), 'Plugin (help_tabs)');
  			printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message );
  			return '';
  		}

  		$screen		= get_current_screen();
  		$dir = plugin_dir_path( __FILE__ ) . '/helps';
  		$help_tabs = new help_tabs( $screen, $dir );
  }
}
if( is_admin() )
{
	new MSFacebook;
}
?>
