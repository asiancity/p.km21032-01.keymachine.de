jQuery(document).ready( function($) {

	$(document).on('click', '.msfacebook a.ajax-update', function(e){
    var $this = $(this);
    $.ajax({
        url: $this.data("href"),
        data: '',
        dataType: 'jsonp',
        type:'post',
        beforeSend: function(formData, jqForm, options) {},
        complete : function(jqXHR, textStatus) {
          var response = $.parseJSON( jqXHR.responseText );
          $this.text(response.statustext);
        },
    });
    return false;
  });
});
