<?php
/*
Plugin Name: ATH Main Plugin
Plugin URI:
Description: Progressbar für ATH Newsletter, ATH User, ATH Mobile
Version: 1.201703.10
Author: Anh-Tuan Hoang
Author URI:
*/

require("classes/db.php");
require("classes/dashboard.php");
require("classes/charts.php");
require("classes/table.php");
require("classes/table_simple.php");
require("classes/inputs.php");

if( is_admin() )
{
	new msMain();
}

class msMain
{
	public $dashicon = 'dashicons-warning';

  function __construct()
  {

		add_action( 'admin_menu', array($this, 'newsletter_admin_menu' ));
    add_action( 'admin_enqueue_scripts', array($this, 'enqueue') );
		add_action( 'admin_print_scripts', array($this, 'admin_inline_js'));
  }
  public static function enqueue() {

    wp_enqueue_style( 'dataTables', 'https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.13/css/jquery.dataTables.css' );
    wp_enqueue_style( 'ms-main', plugin_dir_url(__FILE__) .'css/style.css' );
    wp_enqueue_style( 'ms-main-progressbar', plugin_dir_url(__FILE__) .'css/progressbar.css' );

    wp_enqueue_script( 'dataTables-js', 'https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.13/js/jquery.dataTables.min.js' );
		wp_enqueue_script( 'ms-main-script', plugin_dir_url(__FILE__). 'js/script.js' );
  }
	public function newsletter_admin_menu(){
		global $newsletter_page_settings;
		$msmain01 = add_menu_page('MS Functions', 'MS Hilfe', 'edit_pages', 'ath-msmain', array($this, 'page'), $this->dashicon, 105);
	}
	public function admin_inline_js(){
		echo "<script type='text/javascript'>\n";
		echo 'var msloadingImage = ' . wp_json_encode( '<img alt="loading" class="ms-loading" src="/wp-includes/images/wpspin-2x.gif">' ) . ';';
		echo "\n</script>";
	}

	/*
		fn
	*/
	public function page(){
	?>
		<div class="wrap">
			<h2 class="page-title"><span class="dashicons <?php echo $this->dashicon ?>"></span> <?php echo $GLOBALS['title'] ?></h2>
			<?php $this->allfunctions(); ?>
		</div>
  <?php
	}
	public function allfunctions(){
		$args = array(
			'posts_per_page'   => -1,
			'post_type'        => 'post',
			'orderby'          => 'post_title',
			'order'            => 'ASC',
		);
		$posts = get_posts($args);

		echo '<table class="wp-list-table widefat striped">';
		foreach($posts as $post) {
				echo '<tr>';
				echo '<td>' . $post->post_title . '</td>';
				echo '<td>' . $post->post_excerpt . '</td>';
				echo '<td><a target="_blank" href="'.get_permalink( $post->ID ).'">'. __("Show more details") .'</a></td>';
				echo '<td><a href="'.get_edit_post_link( $post->ID ).'">'. __("Edit") .'</a></td>';
				echo '</tr>';
		}
		echo '</table>';
	}
}
?>
