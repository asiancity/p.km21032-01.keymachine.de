function addProgressbar(){
  jQuery(".meter").remove();
  var html = '<div class="progressbar"><div class="meter animate"><span style="width: 0%"><span></span></span></div><div class="progressbar-text"></div></div>';
  jQuery("body").append( html );

  jQuery("body").on("click", ".progressbar", function(){
    jQuery(".progressbar").remove();
  });
}
function animateProgressbar( args ){
  jQuery(".meter span").css( {"width" : args.percentage +'%'} );
  var html = '';
  html      += '<span>Counter: ' + args.count + '</span>';
  html      += '<span>Gesamt : ' + args.gesamt + '</span>';
  html      += '<span>Prozent: ' + args.percentage + '%</span>';
  jQuery(".progressbar .progressbar-text").html( html );
}

(function($){

  $.fn.boxheight = function (){
    var wh   = $(window).height();
    var frh  = wh;
    var boxh = wh-$(".page-title").outerHeight()-$(this).offset().top+10;
    $(this).height( boxh );

    $(this).find('.meta-box-sortables').each( function(){

      var margeinBottomHeight = 50;
      var boxheight           = ($(this).attr('data-height')*boxh/100)-$(this).find('.hndle').outerHeight()-margeinBottomHeight;
      var mainHeight          = $(this).find(".inside .main").innerHeight();

      $(this).find(".inside").height( boxheight ).find(".main").attr("data-height", mainHeight );
      if( boxheight > mainHeight){
        $(this).find(".inside").height( mainHeight );
      } else{
        $(this).find(".postbox").resizable({
          resize : function(event, ui) {
                    $(this).find('.inside').height( ui.size.height-50 );
                  }
        });
      }
    });
  };

  $.fn.tabletooltip  = function(){
    $('body').on('hover', '.wp-list-table td', function(e) {
      $('.tabletooltip').remove();
      var content = $(this).text();
      if( content.length > 5 ){
        var posY    = (e.pageY)+10;
        var posX    = (e.pageX)-10;
        $('body').append('<div class="tabletooltip">'+content+'</div>');
        $('.tabletooltip').css({"top" : posY, "left" : posX});
      }
    });
  };

  $.fn.autocall = function(idx){
      var data = '',
      $this      = $(this),
      action     = $(this).attr('data-action'),
      target     = $(this).attr('data-target'),
      params     = $(this).attr('data-params'),
      cols       = $(this).attr('data-cols'),
      table      = $.parseJSON($(this).attr('data-table')),
      rowActions = $.parseJSON($(this).attr('data-rowactions')),
      coldata    = [],
      out        = '',
      colidx     = [];

      out = '<table class="wp-list-table widefat striped" style="width:100%"><thead><tr>';
      $.each($.parseJSON(cols), function( idx, val ){

        out += '<th>'+  val +'</th>';
        coldata[idx] = {'data' : val, 'name' : val, 'title' : val, 'className' : 'col-'+val};
        if (val == "actions" ) colidx.actions = idx;
        if (val == "fbpic") colidx.fbpic    = idx;
      });
      out += '</tr></thead></table>';

      $this.find(target).append(out);


      $.ajax({
        url        : cmb2_l10.ajaxurl,
        data       : $.parseJSON(params),
        type       : "GET",
        dataType   : "json",
        beforeSend : function(){},
        success    : function(data){
          $this.find(target).find('table').DataTable({
            "data"       : data,
            "paging"     : (table.paging == 1) ? true : false,
            "searching"  : (table.searching == 1) ? true : false,
            "columns"    : coldata,
            "columnDefs" : [
            {
              "targets"   : colidx.actions,
              "orderable" : false,
              "render"    : function ( data, type, row, meta ) {
                var out = '';

                $.each( rowActions, function(idx, val){
                  var filterCols  = val.filter;
                  var filter = {};
                  $.each( filterCols, function(filterIdx, filterVal){
                    filter[filterVal]  = row[filterVal];
                  });

                  out += '<a href="#" ' +
                        'data-clickAction=\''+JSON.stringify(val.clickAction)+'\' '+
                        'data-filter=\''+ JSON.stringify(filter) +'\' ' +
                        'data-clickTarget="'+val.clickTarget+'">' +
                        '<span class="dashicons dashicons-'+val.icon+'"></span>'+
                        '</a>';
                });
                return out;
              }
            },
            {
              "targets"  : colidx.fbpic,
              "orderable": false,
              "render"   : function ( data, type, row, meta ) {
                return ( $.isNumeric(row.uid) ) ? '<a href="https://www.facebook.com/'+row.uid+'" target=""><img src="https://graph.facebook.com/'+row.uid+'/picture"></a>' : '';
              }
            }]
          });
        },
        complete   : function(){$('.fix-board').boxheight();},
        error      : function(e){console.log( e.respontext );}
      });
  };

  $.fn.dynClick = function(){
    $('body').on('click', '.autocall a', function(){
      var clickAction = $.parseJSON($(this).attr('data-clickaction'));
      var clickTarget = $(this).attr('data-clicktarget');
      var filter      = $.parseJSON($(this).attr('data-filter'));
      var newaction   = $.extend(clickAction, filter);

      $(clickTarget).attr("data-params", JSON.stringify(newaction) );
      $(clickTarget).find('.main').html('');
      $(clickTarget).autocall();
      return '';
    });
  };
})(jQuery);

jQuery(document).ready( function($) {
  var fixBoard  = $('.fix-board');
  if ( fixBoard.length > 0 ){
    fixBoard.boxheight();
  }
  $( window ).resize(function() {
    if ( fixBoard.length > 0 ){
      fixBoard.boxheight();
    }
  });
  $.fn.tabletooltip();
  $('.autocall').each( function(idx){
    $(this).autocall(idx);

  });

  $.fn.dynClick();
});
