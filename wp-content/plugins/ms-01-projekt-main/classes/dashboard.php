<?php
/**
 *  $args = array("postbox_class" => array("first" => "postbox-container"));
 *  $widgets = array("align" => "left", "title" => "Titel", "button" => array( array("title", "href", "id", "class") ), "data" => $data),
 */
class MSDashboard
{
  public $widgets;
  public $args;

  function __construct($widgets=array(), $args=array())
  {
    wp_enqueue_style('dashboard');
    wp_enqueue_script('dashboard');
    wp_enqueue_script('jquery-ui-position');

    wp_enqueue_style('jquery-ui', 'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css');
    wp_enqueue_script('jquery-ui-resizable');

    if( count($widgets) < 1){
      $args = array(
        "postbox_class" => array("first" => "postbox-container", "second" => "postbox-container") );
    }
    if( count($widgets) < 1){
      $widgets = array(
        array(
          "align"       => "left",
          "title"       => "Title",
          "height"      => "",
          "button"      => "",
          "data-action" => "",
          "data-target" => "",
          "data"        => ""
        )
      );
    }

    $this->args    = $args;
    $this->widgets  = $widgets;
    $this->widgets  = $this->columns();
  }
  public function columns(){
    $newWidgets = array();
    foreach( $this->widgets as $widget){
      $newWidgets[ $widget["align"] ][] = $widget;
    }
    /*var_dump($newWidgets);*/
    return $newWidgets;
  }
  public function buttonHandle( $widget ){
    $button = '';
    $out    = '';
    extract( $widget );

    if( is_array($button) ){
      foreach ($button as $but) {
        $id     = '';
        $class  = '';
        $title  = '';
        extract( $but );
        $out .= '<a id="'.$id.'" href="'. (isset($button["href"]) ? $button["href"] : '#') .'" class="handlediv handlediv-ms button '.( isset($class) ? $class : 'button-primary').'">'.$title.'</a>';
      }
    }
    $out   .= $this->btnClosed();
    return $out;
  }
  public function btnClosed(){
    return '<button type="button" class="handlediv button-link" aria-expanded="true">
    <span class="screen-reader-text">Bedienfeld umschalten: Aktivität</span>
    <span class="toggle-indicator" aria-hidden="true"></span>
    </button>';
  }

  public function widget( $widget=array() )
  {
    $id       = '';
    $class    = '';
    $title    = '';
    $data     = '';
    $height   = 100;
    $dataHref = '';
    $dataAjax = '';
    $ajax     = '';
    $boxNo    = '';
    $params   = '';
    $cols     = '';
    $e_param  = '';
    $table    = '';
    $rowactions  = '';
    extract($widget);

    if ( $ajax == TRUE ){
      $class  = $class . ' autocall';
    }

    $out = '<div ';

    $out .= 'id="'.  $id  .'" ';
    $out .= 'class="meta-box-sortables ui-sortable '.$boxNo .' '. $class.'" ';
    $out .= ( $ajax == TRUE ) ? 'data-ajax = "true" ' : '';
    $out .= ($cols) ? "data-cols='".json_encode($cols)."' " : "";
    $out .= 'data-height="'.$height.'" ';
    $out .= ( ( is_array( $params ) && count($params) > 0 ) ) ? "data-params='" . json_encode( $params )  ."' " : "";
    $out .= 'data-target=".main" ';
    $out .= "data-table='".json_encode( $table )."'";
    $out .= "data-rowactions='".json_encode( $rowactions )."'";
    $out .= '>';

    $out .= '<div class="postbox">';
    $out .= $this->buttonHandle( $widget );
    $out .= '<h2 class="hndle ui-sortable-handle">'. $title .'</h2>';
    $out .= '<div class="inside" data-href="'.$dataHref.'"><div class="main">'.$data.'</div></div>';
    $out .= '</div>';
    $out .= '</div>';

    return $out;
  }

  public function display()
  {
    $widgets  =  array();
    $widgets  =  $this->widgets;
    $colNo    =  1;
    $boxNo    =  1;
    $postboxHidden  = FALSE;
    $out      = '<form style="display:none" method="get" action="">';
    $out     .= wp_nonce_field('closedpostboxes', 'closedpostboxesnonce', true );
    $out     .= wp_nonce_field('meta-box-order', 'meta-box-order-nonce', true );
    $out     .= '</form>';
    $out     .= '<div id="dashboard-widgets-wrap">';
    $out     .= '<div id="dashboard-widgets" class="metabox-holder fix-board clearfix">';

    foreach( $this->args["postbox_class"] as $postboxKey => $postboxValue ){
      $wleft               = '';
      $out                .= '<div data-name="'.$postboxKey.'" id="postbox-container-'.$colNo.'" class="postbox-container '.(( $this->args["postbox_class"][$postboxKey] ) ? $this->args["postbox_class"][$postboxKey] : "" ) .'">';
      $widgetsPostBoxkey   = isset($widgets[$postboxKey]) ? $widgets[$postboxKey] : array();

      foreach( $widgetsPostBoxkey as $wleft){
        $wleft["boxNo"] = "boxNo-" . $boxNo;
        $out           .= $this->widget( $wleft );
        $boxNo++;
      }
      $out .= '</div>';
      $colNo++;

      if( strrpos($this->args["postbox_class"][$postboxKey], 'col') !== FALSE && $postboxHidden == FALSE){
        $postboxHidden  = TRUE;
      }
    }
    if( $postboxHidden == FALSE ){
    $out .= '<div id="postbox-container-'.($boxNo).'" class="postbox-container"><div id="column4-sortables" class="meta-box-sortables"></div></div>';
    }
    $out .= '</div></div>';
    return $out;
  }
}
?>
