<?php
/**
 *
 */
class MSTableSimple
{
  public $data;
  public $cols;
  public $button;

  public function __construct( $args )
  {
    if( !is_array($args["cols"]) ){
      wp_die( sprintf(__("Error code: %s"), 'args["cols"]'));
      return '';
    }
    if( !( is_array($args["data"]) || is_object($args["data"]) ) ){
      wp_die( sprintf(__("Error code: %s"), 'args["data"]'));
      return '';
    }

    $this->cols   = @$args["cols"];
    $this->data   = @$args["data"];
    $this->button = @$args["button"];
  }

  public function col(  $d, $col )
  {
    $names  = '';
    $col    = isset( $col ) ? $col : NULL;
    $d      = isset( $d ) ? $d : array();
    if( strpos($col, "button") !== false ){
      $button = $this->button[ $col ];
      $id = $button["id"];

      $out = '<td class="column-'.$col.'">';
      $out .= '<a href="'.$button["url"].'&'. $button["id"]. '=' . $d->$id .'">'.$this->hrefText( $col ).'</a>';
      $out .= '</td>';
      return $out;
    }
    if( $col == "names"){
      $names = maybe_unserialize($d->$col);
      return '<td class="column-'.$col.' '.$col.'">'. $names["fullname"] .'</td>';
    }
    return (!empty($d->$col)) ? '<td class="column-'.$col.' '.$col.'">'. $d->$col .'</td>' : '';
  }

  public function hrefText( $col )
  {
    $button = $this->button[ $col ];
    if( $button["icon"] ){
      return '<span class="dashicons '.$button["icon"].'"></span>';
    }
    return $button["text"];
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

  public function display()
  {
    $out = '<table class="wp-list-table widefat striped">';
    $out .= '<thead>';
    $out .= '<tr>';
    foreach( $this->cols as $col ){
      $out .= ( strpos($col, "button") !== false  ) ? '<td class="column-'.$col.'"></td>' : '<td class="column-'.$col.' '.$col.'">'.$col.'</td>';
    }
    $out .= '</tr>';
    $out .= '</thead>';

    $out .= '<tbody>';
    foreach( $this->data as $d ){
      $out .= '<tr>';
      foreach( $this->cols as $col ){
        $out .= $this->col( $d, $col );
      }
      $out .= '</tr>';
    }
    $out .= '</tbody>';
    $out .= '</table>';

    return $out;
  }
}

?>
