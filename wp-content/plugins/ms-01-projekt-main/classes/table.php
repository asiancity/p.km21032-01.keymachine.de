<?php
class MSTable
{
  public $db;
  public $items;
  public $per_page = 50;
  public $page_number;
  public $total_items;
  public $_pagination_args;
  public $_pagination;
  public $request;

  public function __construct()
  {
    $prefix             = '';
    $this->db           = $prefix . '';
    $this->page_number  = isset($_REQUEST["paged"]) ? $_REQUEST["paged"] : 1;
    $this->request      = $_GET;
  }
  public function tableCols( $col ){
    $request = $_GET;
    unset( $request["orderby"] );
    unset( $request["order"] );
    if( count($request) > 0 ){
      $parameter = '?';
      foreach( $request as $rK => $rV ){
        $parameter .= $rK . '=' .$rV . '&';
      }
    }
    $parameter .= 'orderby=' . $col . '&';
    $parameter .= (isset($_GET["order"]) && $_GET["order"] == "asc") ? 'order=desc' : 'order=asc';
    return '<a href="'.admin_url("admin.php"). $parameter . '">'.$col.'</a>';
  }

  public function cols(){
    $cols = $wpdb->get_col( "DESC " . $this->db, 0 );
    return $cols;
  }

  public function table()
  {

  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

  public function search_box( $text='', $input_id='' ){
    if ( empty( $_REQUEST['s'] ) && !$this->items )
          return;

    $input_id = $input_id . '-search-input';
    $request = $this->request;
    /*unset( $request["orderby"] );
    unset( $request["order"] );*/

    $out = '<p class="search-box">';
    $out .= '<label class="screen-reader-text" for="'. $input_id .'">'. $text .':</label>';
    if( count($request) > 0 ){
      $parameter = '?';
      unset($request["paged"]);
      foreach( $request as $rK => $rV ){
        $out .= '<input type="hidden" name="' .$rK . '" value="' .$rV . '" class="' .$rK . '">';
      }
    }

    $out .= '<input type="search" id="' .$input_id .'" name="s" value="'. (isset($_REQUEST['s']) ? esc_attr( wp_unslash( $_REQUEST['s'] ) ) : '') .'" />';
    $out .= get_submit_button( $text, 'button', '', false, array('id' => 'search-submit') );
    $out .= '</p>';
    return $out;
  }
  public function set_pagination_args( $args )
  {
    $args = wp_parse_args( $args, array(
        'total_items' => 0,
        'total_pages' => 0,
        'per_page' => 0,
    ) );

    if ( $args['total_pages'] > 0 && $args['per_page'] > 0 )
        $args['total_pages'] = ceil( $args['total_items'] / $args['per_page'] );

    // Redirect if page number is invalid and headers are not already sent.
    if ( ! headers_sent() && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) && $args['total_pages'] > 0 && $this->get_pagenum() > $args['total_pages'] ) {
        wp_redirect( add_query_arg( 'paged', $args['total_pages'] ) );
        exit;
    }

    $this->_pagination_args = $args;

  }
  public function get_pagenum() {
    $pagenum = isset( $_REQUEST['paged'] ) ? absint( $_REQUEST['paged'] ) : 0;

    if ( isset( $this->_pagination_args['total_pages'] ) && $pagenum > $this->_pagination_args['total_pages'] )
        $pagenum = $this->_pagination_args['total_pages'];

    return max( 1, $pagenum );
  }

  public function pagination( $which='' ) {
      if ( empty( $this->_pagination_args ) ) {
          return;
      }

      $total_items = $this->_pagination_args['total_items'];
      $total_pages = $this->_pagination_args['total_pages'];
      $infinite_scroll = false;
      if ( isset( $this->_pagination_args['infinite_scroll'] ) ) {
          $infinite_scroll = $this->_pagination_args['infinite_scroll'];
      }


      $output = '<span class="displaying-num">' . sprintf( _n( '%s item', '%s items', $total_items ), number_format_i18n( $total_items ) ) . '</span>';

      $current = $this->get_pagenum();

      $current_url = set_url_scheme( 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );

      $current_url = remove_query_arg( array( 'hotkeys_highlight_last', 'hotkeys_highlight_first' ), $current_url );

      $page_links = array();

      $total_pages_before = '<span class="paging-input">';
      $total_pages_after  = '</span>';

      $disable_first = $disable_last = $disable_prev = $disable_next = false;

      if ( $current == 1 ) {
          $disable_first = true;
          $disable_prev = true;
      }
      if ( $current == 2 ) {
          $disable_first = true;
      }
      if ( $current == $total_pages ) {
          $disable_last = true;
          $disable_next = true;
      }
      if ( $current == $total_pages - 1 ) {
          $disable_last = true;
      }

      if ( $disable_first ) {
          $page_links[] = '<span class="tablenav-pages-navspan" aria-hidden="true">&laquo;</span>';
      } else {
          $page_links[] = sprintf( "<a class='first-page' href='%s'><span class='screen-reader-text'>%s</span><span aria-hidden='true'>%s</span></a>",
              esc_url( remove_query_arg( 'paged', $current_url ) ),
              __( 'First page' ),
              '&laquo;'
          );
      }

      if ( $disable_prev ) {
          $page_links[] = '<span class="tablenav-pages-navspan" aria-hidden="true">&lsaquo;</span>';
      } else {
          $page_links[] = sprintf( "<a class='prev-page' href='%s'><span class='screen-reader-text'>%s</span><span aria-hidden='true'>%s</span></a>",
              esc_url( add_query_arg( 'paged', max( 1, $current-1 ), $current_url ) ),
              __( 'Previous page' ),
              '&lsaquo;'
          );
      }

      if ( 'bottom' === $which ) {
          $html_current_page  = $current;
          $total_pages_before = '<span class="screen-reader-text">' . __( 'Current Page' ) . '</span><span id="table-paging" class="paging-input">';
      } else {
          $html_current_page = sprintf( "%s<input class='current-page' id='current-page-selector' type='text' name='paged' value='%s' size='%d' aria-describedby='table-paging' />",
              '<label for="current-page-selector" class="screen-reader-text">' . __( 'Current Page' ) . '</label>',
              $current,
              strlen( $total_pages )
          );
      }
      $html_total_pages = sprintf( "<span class='total-pages'>%s</span>", number_format_i18n( $total_pages ) );
      $page_links[] = $total_pages_before . sprintf( _x( '%1$s of %2$s', 'paging' ), $html_current_page, $html_total_pages ) . $total_pages_after;

      if ( $disable_next ) {
          $page_links[] = '<span class="tablenav-pages-navspan" aria-hidden="true">&rsaquo;</span>';
      } else {
          $page_links[] = sprintf( "<a class='next-page' href='%s'><span class='screen-reader-text'>%s</span><span aria-hidden='true'>%s</span></a>",
              esc_url( add_query_arg( 'paged', min( $total_pages, $current+1 ), $current_url ) ),
              __( 'Next page' ),
              '&rsaquo;'
          );
      }

      if ( $disable_last ) {
          $page_links[] = '<span class="tablenav-pages-navspan" aria-hidden="true">&raquo;</span>';
      } else {
          $page_links[] = sprintf( "<a class='last-page' href='%s'><span class='screen-reader-text'>%s</span><span aria-hidden='true'>%s</span></a>",
              esc_url( add_query_arg( 'paged', $total_pages, $current_url ) ),
              __( 'Last page' ),
              '&raquo;'
          );
      }

      $pagination_links_class = 'pagination-links';
      if ( ! empty( $infinite_scroll ) ) {
          $pagination_links_class = ' hide-if-js';
      }
      $output .= "\n<span class='$pagination_links_class'>" . join( "\n", $page_links ) . '</span>';

      if ( $total_pages ) {
          $page_class = $total_pages < 2 ? ' one-page' : '';
      } else {
          $page_class = ' no-pages';
      }

      return '<div class="tablenav-pages"'.$page_class.'>'.$output.'</div>';
  }
/**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
  protected function extra_tablenav( $args ) {
    switch ($args["type"]) {
      case 'select':
        return $this->extra_tablenav_select( $args );
        break;

      default:
        # code...
        break;
    }
  }
  /**
 	 * Block comment
 	 *
 	 * @param data, type, label
 	 * @return void
	 */

  public function extra_tablenav_select( $args )
  {
    $items = $args["data"];
    if( count($items) < 1 )
      return '';

    $selectedItems = ( isset($args["name"]) && isset($_REQUEST[ $args["name"] ]) ) ? $_REQUEST[ $args["name"] ] : '';
    /*var_dump( $selectedItems );*/
    $out = '<select name="'.$args["name"].'">';
    $out .= '<option></option>';

    if(count(array_filter($items,'is_array')) > 0){

      foreach($items as $itemK => $itemV)
        $out .= '<option value="'.$itemK.'" '. (($selectedItems == $itemK) ? 'selected' : '') .'>'.$itemV.'</option>';

    } else {

      foreach($items as $item)
        $out .= '<option value="'.$item.'" '. (($selectedItems == $item) ? 'selected' : '') .'>'.$item.'</option>';
    }

    $out .= '</select>';
    return $out;
  }
}
?>
