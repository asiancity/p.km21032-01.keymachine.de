<?php
function cmb2_register_kunden() {
    $pt       = 'kunden';
    $prefix   = $pt. '_';
    $cmb = new_cmb2_box( array(
        'id'            => $prefix . 'edit',
        'title'         => 'Informationen',
        'object_types'  => array( $pt ),
        'context'       => 'normal',
    ) );

    //Kunden Informationen
    $fields = array('nachname', 'vorname', 'firmenname', 'email', 'telefon', 'fax', 'adresse', 'stadt', 'region', 'postleitzahl' );
    foreach( $fields as $f ){
      $cmb->add_field( array(
          'name'          =>  ucfirst($f),
          'desc'          =>  '',
          'type'          =>  'text',
          'id'            =>  $prefix . $f
      ) );
    }

    $cmb->add_field( array(
		  'name' => 'Kunden seit',
		  'id'   => $prefix . 'register_datum',
		  'type' => 'text_date',
	    'date_format' => 'Y-m-d',
	   ) );
}
add_action( 'cmb2_admin_init', 'cmb2_register_kunden' );

//Register Meta Box
function ath_kunden_meta_box() {
    add_meta_box( 'ath-meta-box-id', 'Buchhaltung', 'ath_meta_box_buchhaltung', 'kunden', 'advanced', 'high' );
}
add_action( 'add_meta_boxes', 'ath_kunden_meta_box');

//Add field
function ath_meta_box_buchhaltung() {
  $args = array(
    'numberposts' => 10,
    'post_type'   => 'buchhaltung'
  );
  $posts = get_posts( $args );
  // var_dump($posts);
  if ( $posts ) {
    $out = '<ul>';
    $out .= '<li><ul class="clearfix">
    <li>Reg</li>
    <li>Status</li>
    <li>Eingangsdatum</li>
    <li>Typ</li>
    <li>Betrag</li>
    <li>Rechnungsdatum</li>
    </ul></li>';
    foreach ( $posts as $post ) :
      $out .= '<li><ul class="clearfix">';
        $out .= '<li><a href="/wp-admin/post.php?post='.$post->ID.'&action=edit">'. $post->post_title .'</a></li>';
        $out .= '<li>'. $post->buchhaltung_status .'</li>';
        $out .= '<li>'. ath_kunden_datum_format($post->buchhaltung_zahlung_datum) .'</li>';
        $out .= '<li>'. $post->buchhaltung_type .'</li>';
        $out .= '<li>'. $post->buchhaltung_rechnung_betrag .'</li>';
        $out .= '<li>'. ath_kunden_datum_format($post->buchhaltung_rechnung_datum) .'</li>';
      $out .= '</ul></li>';
    endforeach;
    $out .= '</ul>';
    echo $out;
  }
}
function ath_kunden_datum_format( $date ){
  return date_i18n('d.m.Y', strtotime($date));
}
?>
