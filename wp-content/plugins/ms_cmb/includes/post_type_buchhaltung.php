<?php
function cmb2_register_buchhaltung() {
    $pt       = 'buchhaltung';
    $prefix   = $pt. '_';
    $cmb_2 = new_cmb2_box( array(
        'id'            => $pt . '_status',
        'title'         => 'Rechnungsstatus',
        'object_types'  => array( $pt ),
        'context'       => 'normal',
    ) );
    $cmb_2->add_field( array(
        'name'             => 'Status',
        'id'               => $prefix . 'status',
        'type'             => 'select',
        'show_option_none' => true,
    		'options'          => array(
    			    'in_bearbeitung'   => 'in Bearbeitung',
              'offen'            => 'Offen',
              'bezahlt'          => 'bezahlt'
    		),
    ) );
    $cmb_2->add_field( array(
     'name' => 'Zahlungseingang',
     'id'   => $prefix . 'zahlung_datum',
     'type' => 'text_date',
     'date_format' => 'Y-m-d',
    ) );


    $cmb = new_cmb2_box( array(
        'id'            => $pt,
        'title'         => 'Rechnung',
        'object_types'  => array( $pt ),
        'context'       => 'normal',
    ) );

    $cmb->add_field( array(
        'name'             => 'Kunden',
        'id'               => $prefix. 'kunden',
        'type'             => 'select',
        'show_option_none' => true,
        'default'          => 'custom',
        'options'          => cmb2_get_post_options( array( 'post_type' => 'kunden', 'numberposts' => 5 ) ),
    ) );
    $cmb->add_field( array(
		  'name'             => 'Art',
		  'id'               => $prefix . 'type',
		  'type'             => 'select',
      'show_option_none' => true,
  		'options'          => array(
  			    'privated'   => 'Privat',
            'published'  => 'Veröffentlicht'
  		),
	   ) );
     $cmb->add_field( array(
 		  'name' => 'Rechnungsdatum',
 		  'id'   => $prefix . 'rechnung_datum',
 		  'type' => 'text_date',
 	    'date_format' => 'Y-m-d',
 	   ) );
     $cmb->add_field( array(
 		  'name'          => 'Rechnungsbetrag',
 		  'id'            => $prefix . 'rechnung_betrag',
 		  'type'          => 'text_money',
      'before_field'  => '€',
 	    'after_field'   => ' Netto',
 	   ) );
}
add_action( 'cmb2_admin_init', 'cmb2_register_buchhaltung' );
function cmb2_get_post_options($query_args) {

  $args = wp_parse_args( $query_args, array(
      'post_type'   => 'post',
      'numberposts' => 10,
  ) );

  $posts = get_posts( $args );

  $post_options = array();
  if ( $posts ) {
      foreach ( $posts as $post ) {
        $post_options[ $post->ID ] = $post->post_title;
      }
  }

  return $post_options;
}
?>
