<?php
/*
Plugin Name: ATH CMB2
Plugin URI:
Description: Map CMB2
Version: 1.0
Author: Anh-Tuan Hoang
Author URI:
*/

require('includes/post_type_kunden.php');
require('includes/post_type_buchhaltung.php');

function ms_cmb_style() {
  wp_enqueue_style('ms_cmb_style', plugins_url( 'css/style.css', __FILE__ ) );
}
add_action('admin_enqueue_scripts', 'ms_cmb_style');
